/**
 *
 * DatePickerField
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import TextInput from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import { Grid, FormLabel, FormGroup } from '@material-ui/core';
import KeyboardEventHandler from 'react-keyboard-event-handler';
import moment from 'moment';
function DatePickerField({
  field,
  margin,
  className,
  onChange,
  variant,
  keyEvents,
  loading,
  maxDate,
  minDate,
  disablePast,
  disableFuture,
}) {
  const [month, setMonth] = useState();
  const [day, setDay] = useState();
  const [year, setYear] = useState();

  useEffect(() => {
    if (field.value) {
      const dob = moment(field.value);

      setMonth(dob.month() + 1);
      setDay(dob.date());
      setYear(dob.year());
    }
  }, [field]);
  if (!field) return null;

  const min = minDate || disablePast ? moment(minDate || undefined) : undefined;
  const max =
    maxDate || disableFuture ? moment(maxDate || undefined) : undefined;

  const { error, caption, errorMessage, id, pristine, readOnly } = field;
  let message = caption;
  if (error && !pristine && field.required)
    message = !errorMessage ? `${caption} is required` : errorMessage;

  const handleMonthChange = e => {
    const { value } = e.target;
    setMonth(value);
    const selectedDate = `${value}/${day}/${year}`;
    onChange(new Date(selectedDate));
  };

  const handleDayChange = e => {
    const { value } = e.target;

    setDay(value);
    const selectedDate = `${month}/${value}/${year}`;
    onChange(new Date(selectedDate));
  };

  const handleYearChange = e => {
    const { value } = e.target;
    setYear(value);

    const selectedDate = `${month}/${day}/${value}`;
    if (value !== '' && value.length === 4) {
      onChange(new Date(selectedDate));
    } else {
      onChange(null);
    }
  };

  const renderForm = () => (
    <FormControl
      margin={margin}
      required={field.required}
      fullWidth={field.fullWidth}
      className={className}
      label="test"
    >
      <FormLabel
        component="legend"
        style={{ paddingBottom: '.5rem' }}
        error={error && !pristine}
        required={field.required}
      >
        {field.error && !pristine ? message : caption}
      </FormLabel>
      <FormGroup>
        <Grid container spacing={1}>
          <Grid item xs={3}>
            <TextInput
              id={`mm-${id}`}
              name={`mm-${id}`}
              error={error && !pristine}
              helperText={loading ? 'Please wait while loading...' : undefined}
              label="Month"
              onChange={handleMonthChange}
              placeholder="MM"
              fullWidth
              value={month}
              type="number"
              autoComplete="off"
              inputProps={{ readOnly, autoComplete: 'off', min: 1, max: 12 }}
              variant={variant}
              InputLabelProps={{
                shrink: true,
              }}
              required={field.required}
            />
          </Grid>
          <Grid item xs={3}>
            <TextInput
              id={`dd-${id}`}
              fullWidth
              name={`dd-${id}`}
              error={error && !pristine}
              helperText={loading ? 'Please wait while loading...' : undefined}
              label="Day"
              onChange={handleDayChange}
              placeholder="DD"
              value={day}
              type="number"
              autoComplete="off"
              inputProps={{ readOnly, autoComplete: 'off', min: 1, max: 31 }}
              variant={variant}
              InputLabelProps={{
                shrink: true,
              }}
              required={field.required}
            />
          </Grid>
          <Grid item xs={6}>
            <TextInput
              id={`yy-${id}`}
              name={`yy-${id}`}
              error={error && !pristine}
              helperText={loading ? 'Please wait while loading...' : undefined}
              label="Year"
              onChange={handleYearChange}
              placeholder="YYYY"
              fullWidth
              value={year}
              type="number"
              autoComplete="off"
              inputProps={{
                readOnly,
                autoComplete: 'off',
                min: min.year(),
                max: max.year(),
              }}
              variant={variant}
              InputLabelProps={{
                shrink: true,
              }}
              required={field.required}
            />
          </Grid>
        </Grid>
      </FormGroup>
    </FormControl>
  );
  return !keyEvents ? (
    renderForm()
  ) : (
    <KeyboardEventHandler
      handleKeys={keyEvents.handleKeys}
      onKeyEvent={keyEvents.onKeyEvent}
      style={{ width: '100%' }}
    >
      {renderForm()}
    </KeyboardEventHandler>
  );
}
const { string, bool, shape, func, object, instanceOf } = PropTypes;
DatePickerField.propTypes = {
  field: shape({
    id: string.isRequired,
    value: string,
    caption: string.isRequired,
    errorMessage: string,
    placeholder: string,
    error: bool,
    required: bool,
    fullWidth: bool,
    autoFocus: bool,
  }),
  onChange: func,
  margin: string,
  className: string,
  variant: string,
  keyEvents: object,
  loading: bool,
  minDate: PropTypes.oneOfType([instanceOf(Date), instanceOf(moment)]),
  maxDate: PropTypes.oneOfType([instanceOf(Date), instanceOf(moment)]),
  disableFuture: bool,
  disablePast: bool,
};
DatePickerField.defaultProps = {
  margin: 'normal',
  variant: 'outlined',
  onChange: undefined,
  className: undefined,
  disableFuture: false,
  disablePast: false,
};
export default DatePickerField;
