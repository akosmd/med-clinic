import moment from 'moment';

import { cleanedPhone } from '../patientEvaluator';

export const registerToAdvinow = ({
  registrationData,
  formValues,
  dispatch,
  doInsertAdvinow,
}) => {
  const {
    data: { uuid },
  } = registrationData;
  const {
    firstName,
    lastName,
    zipCode,
    email,
    phone,
    dateOfBirth,
  } = formValues;

  const birthDate = moment(dateOfBirth).format('YYYY-MM-DD');

  const params = {
    first_name: firstName,
    last_name: lastName,
    birth_date: birthDate,
    zip: zipCode,
    email,
    phone: cleanedPhone(phone),
    password: 'Akos@1111',
    external_id: uuid,
  };
  dispatch(doInsertAdvinow(params));
};
