import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core';

export default function ExistsDialog({ handleCancel, open }) {
  return (
    <div>
      <Dialog
        open={open}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Member Exists</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            It seems your account is registered already. Please login using your
            account.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCancel}>Let me update it</Button>
          <Button href="/login" color="primary">
            Yes, take me to Login Screen
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

ExistsDialog.propTypes = {
  open: PropTypes.bool,
  handleCancel: PropTypes.func,
};
