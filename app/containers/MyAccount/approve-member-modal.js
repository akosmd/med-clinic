import React from 'react';

import PropTypes from 'prop-types';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  loading: {
    margin: theme.spacing(1),
  },
}));

export default function ApproveMemberModal({
  details: { loading, data: success } = {},
  member,
  open,
  onClose,
  onApprove,
  approve,
}) {
  const classes = useStyles();

  const name = `${member.firstName || ''} ${member.middleName ||
    ''} ${member.lastName || ''}`;

  return (
    <Dialog
      open={open}
      onClose={onClose}
      disableBackdropClick={loading}
      disableEscapeKeyDown={loading}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Please Confirm</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {!success && (
            <>
              Please confirm to {approve ? 'approve' : 'deny'}{' '}
              <strong>{name}</strong>
            </>
          )}
          {success && (
            <>
              <strong>{name}</strong> is now {approve ? 'approved' : 'denied'}!
            </>
          )}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        {!loading && (
          <Button onClick={onClose} color="primary">
            {!success ? 'Cancel' : 'Close'}
          </Button>
        )}
        {!success && (
          <Button onClick={onApprove} color="primary" autoFocus>
            {loading ? (
              <CircularProgress className={classes.loading} color="primary" />
            ) : (
              `${approve ? 'Approve' : 'Deny'} Member`
            )}
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
}

const { bool, func, object } = PropTypes;

ApproveMemberModal.propTypes = {
  open: bool.isRequired,
  details: object.isRequired,
  member: object.isRequired,
  onClose: func.isRequired,
  onApprove: func.isRequired,
  approve: bool,
};
