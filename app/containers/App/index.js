/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import AkosPage from 'components/AkosPage';
import AuthenticatedRoute from 'components/AuthenticatedRoute';
import ChatBot from 'components/Chat/Loadable';
import MedicalHistory from 'components/MedicalHistory';

import GiChat from 'containers/GIChat/Loadable';
import CovidStandalone from 'containers/CovidChat/covidStandalone';
import Inxite from 'components/Inxite';

import HomePage from 'containers/HomePage/Loadable';
// import MedClinic from 'containers/HomePage/medclinic';
import Covid19Home from 'containers/HomePage/covid19';
import CovidUrgentCareHome from 'containers/HomePage/covid19-agile-urgentcare';
import CovidStandAloneHome from 'containers/HomePage/covidStandAlone';
import MedClinicSso from 'components/MedClinic';
import PaymentOptions from 'components/connect';
import VerifyCode from 'containers/CodeVerification';
import RxSpark from 'containers/RxSpark';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
// import Register from 'containers/Register/Loadable';
import CovidNJRegister from 'containers/Register/covid';
import MedClinicRegister from 'containers/Register/medclinic';
import VdpcLite from 'containers/Register/vdpc-lite';
import ChatRoom from 'containers/ChatRoom';
import Info from 'containers/Info/Loadable';
import InfoSsn from 'containers/Info/info-ssn';
import Login from 'containers/Login/Loadable';
import MedClinicLogin from 'containers/Login/medclinic';
import AccountVerification from 'containers/AccountVerification';
import AccountVerificationSsn from 'containers/AccountVerification/verify-ssn';
import TwoFactor from 'containers/2FA';
import Benefits from 'containers/Benefits';
import Appointment from 'containers/Appointment';
import ChangePassword from 'containers/ChangePassword';
import MyAccount from 'containers/MyAccount';
import Faq from 'containers/Faq';
import NurseWaitingRoom from 'containers/ChatRoom/nurse';
import NurseToMemberChat from 'containers/ChatRoom/nurse-member-chat';
import Conf from 'containers/opentok-conf';

import { MEMBER_TYPES } from 'utils/config';

import {
  makeSelectSignin,
  makeSelectVerifyAuth,
  makeAccountVerification,
  makeSelectVerifySsn,
} from 'containers/App/selectors';

import GlobalStyle from '../../global-styles';

function App(props) {
  const { signin, auth, history, accountVerification, ssnVerified } = props;
  const [isAuthenticated, setAuthenticated] = useState(
    auth.data && auth.data !== false,
  );
  const [isAccountVerified, setAccountVerified] = useState(false);

  useEffect(() => {
    const { runtime } = props;
    if (
      process.env.NODE_ENV === 'production' ||
      process.env.NODE_ENV === 'staging'
    ) {
      runtime.install({
        onUpdating: () => {
          // eslint-disable-next-line no-console
          console.log('SW Event:', 'updating resources');
        },
        onUpdateReady: () => {
          // eslint-disable-next-line no-console
          console.log('SW Event:', 'applying file updates');

          runtime.applyUpdate();
        },
        onUpdated: () => {
          // eslint-disable-next-line no-console
          console.log('SW Event:', 'resources updated successfully');
        },
        onUpdateFailed: () => {
          // eslint-disable-next-line no-console
          console.log(
            'SW Event:',
            'an error occured while trying to update resources',
          );
        },
      });
    }
  }, []);

  useEffect(() => {
    setAuthenticated(auth.data && auth.data !== false);
  }, [auth]);

  useEffect(() => {
    setAccountVerified(
      accountVerification.data && accountVerification.data !== undefined,
    );
  }, [accountVerification]);

  const isVdpc =
    auth.data && auth.data.patient.membershipTypeId === MEMBER_TYPES.VDPC;

  return (
    <div>
      {/* <Router> */}
      <Switch>
        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          exact
          path="/"
          unauthorizedPath="/login"
          component={() => <AkosPage component={HomePage} history={history} />}
        />
        {/* <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          exact
          path="/medclinic"
          unauthorizedPath="/medclinic/login"
          component={() => <AkosPage component={MedClinic} history={history} />}
        />
        <Route
          path="/medclinic/register"
          component={() => (
            <AkosPage component={MedClinicRegister} history={history} />
          )}
        />

        <Route
          path="/medclinic/login"
          component={() => (
            <AkosPage component={MedClinicLogin} history={history} />
          )}
        /> */}
        <AuthenticatedRoute
          path="/medclinic/sso"
          isAuthenticated={isAuthenticated}
          component={() => (
            <AkosPage component={MedClinicSso} history={history} />
          )}
        />
        <AuthenticatedRoute
          isAuthenticated={isAccountVerified}
          path="/verify-code"
          component={() => (
            <AkosPage component={VerifyCode} history={history} />
          )}
        />

        <Route
          path="/register"
          exact
          component={() => (
            <AkosPage component={MedClinicRegister} history={history} />
          )}
        />
        <Route
          path="/vdpc-lite/register"
          exact
          component={() => <AkosPage component={VdpcLite} history={history} />}
        />
        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          path="/chat"
          component={() => (
            <ChatBot
              history={history}
              membershipTypeId={auth.data.patient.membershipTypeId}
            />
          )}
        />
        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          path="/medical-history"
          component={() => (
            <AkosPage component={MedicalHistory} history={history} />
          )}
        />
        <AuthenticatedRoute
          isAuthenticated
          exact
          path="/gi"
          component={() => <GiChat history={history} />}
        />
        <AuthenticatedRoute
          path="/covid19"
          isAuthenticated={isAuthenticated}
          component={() => (
            <AkosPage component={Covid19Home} history={history} />
          )}
        />
        <Route
          exact
          path="/covid-pa"
          component={() => <CovidStandalone history={history} />}
        />
        <AuthenticatedRoute
          exact
          isAuthenticated={isAuthenticated}
          unauthorizedPath="/covid-nj/login"
          path="/covid-nj"
          component={() => (
            <AkosPage component={CovidUrgentCareHome} history={history} />
          )}
        />
        <AuthenticatedRoute
          exact
          isAuthenticated={isAuthenticated}
          unauthorizedPath="/covid/login"
          path="/covid"
          component={() => (
            <AkosPage component={CovidStandAloneHome} history={history} />
          )}
        />
        <Route
          exact
          unauthorizedPath="/covid-nj/login"
          path="/covid-nj/register"
          component={() => (
            <AkosPage
              component={CovidNJRegister}
              history={history}
              membershipTypeId={MEMBER_TYPES.Covid}
            />
          )}
        />
        <Route
          exact
          unauthorizedPath="/covid/login"
          path="/covid/register"
          component={() => (
            <AkosPage
              component={CovidNJRegister}
              history={history}
              membershipTypeId={MEMBER_TYPES.CovidStandAlone}
            />
          )}
        />
        <Route
          exact
          path="/covid-nj/login"
          unauthorizedPath="/covid-nj/login"
          component={() => <AkosPage component={Login} history={history} />}
        />
        <Route
          exact
          path="/covid/login"
          unauthorizedPath="/covid/login"
          component={() => <AkosPage component={Login} history={history} />}
        />
        <AuthenticatedRoute
          isAuthenticated={isAccountVerified}
          path="/info"
          component={() => <AkosPage component={Info} history={history} />}
        />
        <AuthenticatedRoute
          isAuthenticated={ssnVerified && ssnVerified.data !== false}
          path="/info-ssn"
          component={() => <AkosPage component={InfoSsn} history={history} />}
        />
        <Route
          path="/login"
          component={() => (
            <AkosPage component={MedClinicLogin} history={history} />
          )}
        />

        <AuthenticatedRoute
          path="/faq"
          isAuthenticated={isAuthenticated}
          component={() => <AkosPage component={Faq} history={history} />}
        />
        <AuthenticatedRoute
          isAuthenticated={signin.data}
          path="/2fa"
          exact
          component={() => <AkosPage component={TwoFactor} history={history} />}
        />
        <AuthenticatedRoute
          isAuthenticated={signin.data}
          path="/2fa/:id"
          component={() => <AkosPage component={TwoFactor} history={history} />}
        />
        <Route
          path="/account-verification"
          exact
          component={() => (
            <AkosPage component={AccountVerification} history={history} />
          )}
        />
        <Route
          path="/verify"
          exact
          component={() => (
            <AkosPage component={AccountVerification} history={history} />
          )}
        />
        <Route
          path="/verify2"
          exact
          component={() => (
            <AkosPage component={AccountVerificationSsn} history={history} />
          )}
        />
        <Route
          path="/verify2/:id"
          exact
          component={() => (
            <AkosPage component={AccountVerificationSsn} history={history} />
          )}
        />
        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          path="/pharmacy"
          component={() => <AkosPage component={RxSpark} history={history} />}
        />
        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          path="/360"
          component={() => <AkosPage component={Inxite} history={history} />}
        />
        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          path="/benefits"
          component={() => <AkosPage component={Benefits} history={history} />}
        />
        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          path="/create-appointment"
          component={() => (
            <AkosPage component={Appointment} history={history} />
          )}
        />
        {isVdpc && (
          <AuthenticatedRoute
            isAuthenticated={isAuthenticated}
            path="/video-conference"
            exact
            component={() => (
              <AkosPage component={Conf} history={history} fullScreen />
            )}
          />
        )}
        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          path="/video-conference/:id"
          exact
          component={() => (
            <AkosPage component={Conf} history={history} fullScreen />
          )}
        />

        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          path="/covid-assessment"
          component={() => (
            <AkosPage
              component={Conf}
              roomName="workerscomp"
              history={history}
              fullScreen
            />
          )}
        />
        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          path="/change-password"
          component={() => (
            <AkosPage component={ChangePassword} history={history} />
          )}
        />
        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          path="/connect"
          component={() => (
            <AkosPage component={PaymentOptions} history={history} />
          )}
        />
        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          exact
          path="/chatroom"
          component={ChatRoom}
        />

        <Route
          path="/chatroom/nurse"
          exact
          component={() => (
            <AkosPage component={NurseWaitingRoom} history={history} />
          )}
        />
        <Route path="/chatroom/nurse/:id" component={NurseToMemberChat} />
        <AuthenticatedRoute
          isAuthenticated={isAuthenticated}
          path="/my-account"
          component={() => <AkosPage component={MyAccount} history={history} />}
        />
        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
      {/* </Router> */}
    </div>
  );
}

const { object } = PropTypes;

App.propTypes = {
  signin: object,
  auth: object,
  accountVerification: object,
  ssnVerified: object,
  history: object,
};

const mapStateToProps = createStructuredSelector({
  auth: makeSelectVerifyAuth(),
  signin: makeSelectSignin(),
  ssnVerified: makeSelectVerifySsn(),
  accountVerification: makeAccountVerification(),
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(App);
