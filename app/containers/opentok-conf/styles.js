import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  rootContainer: {
    paddingLeft: '0px',
    paddingRight: '0px',
    [theme.breakpoints.down('md')]: {
      marginTop: -60,
    },
  },
  text: {
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
  controls: {
    marginTop: '-2.5rem',
    paddingBottom: '2rem',
    height: 'auto',
    zIndex: theme.zIndex.drawer + 1,
    position: 'fixed',
    padding: '1rem',
    display: 'flex',
    left: 0,
    bottom: '0',
    width: '100%',
    color: 'white',

    justifyContent: 'center',
  },
  video: {
    height: '50%',
  },
  videoRoot: {
    marginTop: '10px',
    width: '100%',
    height: '100%',
    '& > :first-child': {
      display: 'flex',
      height: '100%',
      flexWrap: 'wrap',
      '& > :nth-child(1):nth-last-child(2), & > :nth-child(2):nth-last-child(1)': {
        [theme.breakpoints.down('md')]: {
          margin: '0px',
          flex: '1 0 100%',
          marginBottom: '1px',
        },
      },
      '& > div': {
        flex: '1 0 45%',
        margin: '1px',
        height: 'auto',
        '& > div': {
          transitionProperty: 'all',
          transitionDuration: '0.5s',
          height: '100%',
          [theme.breakpoints.down('md')]: {
            marginBottom: '1px',
          },
        },
      },
    },
    paddingBottom: '1px',
  },
  publisher: {
    width: '400px',
    height: '250px',
    display: 'block',
    position: 'absolute',
    borderRadius: '5px',
    zIndex: 99,
    transition: 'width 200ms, height 200ms ease',
    bottom: '8px',
    left: '10px',
    '& > div': {
      height: '95%',
      width: '70%',
    },
    [theme.breakpoints.down('md')]: {
      width: '30%',
      height: '15%',
      left: '5px',
      bottom: '0px',
    },
  },
  publisherFull: {
    width: '100%',
    height: '50vh',
    display: 'block',
    [theme.breakpoints.down('md')]: {
      height: '75vh',
    },
    '& > div': {
      height: '100%',
      width: '100%',
    },
  },
  parentContainer: {
    width: '100%',
    height: '81vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
    [theme.breakpoints.down('md')]: {
      height: '95vh',
    },
  },
  loadingText: {
    marginTop: '5%',
  },
  quitting: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
  },
}));

export default useStyles;
