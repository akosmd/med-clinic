import React, { useState, useEffect, Fragment } from 'react';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';

import KeyboardEventHandler from 'react-keyboard-event-handler';

import { connect } from 'react-redux';
import { compose } from 'redux';
import moment from 'moment';
import {
  Grid,
  Typography,
  CircularProgress,
  AppBar,
  Tabs,
  Tab,
  Box,
  Button,
  FormControl,
  InputLabel,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { createStructuredSelector } from 'reselect';

import TextField from 'components/TextField';

import GradientButton from 'components/GradientButton';
import SignatureCanvas from 'react-signature-canvas';

import SelectField from 'components/Select';
import StatesSelect from 'components/StatesSelect';

import {
  handleChange,
  handleEmailChange,
  setValuesFrom,
  extractFormValues,
  handleEqualityChange,
  highlightFormErrors,
} from 'utils/formHelper';

import {
  getPatientDetail,
  updatePatient,
  logoutUser,
  resetUpdatedPatient,
  getPatientDependents,
} from 'containers/App/actions';

import ConsentForm from 'components/Consent';
import {
  makeSelectPatientDetail,
  makeSelectVerifyAuth,
  makeSelectUpdatedPatient,
  makePatientDependents,
  makeSelectInxiteSso,
} from 'containers/App/selectors';

import model, { passwordModel } from './model';

const useStyles = makeStyles({
  formContainer: {
    width: '100%',
  },
  loginLink: {
    marginLeft: '1rem',
    textDecoration: 'none !important',
    '& hover': {
      textDecoration: 'none !important',
    },
  },
  scroll: {
    overflow: 'auto',
    maxHeight: '300px',
    border: '1px solid #d9d9d9',
    borderRadius: '5px',
    margin: '1rem',
  },
  marginBottom: {
    marginBottom: '1rem',
  },
  signingPad: {
    border: '1px solid #b7b7b7',
    padding: '1rem',
    borderRadius: '4px',
    width: '100%',
    height: '200px',
  },
  signingLabel: {
    padding: '1rem',
  },
  sigControls: {
    display: 'flex',
    justifyContent: 'space-between',
  },
});

function a11yProps(index) {
  return {
    key: index,
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

export function AccountInfo({
  dispatch,
  doGetPatientDetail,
  doUpdatePatient,
  doLogoutUser,
  doResetUpdatedPatient,
  enqueueSnackbar,
  account,
  updatedPatient,
  auth,
  dependents,
  inxiteSso,
  doGetPatientDependents,
}) {
  const [profile, setProfile] = useState({ ...model });
  const [password, setPassword] = useState({ ...passwordModel });
  const [allProfiles, setAllProfiles] = useState([]);
  const [readConsent, setReadConsent] = useState(false);
  const [tabIndex, setTabIndex] = React.useState(0);

  let sigPad = {};

  const handleChangeTab = (event, newValue) => {
    setTabIndex(newValue);
    setProfile(allProfiles[newValue]);
  };

  useEffect(() => {
    if (auth.data) {
      dispatch(doGetPatientDetail(auth.data.patient.id));
      dispatch(doGetPatientDependents(auth.data.patient.id));
    }
  }, [auth]);

  useEffect(() => {
    if (account.data) {
      const fieldsWithValues = setValuesFrom(account.data, profile, true);
      setProfile({ ...fieldsWithValues });
    }
  }, [account]);

  useEffect(() => {
    const { data } = dependents;
    if (data) {
      let newDependents = data.map(() => ({ ...model }));
      newDependents = newDependents.map((item, index) => ({
        ...setValuesFrom(data[index], item, true),
      }));

      setAllProfiles([profile, ...newDependents]);
    }
  }, [dependents]);

  useEffect(() => {
    if (updatedPatient.data) {
      enqueueSnackbar('Account successfully verified. Please wait.', {
        variant: 'success',
      });
      setTimeout(() => {
        dispatch(doResetUpdatedPatient());
        dispatch(doLogoutUser());
        dispatch(push('/login'));
      }, 3000);
    }
  }, [updatedPatient]);

  const classes = useStyles();

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: profile,
      event,
      saveStepFunc: setProfile,
    });
  };

  const onEmailChange = field => event => {
    handleEmailChange({
      field: field.id,
      state: profile,
      event,
      saveStepFunc: setProfile,
    });
  };

  const onPasswordChange = () => event => {
    handleEqualityChange({
      field1: password.password,
      field2: password.passwordConfirmation,
      state: password,
      event,
      saveStepFunc: setPassword,
    });
  };

  const onSigned = () => {
    const signature = {
      ...model.signature,
      dateStamp: moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
      base64: sigPad.getTrimmedCanvas().toDataURL('image/png'),
      filename: `signature-${Date.now()}.png`,
      filetype: 'image/png',
    };
    setProfile({
      ...profile,
      signature,
      signatureFullName: { ...profile.signatureFullName, error: false },
    });
  };

  const clear = () => {
    const hasFullName = profile.signatureFullName.value !== '';
    setProfile({
      ...profile,
      signature: model.signature,
      signatureFullName: { ...profile.signatureFullName, error: !hasFullName },
    });
    sigPad.clear();
  };

  const handleSubmit = () => {
    const hasSigned =
      profile.signature.base64 || profile.signatureFullName.value !== '';

    if (!password.completed || !hasSigned) {
      highlightFormErrors(password, setPassword);
      highlightFormErrors(profile, setProfile);
      enqueueSnackbar('Please fill out all fields marked with asterisk (*)', {
        variant: 'error',
      });
    } else {
      const { password: pw, passwordConfirmation } = extractFormValues(
        password,
      );
      const formVals = extractFormValues(profile);
      const { signatureFullName, signature } = formVals;

      const params = {
        id: account.data.id,
        password: pw,
        passwordConfirmation,
        signature: signature.base64 ? signature : undefined,
        signatureFullName:
          signature.base64 === null ? signatureFullName : undefined,
      };
      dispatch(doUpdatePatient(params));
    }
  };

  const renderInfo = field => (
    <Grid container spacing={3} justify="center" alignItems="center">
      <Grid item xs={12} md={6}>
        <KeyboardEventHandler handleKeys={['enter']} onKeyEvent={handleSubmit}>
          <TextField
            field={field.firstName}
            variant="outlined"
            onChange={onInputChange(field.firstName)}
          />
        </KeyboardEventHandler>
      </Grid>
      <Grid item xs={12} md={6}>
        <KeyboardEventHandler handleKeys={['enter']} onKeyEvent={handleSubmit}>
          <TextField
            field={field.lastName}
            variant="outlined"
            onChange={onInputChange(field.lastName)}
          />
        </KeyboardEventHandler>
      </Grid>
      <Grid item xs={12} md={4}>
        <KeyboardEventHandler handleKeys={['enter']} onKeyEvent={handleSubmit}>
          <TextField
            field={{
              ...field.birthDateAt,
              value: moment(field.birthDateAt.value).format('MM/DD/YYYY'),
            }}
            variant="outlined"
            onChange={onInputChange(field.birthDateAt)}
          />
        </KeyboardEventHandler>
      </Grid>
      <Grid item xs={12} md={4}>
        <KeyboardEventHandler handleKeys={['enter']} onKeyEvent={handleSubmit}>
          <TextField
            field={field.email}
            variant="outlined"
            type="email"
            onChange={onEmailChange(field.email)}
          />
        </KeyboardEventHandler>
      </Grid>
      <Grid item xs={12} md={4}>
        <KeyboardEventHandler handleKeys={['enter']} onKeyEvent={handleSubmit}>
          <TextField
            field={field.employerCode}
            variant="outlined"
            onChange={onInputChange(field.employerCode)}
          />
        </KeyboardEventHandler>
      </Grid>

      <Grid item xs={12} md={6}>
        <KeyboardEventHandler handleKeys={['enter']} onKeyEvent={handleSubmit}>
          <TextField
            field={field.phoneNumber}
            variant="outlined"
            onChange={onInputChange(field.phoneNumber)}
          />
        </KeyboardEventHandler>
      </Grid>
      <Grid item xs={12} md={6}>
        <KeyboardEventHandler handleKeys={['enter']} onKeyEvent={handleSubmit}>
          <SelectField
            field={field.genderId}
            options={[
              { name: '', value: '' },
              { name: 'Male', value: '12000' },
              { name: 'Female', value: '12001' },
            ]}
            variant="outlined"
            onChange={onInputChange(field.genderId)}
          />
        </KeyboardEventHandler>
      </Grid>
      <Grid item xs={12} md={6}>
        <KeyboardEventHandler handleKeys={['enter']} onKeyEvent={handleSubmit}>
          <TextField
            field={field.address1}
            variant="outlined"
            onChange={onInputChange(field.address1)}
          />
        </KeyboardEventHandler>
      </Grid>
      <Grid item xs={12} md={6}>
        <KeyboardEventHandler handleKeys={['enter']} onKeyEvent={handleSubmit}>
          <TextField
            field={field.city}
            variant="outlined"
            onChange={onInputChange(field.city)}
          />
        </KeyboardEventHandler>
      </Grid>
      <Grid item xs={12} md={6}>
        <KeyboardEventHandler handleKeys={['enter']} onKeyEvent={handleSubmit}>
          <StatesSelect
            field={field.state}
            variant="outlined"
            onChange={onInputChange(field.state)}
          />
        </KeyboardEventHandler>
      </Grid>
      <Grid item xs={12} md={6}>
        <KeyboardEventHandler handleKeys={['enter']} onKeyEvent={handleSubmit}>
          <TextField
            field={field.zipCode}
            variant="outlined"
            onChange={onInputChange(field.zipCode)}
          />
        </KeyboardEventHandler>
      </Grid>
    </Grid>
  );

  return (
    <Fragment>
      <Grid container spacing={2} direction="column">
        <Grid item xs={12} className={classes.marginBottom}>
          <Typography variant="h4" color="primary">
            Information
            <Typography variant="body1" color="textPrimary">
              {
                'To update your information, please contact your Health Plan Administrator'
              }
              .
            </Typography>
          </Typography>
        </Grid>
        <Grid item xs={12} md={6} className={classes.formContainer}>
          {dependents.data && dependents.data.length > 0 && (
            <AppBar position="static" color="default" elevation={0}>
              <Tabs
                value={tabIndex}
                onChange={handleChangeTab}
                indicatorColor="primary"
                textColor="primary"
                variant="scrollable"
                scrollButtons="auto"
              >
                <Tab label="You" {...a11yProps(0)} />
                {dependents.data.map((dependent, index) => (
                  <Tab
                    label={`${dependent.firstName}`}
                    {...a11yProps(index + 1)}
                  />
                ))}
              </Tabs>
            </AppBar>
          )}
          <Typography component="div" role="tabpanel">
            <Box p={3}>{renderInfo(profile)}</Box>
          </Typography>
        </Grid>

        <Grid item xs={12} md={6}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Typography variant="h6">Set your Password</Typography>
            </Grid>
            <Grid item xs={12} md={6}>
              <KeyboardEventHandler
                handleKeys={['enter']}
                onKeyEvent={handleSubmit}
              >
                <TextField
                  field={password.password}
                  type="password"
                  variant="outlined"
                  onChange={onPasswordChange(password.password)}
                />
              </KeyboardEventHandler>
            </Grid>
            <Grid item xs={12} md={6}>
              <KeyboardEventHandler
                handleKeys={['enter']}
                onKeyEvent={handleSubmit}
              >
                <TextField
                  field={password.passwordConfirmation}
                  variant="outlined"
                  type="password"
                  onChange={onPasswordChange(password.passwordConfirmation)}
                />
              </KeyboardEventHandler>
            </Grid>
          </Grid>
        </Grid>

        <Grid item>
          <ConsentForm
            profile={profile}
            readConsent={readConsent}
            handleChange={handleChange}
            setReadConsent={setReadConsent}
            setProfile={setProfile}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="password" className={classes.signingLabel}>
              Signature
            </InputLabel>
            <SignatureCanvas
              penColor="black"
              onEnd={onSigned}
              ref={ref => {
                sigPad = ref;
              }}
              canvasProps={{
                className: classes.signingPad,
              }}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12} md={6} className={classes.sigControls}>
          <Button
            variant="contained"
            color="primary"
            onClick={clear}
            disabled={!profile.signature.base64}
          >
            Clear Signature
          </Button>
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            field={profile.signatureFullName}
            variant="outlined"
            onChange={onInputChange(profile.signatureFullName)}
          />
        </Grid>
        <Grid item xs={12} md={3}>
          <GradientButton
            variant="contained"
            size="large"
            onClick={handleSubmit}
            disabled={!readConsent || !profile.consent.value}
          >
            {updatedPatient.saving ? (
              <CircularProgress color="secondary" />
            ) : (
              'Continue'
            )}
          </GradientButton>
        </Grid>
      </Grid>
      {inxiteSso.data && (
        <iframe
          id="inxiteFrame"
          title="Inxite"
          src={inxiteSso.data.sso_url}
          style={{ display: 'none' }}
        />
      )}
    </Fragment>
  );
}

const { object, func } = PropTypes;
AccountInfo.propTypes = {
  enqueueSnackbar: func.isRequired,
  dispatch: func.isRequired,
  doGetPatientDetail: func.isRequired,
  doUpdatePatient: func.isRequired,
  doLogoutUser: func.isRequired,
  doResetUpdatedPatient: func.isRequired,
  auth: object.isRequired,
  updatedPatient: object.isRequired,
  account: object.isRequired,
  dependents: object.isRequired,
  inxiteSso: object.isRequired,
  doGetPatientDependents: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  account: makeSelectPatientDetail(),
  auth: makeSelectVerifyAuth(),
  updatedPatient: makeSelectUpdatedPatient(),
  dependents: makePatientDependents(),
  inxiteSso: makeSelectInxiteSso(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doGetPatientDetail: getPatientDetail,
    doUpdatePatient: updatePatient,
    doLogoutUser: logoutUser,
    doResetUpdatedPatient: resetUpdatedPatient,
    doGetPatientDependents: getPatientDependents,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(AccountInfo);
