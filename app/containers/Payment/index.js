import React from 'react';

import PropTypes from 'prop-types';

import Cash from './cash';
import Insurance from './insurance';
function Payment(props) {
  const { cash } = props;
  if (cash) return <Cash />;
  return <Insurance />;
}

const { bool } = PropTypes;
Payment.propTypes = {
  cash: bool,
};

export default Payment;
