/* eslint-disable camelcase */
import React, { useState, useEffect } from 'react';
import ImageUploader from 'components/ImageUploader';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Resizer from 'react-image-file-resizer';

import ImageViewer from 'components/ImageViewer';
import DialogBox from 'components/DialogBox';
import WebCam from 'components/WebCam';

import { createStructuredSelector } from 'reselect';
import { Grid, Typography, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import GradientButton from 'components/GradientButton';

import {
  highlightFormErrors,
  extractFormValues,
  checkStatus,
} from 'utils/formHelper';

import { resetPaymentCheckout } from 'containers/App/legacyActions';

import { saveIdDocument, getSavedImages } from 'containers/App/actions';

import { makeSelectPatientCallId } from 'containers/App/legacySelectors';

import {
  makeSelectGetSavedImages,
  makeSelectSaveDocumentToPs,
} from 'containers/App/selectors';

import { PAYMENT_MODES } from 'utils/config';

import { covidNoInsuranceDetails } from './model';

const useStyles = makeStyles({
  formContainer: {
    width: '100%',
  },
  loginLink: {
    marginLeft: '1rem',
  },
  error: {
    border: '2px solid red',
    borderRadius: '5px',
    padding: '1rem',
  },
});

export function NoInsurance({
  dispatch,
  patient,
  amount,
  savedImages,
  enqueueSnackbar,
  doSaveIdDocument,
  onConferenceClick,
  doGetSavedImages,
  buttonCaption,
  callId,
  postImages,
  doResetPaymentCheckout,
  history,
}) {
  const [insurance, setInsurance] = useState({ ...covidNoInsuranceDetails });

  const [openCam, setOpenCam] = useState({
    status: false,
    Action: false,
    title: '',
  });

  const showPayment = false;
  const classes = useStyles();

  useEffect(() => {
    dispatch(doResetPaymentCheckout());
    dispatch(doGetSavedImages(patient.id));
  }, []);

  useEffect(() => {
    const { error } = postImages;
    if (error) {
      if (error === 'Payment Submitted') onConferenceClick();
      else
        enqueueSnackbar(
          typeof error === 'string'
            ? error
            : 'Something went wrong. Please try again.',
          {
            variant: 'error',
          },
        );
    }
  }, [postImages]);

  useEffect(() => {
    if (savedImages.data) {
      const { front_government_id, back_government_id } = savedImages.data;

      setInsurance(prevState => {
        const newState = {
          ...prevState,
          governmentIdImageFront: {
            ...insurance.governmentIdImageFront,
            base64: front_government_id,
            error: !front_government_id,
          },
          governmentIdImageBack: {
            ...insurance.governmentIdImageBack,
            base64: back_government_id,
            error: !back_government_id,
          },
        };
        const formCompleted = !checkStatus({
          source: newState,
          currentField: '',
        });
        return {
          ...newState,
          completed: formCompleted,
        };
      });
    }
  }, [savedImages]);

  const handleSaveInsurance = () => {
    if (!insurance.completed) {
      highlightFormErrors(insurance, setInsurance);
      enqueueSnackbar('Please fill out all fields marked with asterisk *', {
        variant: 'error',
      });
    } else {
      saveImages();
    }
  };

  const saveImages = () => {
    const { uuid, id } = patient;
    const attachments = [];
    const { governmentIdImageFront, governmentIdImageBack } = extractFormValues(
      insurance,
    );

    if (
      governmentIdImageFront.base64 &&
      !governmentIdImageFront.base64.includes('https://')
    ) {
      attachments.push({
        patientUuid: uuid,
        documentType: 'ID',
        title: `front_government_id`,
        dataType: 'JPG/PDF',
        filename: 'front_government_id.jpg',
        data: governmentIdImageFront.base64,
      });
    }
    if (
      governmentIdImageBack.base64 &&
      !governmentIdImageBack.base64.includes('https://')
    ) {
      attachments.push({
        patientUuid: uuid,
        documentType: 'ID',
        title: `back_government_id`,
        dataType: 'JPG/PDF',
        filename: 'back_government_id.jpg',
        data: governmentIdImageBack.base64,
      });
    }

    const params = {
      planMemberId: id,
      paymentModeId: PAYMENT_MODES.PAID_BY_GOVERNMENT,
      amount,
      callId: callId.data.result.call_id,
      attachments,
    };
    if (attachments.length > 0) dispatch(doSaveIdDocument(params));
    else onConferenceClick();
  };

  const b64toBlob = dataURI => {
    const byteString = atob(dataURI.split(',')[1]);
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);

    for (let i = 0; i < byteString.length; i += 1) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: 'image/jpeg' });
  };

  const handleSetImage = field => val => {
    const error = val === undefined;
    const formCompleted = !(
      error ||
      checkStatus({
        source: insurance,
        currentField: field,
      })
    );
    if (val) {
      Resizer.imageFileResizer(
        b64toBlob(val),
        320,
        320,
        'JPEG',
        100,
        0,
        uri => {
          setInsurance({
            ...insurance,
            [field]: {
              ...insurance[field],
              base64: uri || undefined,
              filename: uri ? `${field}-${Date.now()}.png` : undefined,
              filetype: uri ? 'image/png' : undefined,
              pristine: false,
              error,
            },
            completed: formCompleted,
          });
        },
        'base64',
      );
    } else {
      setInsurance({
        ...insurance,
        [field]: {
          ...insurance[field],
          base64: val || undefined,
          filename: val ? `${field}-${Date.now()}.png` : undefined,
          filetype: val ? 'image/png' : undefined,
          pristine: false,
          error,
        },
        completed: formCompleted,
      });
    }
  };

  const company = history.location.pathname.includes('covid-nj')
    ? 'Agile Urgent Care'
    : 'Akos';
  return (
    <div>
      {!showPayment && (
        <Grid container spacing={2} direction="column">
          <Grid item xs={12}>
            <Typography variant="h4" color="primary">
              Details
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography color="secondary">
              {'"'}I attest that I do not have insurance and need government
              assistance to cover the cost of my COVID-19 testing. By
              proceeding, I{"'"}m agreeing that {company} will verify this fact
              on my behalf to ensure that I do not have individual or
              employer-sponsored Medicare or Medicaid coverage, and no other
              payer can reimburse {company} for my COVID-19 testing.{'"'}
            </Typography>
            <br />
            <Typography>
              In order to expedite this verification process, please upload your
              government-issued photo ID.
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Grid container spacing={3} justify="center" alignItems="center">
              <Grid item xs={12} md={6}>
                <div
                  className={
                    !insurance.governmentIdImageFront.pristine &&
                    insurance.governmentIdImageFront.error
                      ? classes.error
                      : ''
                  }
                >
                  <Typography>
                    Front photo of Government Issued ID/Driver License*
                  </Typography>
                  {insurance.governmentIdImageFront.base64 ? (
                    <ImageViewer
                      src={insurance.governmentIdImageFront.base64}
                      height={248}
                      handleDelete={() =>
                        handleSetImage('governmentIdImageFront')()
                      }
                      handleRetake={() =>
                        setOpenCam({
                          status: true,
                          action: val =>
                            handleSetImage('governmentIdImageFront')(val),
                          title: 'Capture Front Photo of your ID',
                        })
                      }
                      loaderHeight={364}
                      loading={savedImages.loading}
                    />
                  ) : (
                    <ImageUploader
                      handleDrop={val =>
                        handleSetImage('governmentIdImageFront')(val)
                      }
                      initCamera={() =>
                        setOpenCam({
                          status: true,
                          action: val =>
                            handleSetImage('governmentIdImageFront')(val),
                          title: 'Capture Front Photo of your Government ID',
                        })
                      }
                      loaderHeight={364}
                      loading={savedImages.loading}
                    />
                  )}
                </div>
              </Grid>
              <Grid item xs={12} md={6}>
                <div
                  className={
                    !insurance.governmentIdImageBack.pristine &&
                    insurance.governmentIdImageBack.error
                      ? classes.error
                      : ''
                  }
                >
                  <Typography>
                    Back photo of Government Issued ID/Driver License*
                  </Typography>
                  {insurance.governmentIdImageBack.base64 ? (
                    <ImageViewer
                      src={insurance.governmentIdImageBack.base64}
                      height={248}
                      handleDelete={() =>
                        handleSetImage('governmentIdImageBack')()
                      }
                      handleRetake={() => {
                        setOpenCam({
                          status: true,
                          action: val =>
                            handleSetImage('governmentIdImageBack')(val),
                          title: 'Capture Back Photo of your Government ID',
                        });
                      }}
                      loaderHeight={364}
                      loading={savedImages.loading}
                    />
                  ) : (
                    <ImageUploader
                      handleDrop={val =>
                        handleSetImage('governmentIdImageBack')(val)
                      }
                      initCamera={() =>
                        setOpenCam({
                          status: true,
                          action: val =>
                            handleSetImage('governmentIdImageBack')(val),
                          title: 'Capture Back Photo of your ID',
                        })
                      }
                      loaderHeight={364}
                      loading={savedImages.loading}
                    />
                  )}
                </div>
              </Grid>
              <DialogBox
                status={openCam.status}
                handleClose={() => setOpenCam({ status: false })}
                title={openCam.title}
              >
                <WebCam
                  handleCapture={openCam.action}
                  handleClose={() => setOpenCam({ status: false })}
                  containerHeight="100vh"
                />
              </DialogBox>
            </Grid>
          </Grid>
          <Grid item xs={12} md={6}>
            <GradientButton
              variant="contained"
              size="large"
              onClick={handleSaveInsurance}
            >
              {postImages.loading ? (
                <CircularProgress color="secondary" />
              ) : (
                'Continue to take assessment'
              )}
            </GradientButton>
          </Grid>
        </Grid>
      )}
      <Grid container spacing={2}>
        <Grid item xs={12}>
          {showPayment && (
            <GradientButton size="large" onClick={onConferenceClick}>
              {buttonCaption}
            </GradientButton>
          )}
        </Grid>
      </Grid>
    </div>
  );
}

const { func, object, number, string } = PropTypes;
NoInsurance.propTypes = {
  patient: object.isRequired,
  amount: number,
  enqueueSnackbar: func.isRequired,
  dispatch: func.isRequired,
  onConferenceClick: func.isRequired,
  doSaveIdDocument: func.isRequired,
  buttonCaption: string,
  savedImages: object.isRequired,
  doGetSavedImages: func.isRequired,
  callId: object.isRequired,
  postImages: object.isRequired,
  doResetPaymentCheckout: func.isRequired,
  history: object.isRequired,
};

NoInsurance.defaultProps = {
  buttonCaption: 'Video Conference Now',
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doGetSavedImages: getSavedImages,
    doSaveIdDocument: saveIdDocument,
    doResetPaymentCheckout: resetPaymentCheckout,
  };
}

const mapStateToProps = createStructuredSelector({
  savedImages: makeSelectGetSavedImages(),
  callId: makeSelectPatientCallId(),
  postImages: makeSelectSaveDocumentToPs(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(NoInsurance);
