import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { compose } from 'redux';

import AkosCard from 'components/AkosCard';
// import AppointmentIcon from 'components/icons/appointment';
import AssessmentIcon from 'components/icons/assessment';
import CovidIcon from 'components/icons/covid';
import RecordsIcon from 'components/icons/records';
import PharmacyIcon from 'components/icons/pharmacy';
import BenefitsIcon from 'components/icons/benefits';
import DependentIcon from 'components/icons/dependent';
import MedicationsIcon from 'components/icons/medications';
import MessageIcon from 'components/icons/message';
import ProviderIcon from 'components/icons/provider';
import FAQICon from 'components/icons/faq';

import { Grid } from '@material-ui/core';

// import RxSpark from 'containers/RxSpark';

import {
  setInxiteUrl,
  initializeInxiteSso,
  getPatientDependents,
  showDependentsModal,
} from 'containers/App/actions';

import { generatePatientCallId } from 'containers/App/legacyActions';

import {
  makeSelectVerifyAuth,
  makeSelectInxiteSso,
  makeSelectInxiteUrl,
  makePatientImpersonation,
  makeShowDependentsModal,
  makeSelectActualPatient,
  makeSelectVideoConfStatus,
} from 'containers/App/selectors';

import { makeSelectPatientCallId } from 'containers/App/legacySelectors';

import { INXITE_SSO_URL, MEDICATION_LOOK_UP_URL } from 'utils/config';

function VdpcButtons({
  dispatch,
  doSetUrl,
  doInitInxiteSso,
  account,
  patient,
  inxiteSso,
  doGetPatientDependents,
  videoConfStatus,
}) {
  // const [showPharmacy, setShowPharmacy] = useState(false);
  const [inxiteLoaded, setInxiteLoaded] = useState(false);

  useEffect(() => {
    if (patient.error) {
      dispatch(push('/login'));
    } else if (
      patient.data &&
      (videoConfStatus.data || videoConfStatus.error)
    ) {
      const { patient: detail } = patient.data;
      const { uuid } = detail;
      setInxiteLoaded(false);
      initiateInxiteSSO(uuid);
    }
  }, [videoConfStatus]);

  useEffect(() => {
    if (account.data) {
      dispatch(doGetPatientDependents(account.data.patient.id));
    }
  }, [account]);

  const initiateInxiteSSO = uuid => {
    dispatch(doInitInxiteSso(uuid));
  };

  const onInxiteClick = url => {
    dispatch(doSetUrl(url));
    dispatch(push('/360'));
  };

  const onInxiteLoaded = () => {
    if (!inxiteLoaded) {
      setInxiteLoaded(true);
    }
  };

  return (
    <Grid container spacing={2} alignItems="stretch">
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="COVID-19 Assessment"
          icon={CovidIcon}
          onClick={() => dispatch(push('/covid19'))}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Health Risk Assessment"
          icon={AssessmentIcon}
          loading={!inxiteLoaded}
          onClick={() =>
            onInxiteClick(
              `${INXITE_SSO_URL}assessments/${
                inxiteSso.data.inxite_patient_id
              }`,
            )
          }
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Talk to a Provider"
          icon={ProviderIcon}
          onClick={() => dispatch(push('/medical-history'))}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Medication Lookup"
          icon={PharmacyIcon}
          href={MEDICATION_LOOK_UP_URL}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Medical Records"
          icon={RecordsIcon}
          loading={!inxiteLoaded}
          onClick={() =>
            onInxiteClick(
              `${INXITE_SSO_URL}unifiedrecord/${
                inxiteSso.data.inxite_patient_id
              }`,
            )
          }
        />
      </Grid>
      {/* <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="My Appointments"
          icon={AppointmentIcon}
          onClick={() => dispatch(push('/create-appointment'))}
        />
      </Grid> */}
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="My Medications"
          icon={MedicationsIcon}
          loading={!inxiteLoaded}
          onClick={() =>
            onInxiteClick(
              `${INXITE_SSO_URL}unifiedrecord/${
                inxiteSso.data.inxite_patient_id
              }#medications`,
            )
          }
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="FAQs"
          icon={FAQICon}
          onClick={() => dispatch(push('/faq'))}
        />
      </Grid>

      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Messages"
          icon={MessageIcon}
          loading={!inxiteLoaded}
          onClick={() => onInxiteClick(`${INXITE_SSO_URL}messages/view`)}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Benefits Wallet"
          icon={BenefitsIcon}
          onClick={() => dispatch(push('/benefits'))}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Add/Edit Dependents"
          icon={DependentIcon}
          onClick={() => dispatch(push('my-account'))}
        />
      </Grid>

      <Grid item xs={12} sm={6} md={6} lg={9} xl={9}>
        {inxiteSso.data && (
          <iframe
            id="inxiteFrame"
            title="Inxite"
            src={inxiteSso.data.sso_url}
            style={{ display: 'none' }}
            onLoad={onInxiteLoaded}
          />
        )}
      </Grid>
    </Grid>
  );
}

const { func, object } = PropTypes;
VdpcButtons.propTypes = {
  dispatch: func.isRequired,
  doSetUrl: func.isRequired,
  doInitInxiteSso: func.isRequired,
  doGetPatientDependents: func.isRequired,
  inxiteSso: object.isRequired,
  account: object.isRequired,
  patient: object.isRequired,
  videoConfStatus: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  patient: makeSelectActualPatient(),
  account: makeSelectVerifyAuth(),
  inxiteSso: makeSelectInxiteSso(),
  inxiteUrl: makeSelectInxiteUrl(),
  impersonatedPatient: makePatientImpersonation(),
  impersonationShowModal: makeShowDependentsModal(),
  callId: makeSelectPatientCallId(),
  videoConfStatus: makeSelectVideoConfStatus(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSetUrl: setInxiteUrl,
    doInitInxiteSso: initializeInxiteSso,
    doGetPatientDependents: getPatientDependents,
    doShowDependentsModal: showDependentsModal,
    doGeneratePatientCallId: generatePatientCallId,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(VdpcButtons);
