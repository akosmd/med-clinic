/**
 *
 * StatesSelect
 *
 */

import React from 'react';

import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import KeyboardEventHandler from 'react-keyboard-event-handler';

import PropTypes from 'prop-types';

import states from 'utils/stateList.json';

function StatesSelect({ onChange, field, className, keyEvents }) {
  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);
  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  const getStates = () =>
    states.map(state => (
      <option value={state.abbreviation} key={state.abbreviation}>
        {state.name}
      </option>
    ));
  if (!field) return null;

  const { error, pristine, readOnly } = field;
  return !keyEvents ? (
    <FormControl
      margin="normal"
      required={field.required}
      fullWidth
      variant="outlined"
      className={className}
      error={error && !pristine}
    >
      <InputLabel
        htmlFor={field.id}
        ref={inputLabel}
        error={error && !pristine}
      >
        {field.error && !pristine
          ? field.errorMessage || field.caption
          : field.caption}
      </InputLabel>
      <Select
        native
        labelWidth={labelWidth}
        disabled={readOnly}
        value={field.value !== null ? field.value : ''}
        id={field.id}
        name={field.name}
        onChange={onChange}
      >
        {getStates()}
      </Select>
    </FormControl>
  ) : (
    <KeyboardEventHandler
      handleKeys={keyEvents.handleKeys}
      onKeyEvent={keyEvents.onKeyEvent}
    >
      <FormControl
        margin="normal"
        required={field.required}
        fullWidth
        variant="outlined"
        className={className}
        error={error && !pristine}
      >
        <InputLabel
          htmlFor={field.id}
          ref={inputLabel}
          error={error && !pristine}
        >
          {field.error && !pristine
            ? field.errorMessage || field.caption
            : field.caption}
        </InputLabel>
        <Select
          native
          labelWidth={labelWidth}
          disabled={readOnly}
          value={field.value !== null ? field.value : ''}
          id={field.id}
          name={field.name}
          onChange={onChange}
        >
          {getStates()}
        </Select>
      </FormControl>
    </KeyboardEventHandler>
  );
}

const { string, bool, shape, func } = PropTypes;
StatesSelect.propTypes = {
  field: shape({
    id: string.isRequired,
    value: string,
    caption: string.isRequired,
    errorMessage: string,
    placeholder: string,
    error: bool,
    required: bool,
    fullWidth: bool,
    autoFocus: bool,
  }),
  onChange: func,
  className: string,
};

StatesSelect.defaultProps = {
  onChange: undefined,
  className: undefined,
};

export default StatesSelect;
