/**
 *
 * Faq
 *
 */

import React from 'react';

import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  container: {
    paddingBottom: 50,
  },
});

function Faq() {
  const classes = useStyles();
  return (
    <Grid
      className={classes.container}
      item
      container
      spacing={2}
      xs={12}
      sm={12}
      md={8}
      lg={10}
    >
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h4" color="textPrimary">
          AKOS FAQs
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          GENERAL
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          What is Akos?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          Akos is a revolutionary telemedicine company providing patients
          virtual access to board-certified physicians 24/7/365. Akos puts
          medical care in the palm of your hand. Simply download the Akos app to
          your smartphone or tablet and you can have a virtual consultation with
          a physician in minutes anytime, anywhere.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          When is Akos available for physician consultations?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          Akos is available 24/7/365. If you have access to a smartphone or
          tablet, you can have a virtual consult with a board certified
          physician anytime, anywhere.
        </Typography>
      </Grid>
      {/* <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          How do I contact Akos?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          Simply download the Akos app to your smartphone or tablet. Our Akos
          app is available for download on the App Store and the Google Play.
          With just a few clicks to set up your account, you can have a virtual
          consult with a doctor in minutes.
        </Typography>
      </Grid> */}
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          Is Akos safe and private?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          Confidentiality is a top priority for Akos. Our app has been designed
          on a HIPAA compliant platform so you can rest assured your information
          is securely and privately stored.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          How are the physicians in your network selected?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          Each of our physicians undergo a rigorous credentialing process based
          upon guidelines set by the National Committee for Quality Assurance
          (NCQA). All physicians in the Akos Preferred Provider Network are
          board-certified, licensed and credentialed. All Akos physicians have
          completed our Akos comprehensive training program.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          What medical conditions do you treat?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          Our physicians can diagnose and treat a wide range of non-emergency
          medical conditions. Click here for a list of common conditions
          treated.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          Can I choose the physician I want to speak with?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          You will first speak with a care coordinator who will confirm your
          medical history and assess your symptoms. The care coordinator will
          determine if your condition can be treated by a virtual consultation.
          The care coordinator will select the best physician to treat your
          medical condition.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          Can your physicians prescribe medication?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          When medically necessary, Akos physicians are able to prescribe a wide
          range of medications to treat your condition. Akos physicians do not
          prescribe or renew a prescription for controlled substances regulated
          by the U.S. Drug Enforcement Agency that have been designated as U.S.
          controlled substances. Click here to view a list of controlled
          substances. In addition, Akos physicians will not prescribe or renew
          large quantities of medications for any condition.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          Can I choose the pharmacy if I need a prescription?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          If our physician determines a medication is medically necessary, they
          can write a prescription for non-narcotic medications which will be
          sent electronically to the pharmacy of your choice.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          Can medical forms, such as work/school excuses be provided?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          Our physicians are able to provide simple forms such as work/school
          excuses or return to work/school documents as clinically appropriate.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          What if my condition cannot be treated by Akos?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          If for any reason your condition falls outside the scope of what Akos
          covers, a care coordinator will direct you to a preferred healthcare
          center in your area, so you can get the immediate care you need.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          How do I update my account information or reset my password or pin?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          You can update your account information, password or PIN under “My
          Account” from the main menu in the app.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          Can I change my email address that I use to log in?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          Your patient profile is connected to the email address that was used
          to create the account. For security reasons, you cannot change the
          email address assigned to the profile. You will need to create a new
          account in order to change your email address.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          How do I update my medical history?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          You can update your medical history under “My Health” from the main
          menu in the app.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          How much does Akos cost?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          Akos saves you more than time. We save you money. Our flat fee of $79
          for a consult with our board-certified physician is lower than the
          average urgent care fee. Memberships are also available for
          individuals and families at $9.99 per month (with a reduced consult
          fee of $40). But that’s not all. We also offer a new unlimited plan,
          allowing members and their families to access board-certified
          physicians as often as needed for a flat rate of $100 per month.
          Members on the unlimited plan can save an additional 10% for paying
          annually. That’s a $120 in savings!
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          Will I be charged if the care coordinator determines that my condition
          cannot be treated by a virtual consult?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          If your care coordinator determines that your medical condition falls
          outside the scope of a virtual consult and refers you to an ER, urgent
          care or a doctor’s office, you will not be charged for the assessment.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          What payment methods are accepted?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          Akos accepts payment from FSA, HSA, and HRA accounts, as well as all
          major debit and credit cards.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          Does Akos accept medical insurance?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          Akos does not currently accept medical insurance. However, you can pay
          for the fee with your HSA account.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          Do I need wifi to use Akos?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          Although we do recommend you use WiFi for the best possible
          experience, it is not required to contact Akos.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          What if my connection is poor?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          If you do experience connection issues, you can select to have a voice
          only consult which should improve your connection.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          Can I get assistance setting up my account?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          Our Member Service Team is available 24/7/365 to assist patients. We
          are happy to assist in setting up your account.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          How do I find your app in the app store and google play?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          Search using the term Akos on the Apple App Store or the Google Play
          Store or you can click the links below.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="primary">
          Is Akos satisfaction guaranteed?
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={8} lg={8}>
        <Typography variant="h6" color="textPrimary">
          Patient satisfaction is a top priority for Akos. We also strive to
          continually improve the patient experience. We welcome your feedback,
          both positive or otherwise. Following each consult, you will have the
          opportunity to rate Akos and your physician. These ratings are
          published on our app and website. Our patients are also sent a
          satisfaction survey to evaluate their experience. These results are
          reviewed for quality assurance and used as part of our continuous
          improvement process. We also encourage patients to contact our Member
          Service Team at any time with questions, comments or feedback.
        </Typography>
      </Grid>
    </Grid>
  );
}

export default Faq;
