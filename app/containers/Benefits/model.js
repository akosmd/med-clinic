import { initialState, initialField } from 'utils/formHelper';

export const insuranceInfoModel = {
  ...initialState,
  carrierName: {
    ...initialField,
    id: 'carrierName',
    caption: 'Carrier Name',
  },
  policyNumber: {
    ...initialField,
    id: 'policyNumber',
    caption: 'Policy Number',
  },
  renewalDate: {
    ...initialField,
    id: 'renewalDate',
    caption: 'Renewal Date',
  },

  expiryDate: {
    ...initialField,
    id: 'expiryDate',
    caption: 'Expiry Date',
  },
};
