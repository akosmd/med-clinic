import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Button, Typography } from '@material-ui/core';

import EditIcon from '@material-ui/icons/Edit';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  header: {
    marginTop: '2rem',
  },
  buttonContainer: {
    marginTop: '1rem',
  },
  centered: {
    marginTop: '2rem',
    display: 'flex',
    justifyContent: 'center',
  },
});
function ConfirmPage({ details, onBack, onSendEmail, onSendPhone }) {
  const classes = useStyles();
  const hasEmail = details.email.value && details.email.value !== '';
  const hasPhone = details.phone.value && details.phone.value !== '';

  return (
    <Grid
      container
      alignItems="center"
      justify="center"
      alignContent="center"
      spacing={1}
    >
      <Grid item xs={12} className={classes.header}>
        <Typography variant="h5" align="center">
          Verification Notice
        </Typography>
        <Typography align="center" component="div" className={classes.header}>
          Hello <strong>{details.firstName.value},</strong>
          <br /> You can use either your email address or mobile number to
          login. Please confirm if they are correct.
        </Typography>
      </Grid>
      {hasEmail && (
        <Grid item xs={12} md={4}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography variant="h6" align="right" color="primary">
                <strong>Email: </strong>
                {details.email.value}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      )}
      {hasPhone && (
        <Grid item xs={12} md={4}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography variant="h6" color="secondary">
                <strong>Phone #: </strong>
                {details.phone.value}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      )}
      <Grid item xs={12} className={classes.centered}>
        <Button startIcon={<EditIcon />} onClick={onBack}>
          Edit my Information
        </Button>
      </Grid>
      <Grid item xs={12} className={classes.centered}>
        <Button
          variant="contained"
          color="primary"
          onClick={() => (hasEmail ? onSendEmail() : onSendPhone())}
        >
          Confirm and Submit
        </Button>
      </Grid>
    </Grid>
  );
}

const { object, func } = PropTypes;

ConfirmPage.propTypes = {
  details: object.isRequired,
  onSendEmail: func.isRequired,
  onSendPhone: func.isRequired,
  onBack: func.isRequired,
};

export default ConfirmPage;
