import moment from 'moment';
import _ from 'lodash';

export const cleanedPhone = phone => {
  const cleaned =
    phone &&
    phone
      .replace('(', '')
      .replace(')', '')
      .replace('-', '')
      .replace(' ', '');

  return cleaned;
};

export const patientExists = ({ patients, patientToFind }) => {
  if (!patientExists || (patients && patients.length === 0)) return false;
  let exists = patients.find(
    p =>
      p.firstName.toLowerCase() === patientToFind.firstName.toLowerCase() &&
      p.lastName.toLowerCase() === patientToFind.lastName.toLowerCase() &&
      moment(p.birthDateAt).format('YYYY-MM-DD') ===
        moment(patientToFind.dateOfBirth).format('YYYY-MM-DD'),
  );
  if (!exists) {
    // find by Email
    exists = patients.find(
      p =>
        patientToFind.email &&
        p.email &&
        p.email.toLowerCase() === patientToFind.email.toLowerCase(),
    );
  }

  if (!exists) {
    // find by Phone Number
    exists = patients.find(
      p =>
        patientToFind.phone &&
        p.phoneNumber &&
        cleanedPhone(p.phoneNumber) === cleanedPhone(patientToFind.phone),
    );
  }
  return exists;
};

export const checkPatientExists = ({ patients, patientToFind }) => {
  if (patients && patients.length === 0) return false;
  const data = _.filter(patients, p => {
    if (
      patientToFind.phone &&
      p.phoneNumber &&
      _.isEqual(cleanedPhone(p.phoneNumber), cleanedPhone(patientToFind.phone))
    ) {
      return true;
    }
    if (
      patientToFind.email &&
      p.email &&
      _.isEqual(p.email.toLowerCase(), patientToFind.email.toLowerCase())
    ) {
      return true;
    }
    return (
      p.firstName.toLowerCase() === patientToFind.firstName.toLowerCase() &&
      p.lastName.toLowerCase() === patientToFind.lastName.toLowerCase() &&
      moment(p.birthDateAt).format('YYYY-MM-DD') ===
        moment(patientToFind.dateOfBirth).format('YYYY-MM-DD')
    );
  });

  return data.length === 0 ? false : data;
};
