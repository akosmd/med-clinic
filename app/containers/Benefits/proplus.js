export const questions = [
  {
    number: 1,
    question: 'What is the overall deductible?',
    answer: '$0.00 individual / $0.00 family participating providers',
  },
  {
    number: 2,
    question: 'Are there services covered before you meet your deductible?',
    answer: 'No. There are no other specific deductibles.',
  },
  {
    number: 3,
    question: 'Are there other deductibles for specific services?',
    answer: 'No',
  },
  {
    number: 4,
    question: 'What is the out-of-pocket limit for this plan?',
    answer:
      '$1,050.00 individual participating providers $2,100.00 family participating providers',
  },
  {
    number: 5,
    question: 'What is not included in the out-of-pocket limit?',
    answer:
      "Premiums; amounts over allowed amount; and health care this plan doesn't cover.",
  },
  {
    number: 6,
    question: 'Will you pay less if you use a network provider?',
    answer:
      'Yes. Refer to your I.D. card to identify the network logo. Please visit my.breckpoint.com, click on FIND A PROVIDER and select the appropriate network logo that matches your I.D. card. See your plan document for more information on your participating provider. You may also call (844) 798-4878 if you have any questions.',
  },
  {
    number: 7,
    question: 'Do you need a referral to',
    answer: 'No',
  },
];

export const events = [
  {
    event: "If you visit a health care provider's office or clinic",
    services: [
      'Preventive care/screening/immunization',
      'Dedicated primary care virtual clinic',
      'Virtual Urgent Care (Powered by MeMD)',
      'Primary care visit to treat an injury or illness',
      'Specialist visit',
      'Rideshare transport',
    ],
    youPay: [
      {
        header: 'Participating Provider (You will pay the least)',
        pay: [
          'No charge',
          'No charge',
          'No charge',
          '$25.00 copayment',
          '$35.00 copayment',
          'No charge',
        ],
      },
      {
        header: 'Non-Participating Provider (You will pay the most)',
        pay: [
          'Not covered',
          'Not covered',
          'Not covered',
          'Not covered',
          'Not covered',
          'Not covered',
        ],
      },
    ],
  },
  {
    event: 'If you have a test',
    services: [
      'Diagnostic test (x-ray, blood work)',
      'Imaging (CT/PET scans, MRIs)',
    ],
    youPay: [
      {
        header: 'Participating Provider (You will pay the least)',
        pay: ['$75.00 copayment', '$75.00 copayment'],
      },
      {
        header: 'Non-Participating Provider (You will pay the most)',
        pay: ['Not covered', 'Not covered'],
      },
    ],
  },
  {
    event:
      'If you need drugs to treat your illness or condition More information about prescription drug coverage is available at www.RXValet.com',
    services: ['Preventive drugs'],
    youPay: [
      {
        header: 'Participating Provider (You will pay the least)',
        pay: ['At pharmacy & mail order: No charge for preventive drugs only'],
      },
      {
        header: 'Non-Participating Provider (You will pay the most)',
        pay: ['Not covered'],
      },
    ],
  },
  {
    event: 'If you have outpatient surgery',
    services: [
      'Facility fee (e.g., ambulatory surgery center)',
      'Physician/surgeon fees',
    ],
    youPay: [
      {
        header: 'Participating Provider (You will pay the least)',
        pay: ['Not covered', 'Not covered'],
      },
      {
        header: 'Non-Participating Provider (You will pay the most)',
        pay: ['Not covered', 'Not covered'],
      },
    ],
  },
  {
    event: 'If you need immediate medical attention',
    services: [
      'Emergency room care',
      'Emergency medical transportation',
      'Urgent care',
    ],
    youPay: [
      {
        header: 'Participating Provider (You will pay the least)',
        pay: [
          'For medical emergency: $250.00 copayment',
          'Not covered',
          '$50.00 copayment',
        ],
      },
      {
        header: 'Non-Participating Provider (You will pay the most)',
        pay: ['Not covered', 'Not covered', 'Not covered'],
      },
    ],
  },
  {
    event: 'If you have a hospital stay',
    services: ['Facility fee (e.g., hospital room)', 'Physician/surgeon fees'],
    youPay: [
      {
        header: 'Participating Provider (You will pay the least)',
        pay: ['Not covered', 'Not covered'],
      },
      {
        header: 'Non-Participating Provider (You will pay the most)',
        pay: ['Not covered', 'Not covered'],
      },
    ],
  },
  {
    event:
      'If you need mental health, behavioral health, or substance abuse services',
    services: ['Outpatient services', 'Inpatient services'],
    youPay: [
      {
        header: 'Participating Provider (You will pay the least)',
        pay: [
          'Mental and Behavioral Health: Office visits: $25.00 copayment Intermediate care: Not covered. Substance Abuse: Office visits: $25.00 copayment Intermediate care: Not covered ',
          'Mental and Behavioral Health: Not covered, Substance Abuse: Not covered',
        ],
      },
      {
        header: 'Non-Participating Provider (You will pay the most)',
        pay: ['Not covered', 'Not covered'],
      },
    ],
  },
  {
    event: 'If you are pregnant',
    services: [
      'Office Visits',
      'Childbirth/delivery professional services',
      'Childbirth/delivery facility services',
    ],
    youPay: [
      {
        header: 'Participating Provider (You will pay the least)',
        pay: ['$25.00 copayment', 'Not covered', 'Not covered'],
      },
      {
        header: 'Non-Participating Provider (You will pay the most)',
        pay: ['Not covered', 'Not covered', 'Not covered'],
      },
    ],
  },
  {
    event: 'If you need help recovering or have other special health needs',
    services: [
      'Home health care',
      'Rehabilitation services',
      'Habilitation services',
      'Skilled nursing care',
      'Durable medical equipment',
      'Hospice service',
    ],
    youPay: [
      {
        header: 'Participating Provider (You will pay the least)',
        pay: [
          'Not covered',
          'Not covered',
          'Not covered',
          'Not covered',
          'Not covered',
          'Not covered',
        ],
      },
      {
        header: 'Non-Participating Provider (You will pay the most)',
        pay: [
          'Not covered',
          'Not covered',
          'Not covered',
          'Not covered',
          'Not covered',
          'Not covered',
        ],
      },
    ],
  },
  {
    event: 'If your child needs dental or eye care',
    services: [
      "Children's eye exam",
      "Children's glasses",
      "Children's dental check-up",
    ],
    youPay: [
      {
        header: 'Participating Provider (You will pay the least)',
        pay: ['Not covered', 'Not covered', 'Not covered'],
      },
      {
        header: 'Non-Participating Provider (You will pay the most)',
        pay: ['Not covered', 'Not covered', 'Not covered'],
      },
    ],
  },
];
