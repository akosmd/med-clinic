/* eslint-disable jsx-a11y/anchor-is-valid */
/**
 *
 * Login
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { useParams } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';

import KeyboardEventHandler from 'react-keyboard-event-handler';

import { compose } from 'redux';
import { Grid, Typography, Link, CircularProgress } from '@material-ui/core';
import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';
import MyLink from 'components/MyLink';
import {
  verifyAuthentication,
  reSendVerificationCode,
  resetVerificationCode,
  initializeInxiteSso,
  resetSignin,
  insertAdvinow,
  setUserIsFromMedClinic,
} from 'containers/App/actions';

import {
  makeSelectSignin,
  makeSelectVerifyAuth,
  makeIsSentToEmail,
  makeSelectVerificationCode,
  makeSelectInxiteSso,
} from 'containers/App/selectors';

import { MEMBER_TYPES } from 'utils/config';
export function TwoFactorVerification({
  dispatch,
  doVerifyAuth,
  doResendVerificationCode,
  doResetVerificationCode,
  doResetSignin,
  signin,
  auth,
  verificationCode,
  isSentToEmail,
  inxiteSso,
  doInitInxiteSso,
  doInsertAdvinow,
  doSetUserIsFromMedClinic,
  enqueueSnackbar,
}) {
  const [code, setCode] = useState('');
  const { id: isFromMedClinic } = useParams();
  useEffect(() => {
    if (!signin.data) {
      dispatch(push('/login'));
    }
  }, [signin]);

  useEffect(() => {
    if (verificationCode.data) {
      let message = `New Authentication code sent to your email ${
        signin.data.patient.email
      }`;
      if (!isSentToEmail.data)
        message = `New Authentication code sent to your Phone ${
          signin.data.patient.phoneNumber
        }`;

      enqueueSnackbar(message, {
        variant: 'success',
      });
      dispatch(doResetVerificationCode());
    }
  }, [verificationCode]);

  useEffect(() => {
    if (
      signin.data &&
      signin.data.patient.membershipTypeId === MEMBER_TYPES.VDPC
    ) {
      initiateInxiteSSO(signin.data.patient.uuid);
    }
  }, [signin]);

  const insertMemberToAdvinow = () => {
    const { id } = auth.data.patient;

    dispatch(doInsertAdvinow({ member_id: id }));
  };
  useEffect(() => {
    if (auth.error && auth.error.status === 403) {
      enqueueSnackbar('Invalid Authentication Code', {
        variant: 'error',
      });
    } else if (auth.data) {
      if (
        isFromMedClinic &&
        auth.data.patient.advinow_id === null &&
        auth.data.patient.membershipTypeId !== MEMBER_TYPES.MedClinic
      ) {
        insertMemberToAdvinow();
      }
      if (isFromMedClinic) dispatch(doSetUserIsFromMedClinic(true));
      dispatch(push('/'));
    }
  }, [auth]);

  const initiateInxiteSSO = uuid => {
    if (uuid) dispatch(doInitInxiteSso(uuid));
  };

  const handleVerify = () => {
    if (!code || code === '') {
      enqueueSnackbar('Please provide 6 digit Authentication code', {
        variant: 'error',
      });
    } else
      dispatch(
        doVerifyAuth({
          patientId: signin.data.patient.id,
          verificationCode: code,
        }),
      );
  };
  const onCodeChange = e => {
    const { value } = e.target;
    setCode(value);
  };

  const handleResendCode = () => {
    dispatch(
      doResendVerificationCode({
        employeeValidator: signin.data.patient.email,
        employerCode: signin.data.patient.employerCode,
      }),
    );
  };

  const handleReLogin = () => {
    dispatch(doResetSignin());
    dispatch(push('login'));
  };

  const sentTo = isSentToEmail.data
    ? signin.data.patient.email
    : signin.data.patient.phoneNumber;

  return (
    <div>
      <Grid container spacing={2} direction="column">
        <Grid item xs={12}>
          <Typography variant="h4" color="primary">
            Two Factor Authentication
            <Typography variant="body1" color="textPrimary">
              For your security, please enter the <strong>6-digit</strong>{' '}
              verfication code sent to{' '}
              <strong>{signin.data ? sentTo : ''}</strong>
            </Typography>
          </Typography>
        </Grid>
        <Grid item xs={12} md={3} style={{ width: '100%' }}>
          <KeyboardEventHandler
            handleKeys={['enter']}
            onKeyEvent={handleVerify}
          >
            <TextField
              field={{
                id: 'verificationCode',
                caption: 'Enter Verification Code',
                required: true,
                pristine: true,
                error: true,
                fullWidth: true,
              }}
              variant="outlined"
              onChange={onCodeChange}
            />
          </KeyboardEventHandler>
        </Grid>
        <Grid item xs={12} md={3}>
          <Typography variant="body1">
            Did not receive the code?{' '}
            <Link
              onClick={handleResendCode}
              component={MyLink}
              variant="body1"
              color="secondary"
              to="#"
            >
              <strong>Resend Code</strong>
            </Link>
          </Typography>
        </Grid>
        <Grid item xs={12} md={3}>
          <Link
            onClick={handleReLogin}
            component={MyLink}
            variant="body1"
            color="secondary"
            to="#"
          >
            <strong>Back to Login</strong>
          </Link>
        </Grid>
        <Grid item xs={12} md={3}>
          <GradientButton
            variant="contained"
            size="large"
            onClick={handleVerify}
          >
            {auth.loading && <CircularProgress color="secondary" />}
            {!auth.loading && 'Proceed'}
          </GradientButton>
        </Grid>
      </Grid>
      {inxiteSso.data && (
        <iframe
          id="inxiteFrame"
          title="Inxite"
          src={inxiteSso.data.sso_url}
          style={{ display: 'none' }}
        />
      )}
    </div>
  );
}

const { func, object } = PropTypes;
TwoFactorVerification.propTypes = {
  auth: object.isRequired,
  verificationCode: object.isRequired,
  signin: object.isRequired,
  dispatch: func.isRequired,
  enqueueSnackbar: func.isRequired,
  doVerifyAuth: func.isRequired,
  doResendVerificationCode: func.isRequired,
  isSentToEmail: object.isRequired,
  doResetVerificationCode: func.isRequired,
  doInitInxiteSso: func.isRequired,
  doResetSignin: func.isRequired,
  doInsertAdvinow: func.isRequired,
  doSetUserIsFromMedClinic: func.isRequired,
  inxiteSso: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  signin: makeSelectSignin(),
  auth: makeSelectVerifyAuth(),
  verificationCode: makeSelectVerificationCode(),
  isSentToEmail: makeIsSentToEmail(),
  inxiteSso: makeSelectInxiteSso(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doVerifyAuth: verifyAuthentication,
    doResendVerificationCode: reSendVerificationCode,
    doResetVerificationCode: resetVerificationCode,
    doInitInxiteSso: initializeInxiteSso,
    doResetSignin: resetSignin,
    doInsertAdvinow: insertAdvinow,
    doSetUserIsFromMedClinic: setUserIsFromMedClinic,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(TwoFactorVerification);
