/**
 *
 * Appointment
 *
 */

import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { push } from 'connected-react-router';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';

import KeyboardEventHandler from 'react-keyboard-event-handler';

import moment from 'moment';
import TextArea from 'components/TextArea';
import GradientButton from 'components/GradientButton';
import SelectField from 'components/Select';
import DateTimePickerField from 'components/DateTimePickerField';

import { Grid, Typography, CircularProgress } from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';

import {
  handleChange,
  highlightFormErrors,
  handleDateChange,
  extractFormValues,
} from 'utils/formHelper';

import {
  makeSelectAppointment,
  makeSelectSignin,
  makePatientImpersonation,
} from 'containers/App/selectors';
import { sendAppointment, resetAppointmentData } from 'containers/App/actions';
import model from './model';

function Appointment({
  dispatch,
  appointmentData,
  doSendAppointment,
  doResetAppointment,
  selectCurrentUser,
  impersonatedPatient,
  enqueueSnackbar,
}) {
  const [appointment, setAppointment] = useState({ ...model });

  useEffect(() => {
    if (appointmentData.data && !appointmentData.error) {
      enqueueSnackbar('Your appointment was sent!', {
        variant: 'success',
      });
      setAppointment({ ...model });
      dispatch(doResetAppointment());
      setTimeout(() => dispatch(push('/')), 5000);
    }
  }, [appointmentData]);

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: appointment,
      event,
      saveStepFunc: setAppointment,
    });
  };

  const onDateChange = field => value => {
    handleDateChange({
      field: field.id,
      state: appointment,
      value,
      saveStepFunc: setAppointment,
    });
  };

  const handleSubmit = () => {
    if (!appointment.completed) {
      highlightFormErrors(appointment, setAppointment);
      enqueueSnackbar('Please fill out all fields marked with asterisk *', {
        variant: 'error',
      });
    } else {
      const { patient } = selectCurrentUser.data;
      const formValues = extractFormValues(appointment);
      const firstOption = moment(formValues.firstOption).format(
        'YYYY-MM-DD HH:mm:ss',
      );
      const secondOption = moment(formValues.secondOption).format(
        'YYYY-MM-DD HH:mm:ss',
      );

      /*eslint-disable */
      const patientNotes = `\nAppointment with: ${
        formValues.appointmentWith
      }\n \nDetails:\n${formValues.patientNotes}.${
        impersonatedPatient.data
          ? ` This appointment is for my dependent: ${
              impersonatedPatient.data.patient.firstName
            } ${impersonatedPatient.data.patient.lastName}`
          : ''
      }`;
      /* eslint-enable */

      const params = {
        patientID: patient.id,
        firstChoice: firstOption,
        secondChoice: secondOption,
        patientNotes,
      };

      if (patient) dispatch(doSendAppointment(params));
    }
  };

  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <Typography variant="h4" color="primary">
          Make an Appointment
          <Typography variant="body1" color="textPrimary">
            Please provide the required details of your appointment.
          </Typography>
        </Typography>
      </Grid>
      <Grid item xs={12} md={6} lg={4}>
        <KeyboardEventHandler handleKeys={['enter']} onKeyEvent={handleSubmit}>
          <DateTimePickerField
            openTo="date"
            clearable="true"
            fullWidth
            field={appointment.firstChoice}
            onChange={onDateChange(appointment.firstChoice)}
            disablePast
          />
        </KeyboardEventHandler>
      </Grid>
      <Grid item xs={12} md={6} lg={4}>
        <KeyboardEventHandler handleKeys={['enter']} onKeyEvent={handleSubmit}>
          <DateTimePickerField
            openTo="date"
            clearable="true"
            fullWidth
            field={appointment.secondChoice}
            onChange={onDateChange(appointment.secondChoice)}
            disablePast
          />
        </KeyboardEventHandler>
      </Grid>
      <Grid item xs={12} lg={8}>
        <KeyboardEventHandler handleKeys={['enter']} onKeyEvent={handleSubmit}>
          <SelectField
            field={appointment.appointmentWith}
            variant="outlined"
            options={[
              { name: '', value: '' },
              { name: 'Our Care Team', value: 'Our Care Team' },
              { name: 'Healthcare Navigator', value: 'Healthcare Navigator' },
              { name: 'Covid Testing', value: 'Covid Testing' },
            ]}
            onChange={onInputChange(appointment.appointmentWith)}
          />
        </KeyboardEventHandler>
      </Grid>
      <Grid item xs={12} lg={8}>
        <KeyboardEventHandler handleKeys={['enter']} onKeyEvent={handleSubmit}>
          <TextArea
            field={appointment.patientNotes}
            variant="outlined"
            rowSize={10}
            onChange={onInputChange(appointment.patientNotes)}
          />
        </KeyboardEventHandler>
      </Grid>
      <Grid item xs={12} lg={8}>
        <GradientButton
          variant="contained"
          size="medium"
          onClick={handleSubmit}
        >
          {appointmentData.loading ? (
            <CircularProgress color="secondary" />
          ) : (
            <>
              SEND
              <SendIcon fontSize="small" />
            </>
          )}
        </GradientButton>
      </Grid>
    </Grid>
  );
}

const { func, object } = PropTypes;
Appointment.propTypes = {
  enqueueSnackbar: func.isRequired,
  dispatch: func.isRequired,
  doSendAppointment: func.isRequired,
  doResetAppointment: func.isRequired,
  appointmentData: object.isRequired,
  selectCurrentUser: object.isRequired,
  impersonatedPatient: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  appointmentData: makeSelectAppointment(),
  selectCurrentUser: makeSelectSignin(),
  impersonatedPatient: makePatientImpersonation(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSendAppointment: sendAppointment,
    doResetAppointment: resetAppointmentData,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Appointment);
