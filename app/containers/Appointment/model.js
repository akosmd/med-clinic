import { initialField } from 'utils/formHelper';

export default {
  firstChoice: {
    ...initialField,
    id: 'firstChoice',
    caption: 'First choice appointment date',
    value: null,
  },
  secondChoice: {
    ...initialField,
    id: 'secondChoice',
    caption: 'Second choice appointment date',
    value: null,
  },
  appointmentWith: {
    ...initialField,
    id: 'appointmentWith',
    caption: 'Appointment with',
    variant: 'outlined',
  },
  patientNotes: {
    ...initialField,
    id: 'patientNotes',
    caption: 'Why do you need an appointment?',
  },
};
