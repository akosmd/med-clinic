/**
 *
 * This components serves as a routing component that automatically redirects users to the Login page if it's not authenticated; otherwise the user will be redirected to the designated component/page
 *
 */

import React, { useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { setUnauthorizedPath } from 'containers/App/actions';
import { makeSelectUnauthorizedPath } from 'containers/App/selectors';
function AuthenticatedRoute({
  component: Component,
  isAuthenticated,
  appInfo,
  unauthorizedPath,
  dispatch,
  doSetUnauthorizedPath,
  storedUnauthorizePath,
  ...rest
}) {
  useEffect(() => {
    if (unauthorizedPath !== '')
      dispatch(doSetUnauthorizedPath(unauthorizedPath));
  }, []);
  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated ? (
          <Component {...props} {...rest} />
        ) : (
          <Redirect
            to={{
              pathname:
                unauthorizedPath !== ''
                  ? unauthorizedPath
                  : storedUnauthorizePath,
              state: { appInfo },
            }}
          />
        )
      }
    />
  );
}

const { object, func, oneOfType, node, bool, string } = PropTypes;
AuthenticatedRoute.propTypes = {
  component: oneOfType([func, object, node]).isRequired,
  render: func,
  isAuthenticated: oneOfType([object, bool]),
  appInfo: object,
  unauthorizedPath: string,
  dispatch: func.isRequired,
  doSetUnauthorizedPath: func.isRequired,
  storedUnauthorizePath: string.isRequired,
};
AuthenticatedRoute.defaultProps = {
  isAuthenticated: false,
  unauthorizedPath: '',
  appInfo: undefined,
  render: undefined,
};

const mapStateToProps = createStructuredSelector({
  storedUnauthorizePath: makeSelectUnauthorizedPath(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSetUnauthorizedPath: setUnauthorizedPath,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(AuthenticatedRoute);

// export default memo(AuthenticatedRoute);
