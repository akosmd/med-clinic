import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { Typography, Button } from '@material-ui/core';

import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';

import TextField from 'components/TextField';

import { handleChange, handleEmailChange } from 'utils/formHelper';

import { familyInviteModel } from './model';

function InviteMemberModal({ onToggle, open, onSubmit }) {
  const [familyInvite, setFamilyInvite] = useState({ ...familyInviteModel });

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: familyInvite,
      event,
      saveStepFunc: setFamilyInvite,
    });
  };

  const onEmailChange = field => event => {
    handleEmailChange({
      field: field.id,
      state: familyInvite,
      event,
      saveStepFunc: setFamilyInvite,
    });
  };

  return (
    <Dialog
      onClose={onToggle}
      aria-labelledby="Family-dialog"
      open={open}
      disableBackdropClick
      disableEscapeKeyDown
    >
      <DialogTitle id="Family-dialog">Add a Family Member</DialogTitle>

      <DialogContent>
        <Typography>
          You can invite a family member to join. Enter their email address and
          your guest will receive an email invitation with a link to join.
        </Typography>
        <TextField
          field={familyInvite.firstName}
          variant="outlined"
          onChange={onInputChange(familyInvite.firstName)}
        />
        <TextField
          field={familyInvite.lastName}
          variant="outlined"
          onChange={onInputChange(familyInvite.lastName)}
        />
        <TextField
          field={familyInvite.email}
          variant="outlined"
          type="email"
          onChange={onEmailChange(familyInvite.email)}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onToggle} color="primary">
          Cancel
        </Button>
        <Button onClick={() => onSubmit(familyInvite)} color="primary">
          Send Invitation
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const { func, bool } = PropTypes;
InviteMemberModal.propTypes = {
  onToggle: func.isRequired,
  open: bool.isRequired,
  onSubmit: func.isRequired,
};

export default InviteMemberModal;
