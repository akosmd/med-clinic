/**
 *
 * Pricing
 *
 */

import React, { memo, Fragment, useState } from 'react';

import classNames from 'classnames';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import Divider from '@material-ui/core/Divider';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';

import { Typography, Avatar, FormControlLabel, Radio } from '@material-ui/core';

import CheckIcon from '@material-ui/icons/Check';

const styles = makeStyles(theme => ({
  cost: {
    margin: '20px 0',
  },
  costCaption: {
    fontSize: '1.5rem',
  },
  formLabel: {
    fontFamily: 'lato',
  },
  pricing: {
    marginTop: '10px',
    marginBottom: '20px',
  },
  pricingDetail: {
    margin: '5px 0',
  },
  card: {
    margin: '.5rem',
    height: '98%',
  },
  selectedIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  greenAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: green[500],
  },
  cardSelected: {
    // margin: '1rem',
    zoom: 1.1,
    height: '100%',
    boxShadow:
      '0 8px 28px -12px rgba(0, 0, 0, 0.56), 0 4px 15px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)',
    transition: theme.transitions.create('all', {
      easing: theme.transitions.easing.easeInOut,
    }),
  },
  actionArea: {
    backgroundColor: 'transparent',
    height: '100%',
    transition: 'background-color .5s ease',
    '&:hover': {
      color: '#fff',
      backgroundColor: '#12bcc5',
    },
  },
  actionAreaSelected: {
    color: '#fff',
    backgroundColor: '#12bcc5',

    transition: theme.transitions.create('backgroundColor', {
      easing: theme.transitions.easing.easeInOut,
    }),
  },
}));

const Pricing = ({ title, pricing, selected, plans, onPlanSelected }) => {
  const classes = styles();
  const [selectedValue, setSelectedValue] = useState(null);

  const handleChange = plan => e => {
    const { value } = e.target;
    onPlanSelected(plan);
    setSelectedValue(value);
  };

  return (
    <Card
      elevation={0}
      disableRipple
      className={selected ? classes.cardSelected : classes.card}
    >
      <CardActionArea
        className={classNames(classes.actionArea, {
          [classes.actionAreaSelected]: selected,
        })}
      >
        <CardContent>
          <Grid container alignItems="center" justify="center">
            <Grid item xs={12}>
              <Typography variant="h6" align="center" color="inherit">
                {title}
              </Typography>
              {selected && (
                <Typography
                  component="div"
                  align="center"
                  className={classes.selectedIcon}
                >
                  <Avatar className={classes.greenAvatar}>
                    <CheckIcon fontSize="large" />
                  </Avatar>
                </Typography>
              )}
            </Grid>
            <Grid item xs={12} className={classes.pricing}>
              {pricing.map((price, i) => (
                <Fragment key={`price-${i + 1}`}>
                  <Typography
                    variant="body1"
                    align="center"
                    color="inherit"
                    className={classes.pricingDetail}
                  >
                    {price.striked ? (
                      <strike>
                        {price.caption} - <strong>{price.amount}</strong>
                      </strike>
                    ) : (
                      <span>
                        {price.caption}
                        <strong>
                          {/* eslint-disable-next-line no-restricted-globals */}
                          {isNaN(price.amount) || price.amount === ''
                            ? ` ${price.amount ? ` - ${price.amount}` : ''}`
                            : `- $ ${price.amount.toFixed(2)}`}
                        </strong>
                      </span>
                    )}
                  </Typography>
                  <Divider variant="middle" />
                </Fragment>
              ))}
            </Grid>
            <Grid item xs={12}>
              <Grid container alignItems="center" justify="center">
                <Grid item xs={8}>
                  <Typography variant="button">
                    <strong>Per Individual Per Month (PIPM)</strong>
                  </Typography>
                </Grid>
                <Grid item xs={4}>
                  <Typography variant="button">
                    <strong>Cost</strong>
                  </Typography>
                </Grid>

                {plans &&
                  plans.map(plan => (
                    <>
                      <Grid item xs={8}>
                        {plan.description}
                      </Grid>
                      <Grid item xs={4}>
                        <FormControlLabel
                          control={
                            <Radio
                              checked={selectedValue === plan.name}
                              onChange={handleChange(plan)}
                              value={plan.name}
                              name="braintree-plans"
                              inputProps={{ 'aria-label': 'braintree-plans' }}
                            />
                          }
                          label={`USD ${plan.cost}`}
                          labelPlacement="start"
                        />
                      </Grid>
                    </>
                  ))}
              </Grid>
            </Grid>
          </Grid>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

const { string, array, func, bool } = PropTypes;
Pricing.propTypes = {
  title: string.isRequired,
  pricing: array.isRequired,
  selected: bool,
  plans: array,
  onPlanSelected: func.isRequired,
};

Pricing.defaultProps = {
  selected: false,
};

export default memo(Pricing);
