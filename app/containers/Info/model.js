import { initialState, initialField } from 'utils/formHelper';
import moment from 'moment';
export const address = {
  ...initialState,
};

export default {
  firstName: {
    ...initialField,
    id: 'firstName',
    caption: 'First Name',
    readOnly: true,
    required: false,
    error: false,
  },
  lastName: {
    ...initialField,
    id: 'lastName',
    caption: 'Last Name',
    readOnly: true,
    required: false,
    error: false,
  },
  employerCode: {
    ...initialField,
    id: 'employerCode',
    caption: 'Employer Code',
    readOnly: true,
    required: false,
    error: false,
  },
  birthDateAt: {
    ...initialField,
    id: 'birthDateAt',
    caption: 'Date of Birth',
    readOnly: true,
    required: false,
    error: false,
  },
  email: {
    ...initialField,
    id: 'email',
    caption: 'Email Address',
    readOnly: true,
    required: false,
    error: false,
  },
  phoneNumber: {
    ...initialField,
    id: 'phoneNumber',
    caption: 'Mobile Number',
    required: false,
    error: false,
    readOnly: true,
  },
  genderId: {
    ...initialField,
    id: 'genderId',
    caption: 'Gender',
    variant: 'outlined',
    readOnly: true,
    required: false,
    error: false,
  },
  address1: {
    ...initialField,
    id: 'address1',
    caption: 'Street',
    readOnly: true,
    required: false,
    error: false,
  },
  address2: {
    ...initialField,
    id: 'address2',
    caption: 'Street Line 2',
    required: false,
    error: false,
    readOnly: true,
  },
  city: {
    ...initialField,
    id: 'city',
    caption: 'City',
    readOnly: true,
    required: false,
    error: false,
  },
  state: {
    ...initialField,
    id: 'state',
    caption: 'State',
    readOnly: true,
    required: false,
    error: false,
  },
  zipCode: {
    ...initialField,
    id: 'zipCode',
    caption: 'Zip Code',
    readOnly: true,
    required: false,
    error: false,
  },
  consent: {
    ...initialField,
    id: 'consent',
    caption: 'I Agree',
    required: false,
    value: 0,
  },
  signature: {
    ...initialField,
    base64: null,
    filename: null,
    filetype: null,
    dateStamp: moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
    isFile: true,
    id: 'signature',
    required: false,
    error: false,
    caption: 'Signature',
  },

  signatureFullName: {
    ...initialField,
    id: 'signatureFullName',
    caption: 'Or, Type your Full name if you cannot sign above',
    required: false,
    error: false,
  },
  ...initialState,
};
export const modelSSN = {
  firstName: {
    ...initialField,
    id: 'firstName',
    caption: 'First Name',
    readOnly: true,
    required: false,
    error: false,
  },
  lastName: {
    ...initialField,
    id: 'lastName',
    caption: 'Last Name',
    readOnly: true,
    required: false,
    error: false,
  },
  employerCode: {
    ...initialField,
    id: 'employerCode',
    caption: 'Employer Code',
    readOnly: true,
    required: false,
    error: false,
  },
  birthDateAt: {
    ...initialField,
    id: 'birthDateAt',
    caption: 'Date of Birth',
    readOnly: true,
    required: false,
    error: false,
  },
  email: {
    ...initialField,
    id: 'email',
    caption: 'Email Address',
    required: false,
    pristine: true,
    error: true,
  },
  phoneNumber: {
    ...initialField,
    id: 'phoneNumber',
    caption: 'Mobile Number',
    required: false,
    pristine: true,
    error: true,
  },
  genderId: {
    ...initialField,
    id: 'genderId',
    caption: 'Gender',
    variant: 'outlined',
    readOnly: true,
    required: false,
    error: false,
  },
  address1: {
    ...initialField,
    id: 'address1',
    caption: 'Street',
    readOnly: true,
    required: false,
    error: false,
  },
  address2: {
    ...initialField,
    id: 'address2',
    caption: 'Street Line 2',
    required: false,
    error: false,
    readOnly: true,
  },
  city: {
    ...initialField,
    id: 'city',
    caption: 'City',
    readOnly: true,
    required: false,
    error: false,
  },
  state: {
    ...initialField,
    id: 'state',
    caption: 'State',
    readOnly: true,
    required: false,
    error: false,
  },
  zipCode: {
    ...initialField,
    id: 'zipCode',
    caption: 'Zip Code',
    readOnly: true,
    required: false,
    error: false,
  },
  consent: {
    ...initialField,
    id: 'consent',
    caption: 'I Agree',
    required: false,
    value: 0,
  },
  signature: {
    ...initialField,
    base64: null,
    filename: null,
    filetype: null,
    dateStamp: moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
    isFile: true,
    id: 'signature',
    required: false,
    error: false,
    caption: 'Signature',
  },

  signatureFullName: {
    ...initialField,
    id: 'signatureFullName',
    caption: 'Or, Type your Full name if you cannot sign above',
    required: false,
    error: false,
  },
  ...initialState,
};

export const passwordModel = {
  password: {
    ...initialField,
    id: 'password',
    caption: 'Password',
    type: 'password',
  },
  passwordConfirmation: {
    ...initialField,
    id: 'passwordConfirmation',
    caption: 'Confirm Password',
    type: 'password',
  },
  ...initialState,
};
