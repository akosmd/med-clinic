import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { compose } from 'redux';
import moment from 'moment';

import AkosCard from 'components/AkosCard';
// import AppointmentIcon from 'components/icons/appointment';
import AssessmentIcon from 'components/icons/assessment';
import RecordsIcon from 'components/icons/records';
import PharmacyIcon from 'components/icons/pharmacy';
import MedicationsIcon from 'components/icons/medications';
import MessageIcon from 'components/icons/message';
import ProviderIcon from 'components/icons/provider';
import FAQICon from 'components/icons/faq';

import { Grid } from '@material-ui/core';

import RxSpark from 'containers/RxSpark';

import {
  setInxiteUrl,
  initializeInxiteSso,
  getPatientDependents,
  showDependentsModal,
  getBraintreeSubscription,
} from 'containers/App/actions';
import { generatePatientCallId } from 'containers/App/legacyActions';

import {
  makeSelectVerifyAuth,
  makeSelectInxiteSso,
  makeSelectInxiteUrl,
  makePatientImpersonation,
  makeShowDependentsModal,
  makeSelectActualPatient,
  makeSelectBraintreeSubscriptionStatus,
  makeSelectBraintreeCheckout,
  makeSelectVideoConfStatus,
} from 'containers/App/selectors';

import { makeSelectPatientCallId } from 'containers/App/legacySelectors';

import { INXITE_SSO_URL, MEDICATION_LOOK_UP_URL } from 'utils/config';

import PricingPlan from 'containers/Register/vdpc-lite/plan';

function VdpcLiteButtons({
  dispatch,
  doSetUrl,
  callId,
  doInitInxiteSso,
  doGeneratePatientCallId,
  doGetPatientDependents,
  doGetSubscriptionStatus,
  account,
  patient,
  subscriptionStatus,
  braintreeCheckout,
  inxiteSso,
  videoConfStatus,
}) {
  // const [showPharmacy, setShowPharmacy] = useState(false);
  const [inxiteLoaded, setInxiteLoaded] = useState(false);
  const [showPricing, setShowPricing] = useState(false);
  useEffect(() => {
    if (patient.error) {
      dispatch(push('/login'));
    } else if (
      patient.data &&
      (videoConfStatus.data || videoConfStatus.error)
    ) {
      const { patient: detail } = patient.data;
      const { uuid } = detail;
      setInxiteLoaded(false);
      initiateInxiteSSO(uuid);

      const params = {
        patientId: detail.patientLegacyId,
        patientEmail: detail.email,
        patientDob: detail.birthDateAt,
        uuid,
        staffid: 500,
        call_started: moment(Date.now()).format('YYYY/MM/DD'),
        room_name:
          (videoConfStatus.data && videoConfStatus.data.room) || 'VPDC',
        call_type: 'connect',
        device_type: window.navigator.userAgent,
      };
      if (!callId.data && !callId.loading)
        dispatch(doGeneratePatientCallId(params));

      if (!inxiteSso.data) initiateInxiteSSO(uuid);
      if (!subscriptionStatus.data)
        dispatch(doGetSubscriptionStatus(detail.id));
    }
  }, [videoConfStatus]);

  useEffect(() => {
    if (account.data) {
      dispatch(doGetPatientDependents(account.data.patient.id));
    }
  }, [account]);

  useEffect(() => {
    if (subscriptionStatus.data && !subscriptionStatus.data.is_active) {
      setShowPricing(true);
    }
  }, [subscriptionStatus]);

  useEffect(() => {
    if (braintreeCheckout.data && braintreeCheckout.data === 200)
      setShowPricing(false);
  }, [braintreeCheckout]);

  const initiateInxiteSSO = uuid => {
    dispatch(doInitInxiteSso(uuid));
  };

  const onPharmacyClick = () => {
    if (typeof window !== 'undefined') {
      window.open(MEDICATION_LOOK_UP_URL, '_blank');
    }
  };

  const onInxiteClick = url => {
    dispatch(doSetUrl(url));
    dispatch(push('/360'));
  };

  const onInxiteLoaded = () => {
    if (!inxiteLoaded) {
      setInxiteLoaded(true);
    }
  };
  if (showPricing) return <PricingPlan />;

  if (braintreeCheckout.loading) return null;

  return (
    <Grid container spacing={2} alignItems="stretch">
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Health Risk Assessment"
          icon={AssessmentIcon}
          loading={!inxiteLoaded}
          onClick={() =>
            onInxiteClick(
              `${INXITE_SSO_URL}assessments/${
                inxiteSso.data.inxite_patient_id
              }`,
            )
          }
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Talk to a Provider"
          icon={ProviderIcon}
          onClick={() => dispatch(push('/medical-history'))}

          // onClick={() => dispatch(push('/chat?talkToProvider=true'))}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Medication Lookup"
          icon={PharmacyIcon}
          onClick={onPharmacyClick}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Medical Records"
          icon={RecordsIcon}
          loading={!inxiteLoaded}
          onClick={() =>
            onInxiteClick(
              `${INXITE_SSO_URL}unifiedrecord/${
                inxiteSso.data.inxite_patient_id
              }`,
            )
          }
        />
      </Grid>
      {/* <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="My Appointments"
          icon={AppointmentIcon}
          onClick={() => dispatch(push('/create-appointment'))}
        />
      </Grid> */}
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="My Medications"
          icon={MedicationsIcon}
          loading={!inxiteLoaded}
          onClick={() =>
            onInxiteClick(
              `${INXITE_SSO_URL}unifiedrecord/${
                inxiteSso.data.inxite_patient_id
              }#medications`,
            )
          }
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="FAQs"
          icon={FAQICon}
          onClick={() => dispatch(push('/faq'))}
        />
      </Grid>

      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Messages"
          icon={MessageIcon}
          loading={!inxiteLoaded}
          onClick={() => onInxiteClick(`${INXITE_SSO_URL}messages/view`)}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={9} xl={9}>
        {inxiteSso.data && (
          <iframe
            id="inxiteFrame"
            title="Inxite"
            src={inxiteSso.data.sso_url}
            style={{ display: 'none' }}
            onLoad={onInxiteLoaded}
          />
        )}
      </Grid>
    </Grid>
  );
}

const { func, object } = PropTypes;
VdpcLiteButtons.propTypes = {
  dispatch: func.isRequired,
  doSetUrl: func.isRequired,
  doInitInxiteSso: func.isRequired,
  doGetPatientDependents: func.isRequired,
  doGeneratePatientCallId: func.isRequired,
  doGetSubscriptionStatus: func.isRequired,
  inxiteSso: object.isRequired,
  account: object.isRequired,
  patient: object.isRequired,
  callId: object.isRequired,
  braintreeCheckout: object.isRequired,
  subscriptionStatus: object.isRequired,
  videoConfStatus: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  patient: makeSelectActualPatient(),
  account: makeSelectVerifyAuth(),
  inxiteSso: makeSelectInxiteSso(),
  inxiteUrl: makeSelectInxiteUrl(),
  impersonatedPatient: makePatientImpersonation(),
  impersonationShowModal: makeShowDependentsModal(),
  callId: makeSelectPatientCallId(),
  subscriptionStatus: makeSelectBraintreeSubscriptionStatus(),
  braintreeCheckout: makeSelectBraintreeCheckout(),
  videoConfStatus: makeSelectVideoConfStatus(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSetUrl: setInxiteUrl,
    doInitInxiteSso: initializeInxiteSso,
    doGetPatientDependents: getPatientDependents,
    doShowDependentsModal: showDependentsModal,
    doGeneratePatientCallId: generatePatientCallId,
    doGetSubscriptionStatus: getBraintreeSubscription,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(VdpcLiteButtons);
