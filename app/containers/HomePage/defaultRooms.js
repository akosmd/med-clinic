import theme, { ucareTheme } from 'utils/theme';

const akos = {
  name: 'Akos',
  email: 'info@akosmd.com',
  phone: '1.844.900.AKOS(2567)',
};
const covid = {
  name: 'Agile Urgent Care',
  email: 'info@agileurgentcare.com',
  phone: '1.201.381.4800',
};

const defaultRooms = [
  {
    membershiptype: 70001,
    roomName: 'medical',
    price: 49,
    logo: undefined,
    footer: { ...akos },
    theme,
  },
  {
    membershiptype: 70002,
    roomName: 'vpdc',
    price: 0,
    logo: undefined,
    footer: { ...akos },
    theme,
  },
  {
    membershiptype: 70003,
    roomName: 'medical',
    price: 49,
    logo: undefined,
    footer: { ...akos },
    theme,
  },
  {
    membershiptype: 70004,
    roomName: 'medical',
    price: 49,
    logo: undefined,
    footer: { ...akos },
    theme,
  },
  {
    membershiptype: 70005,
    roomName: 'medical',
    price: 49,
    logo: undefined,
    footer: { ...akos },
    theme,
  },
  {
    membershiptype: 70006,
    roomName: 'agileurgentcare',
    price: 49,
    logo: undefined,
    footer: { ...covid },
    theme: ucareTheme,
  },
  {
    membershiptype: 10000,
    roomName: 'covid19',
    price: 0,
    logo: undefined,
    footer: { ...akos },
    theme,
  },
];

const getRoom = membershipTypeId =>
  defaultRooms.find(r => r.membershiptype === membershipTypeId);

export default getRoom;
