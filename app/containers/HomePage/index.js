/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { classes } from 'istanbul-lib-coverage';
import { Grid } from '@material-ui/core';
import { push } from 'connected-react-router';

import {
  makeSelectVerifyAuth,
  makeSelectActualPatient,
  makeSelectUserFromMedClinic,
  makeSelectDashboardPath,
} from 'containers/App/selectors';

import {
  getUserDefaultRoom,
  getUserDefaultRoomSuccess,
} from 'containers/App/actions';

import { resetMedicalHistory } from 'containers/App/legacyActions';

import { MEMBER_TYPES } from 'utils/config';
import VdpcButtons from './vdpc';
import VdpcLiteButtons from './vdpc-lite';
import IndividualButtons from './individual';
import MedClinic from './medclinic';
import CovidAgileUrgentCare from './covid19-agile-urgentcare';
import CovidStandAlone from './covidStandAlone';
import VideoConfConfirmation from './videoConfConfirmationModal';

import getRoom from './defaultRooms';
function HomePage({
  setDefaultRoom,
  account,
  dispatch,
  isFromMedClinic,
  doResetMedicalHistory,
  dashboardPath,
  ...rest
}) {
  const buttons = {
    70001: <IndividualButtons />,
    70002: <VdpcButtons />,
    70003: <VdpcLiteButtons />,
    70004: <VdpcButtons />,
    70005: <MedClinic />,
    70006: <CovidAgileUrgentCare {...rest} roomName="agileurgentcare" />,
    70007: <CovidStandAlone />,
  };

  useEffect(() => {
    dispatch(doResetMedicalHistory());
    if (
      account.data &&
      account.data.patient.membershipTypeId === MEMBER_TYPES.Covid
    ) {
      dispatch(push('/covid-nj'));
    }
    if (
      (account.data &&
        account.data.patient.membershipTypeId ===
          MEMBER_TYPES.CovidStandAlone) ||
      (dashboardPath === 'covid' &&
        [MEMBER_TYPES.VDPC, MEMBER_TYPES.individual].includes(
          account.data.patient.membershipTypeId,
        ))
    ) {
      dispatch(push('/covid'));
    }
    if (account.data && account.data.patient.membershipTypeId)
      dispatch(setDefaultRoom(getRoom(account.data.patient.membershipTypeId)));
  }, [account]);

  if (!account.data) return null;
  const { membershipTypeId } = account.data.patient;
  const isMedClinicUser =
    membershipTypeId === MEMBER_TYPES.MedClinic && isFromMedClinic;

  const isMedClinicUserFromDefault =
    membershipTypeId === MEMBER_TYPES.MedClinic && !isFromMedClinic;

  const isOtherTypeFromMedClinic =
    membershipTypeId !== MEMBER_TYPES.MedClinic && isFromMedClinic;
  const isOtherType =
    membershipTypeId !== MEMBER_TYPES.MedClinic && !isFromMedClinic;

  return (
    <div className={classes.root}>
      <Grid container spacing={1} justify="center" alignItems="stretch">
        <Grid item xs={12} xl={10}>
          {isMedClinicUser && buttons[MEMBER_TYPES.MedClinic]}
          {isOtherTypeFromMedClinic && buttons[MEMBER_TYPES.MedClinic]}
          {isMedClinicUserFromDefault && buttons[MEMBER_TYPES.individual]}
          {isOtherType && buttons[membershipTypeId]}
        </Grid>
      </Grid>
      <VideoConfConfirmation />
    </div>
  );
}

const { object, bool, func, string } = PropTypes;
HomePage.propTypes = {
  account: object.isRequired,
  isFromMedClinic: bool.isRequired,
  dispatch: func.isRequired,
  doGetDefaultUserRoom: func.isRequired,
  setDefaultRoom: func.isRequired,
  doResetMedicalHistory: func.isRequired,
  dashboardPath: string,
};

const mapStateToProps = createStructuredSelector({
  account: makeSelectVerifyAuth(),
  patient: makeSelectActualPatient(),
  isFromMedClinic: makeSelectUserFromMedClinic(),
  dashboardPath: makeSelectDashboardPath(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doGetDefaultUserRoom: getUserDefaultRoom,
    setDefaultRoom: getUserDefaultRoomSuccess,
    doResetMedicalHistory: resetMedicalHistory,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(HomePage);
