import React, { createRef, useState } from 'react';
import ImageUploader from 'components/ImageUploader';
import PropTypes from 'prop-types';
import BaseWebcam from 'react-webcam';
import FlexView from 'components/FlexView';
import Grid from '@material-ui/core/Grid';
import Capture from '@material-ui/icons/Camera';
import FlipCamera from '@material-ui/icons/FlipCameraAndroid';
import { styled } from '@material-ui/styles';
import { Typography } from '@material-ui/core';

const RootView = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  position: 'absolute',
  bottom: 50,
  padding: 10,
});

const Webcam = styled(BaseWebcam)({
  background: '#100616',
});

const WebCam = ({
  handleCapture,
  handleClose,
  height,
  width,
  withAudio,
  containerHeight,
}) => {
  const webcamRef = createRef();
  const [mode, setMode] = useState('user');
  const [cameraStatus, setCameraStatus] = useState({ error: false, msg: '' });
  const [cameraLoading, setCameraLoading] = useState(true);
  const doCapture = () => {
    handleCapture(webcamRef.current.getScreenshot());
    handleClose();
  };
  const handleMode = val => {
    if (val === 'user') {
      setMode({ exact: 'environment' });
    } else {
      setMode('user');
    }
  };

  return (
    <Grid container>
      <Grid item xs={12}>
        {cameraStatus.error ? (
          <FlexView
            justify="center"
            alignitems="center"
            position="relative"
            maxheight="100vh"
            direction="column"
            height={containerHeight}
          >
            <Typography variant="h5" align="center">
              {'Your browser does not support this functionality'}
            </Typography>
            <Typography variant="body2" align="center">{`Technical Details: ${
              cameraStatus.msg
            }`}</Typography>
            <ImageUploader
              handleDrop={val => {
                handleCapture(val);
                handleClose();
              }}
              disableCamera
            />
          </FlexView>
        ) : (
          <FlexView
            justify="center"
            position="relative"
            maxheight="100vh"
            height={containerHeight}
          >
            <Webcam
              audio={withAudio || false}
              ref={webcamRef}
              height={height || '100%'}
              screenshotFormat="image/jpeg"
              width={width || '100%'}
              videoConstraints={{
                width: 1920,
                height: 1080,
                frameRate: 15,
                facingMode: mode,
                playsinline: true,
                autoplay: true,
              }}
              onUserMedia={() => {
                setCameraLoading(false);
                setCameraStatus({ error: false, msg: '' });
              }}
              onUserMediaError={err =>
                setCameraStatus({ error: true, msg: err })
              }
            />
            {!cameraLoading && (
              <RootView>
                <FlexView justify="space-evenly">
                  <Capture
                    color="primary"
                    onClick={doCapture}
                    fontSize="large"
                    style={{ width: '3rem', height: '3rem' }}
                  />
                </FlexView>
                <FlexView justify="space-evenly">
                  <FlipCamera
                    color="primary"
                    onClick={() => handleMode(mode)}
                    fontSize="large"
                    style={{ width: '3rem', height: '3rem' }}
                  />
                </FlexView>
              </RootView>
            )}
          </FlexView>
        )}
      </Grid>
    </Grid>
  );
};

const { oneOfType, number, func, string, bool } = PropTypes;

WebCam.propTypes = {
  handleCapture: func,
  handleClose: func,
  height: string,
  width: string,
  withAudio: bool,
  containerHeight: oneOfType([string, number]),
};

export default WebCam;
