import React from 'react';
import PropTypes from 'prop-types';
import { Container } from './style';

const InsuranceIcon = props => (
  <Container {...props}>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="100"
      height="100"
      viewBox="0 0 100 100"
    >
      <g id="Group_18" data-name="Group 18" transform="translate(-973 -573)">
        <g id="Outline" transform="translate(981 581)">
          <path
            id="Path_29"
            data-name="Path 29"
            d="M43,15.333v-8A1.333,1.333,0,0,0,41.667,6H12.333A1.333,1.333,0,0,0,11,7.333v8a1.333,1.333,0,0,0,1.333,1.333H41.667A1.333,1.333,0,0,0,43,15.333ZM40.333,14H13.667V8.667H40.333Z"
            transform="translate(3 1.333)"
            fill="#45afb8"
          />
          <path
            id="Path_30"
            data-name="Path 30"
            d="M7,18.333v16a1.333,1.333,0,0,0,1.333,1.333h16a1.333,1.333,0,0,0,1.333-1.333V26.656L23,29.323V33H9.667V19.667H21.344L24.011,17H8.333A1.333,1.333,0,0,0,7,18.333Z"
            transform="translate(1.667 5)"
            fill="#45afb8"
          />
          <path
            id="Path_31"
            data-name="Path 31"
            d="M25.96,17.293l-9.724,9.724L13.178,23.96l-1.885,1.885,4,4a1.333,1.333,0,0,0,1.885,0L27.845,19.178Z"
            transform="translate(3.098 5.098)"
            fill="#45afb8"
          />
          <path
            id="Path_32"
            data-name="Path 32"
            d="M7,51.333a1.333,1.333,0,0,0,1.333,1.333h16a1.333,1.333,0,0,0,1.333-1.333V43.656L23,46.323V50H9.667V36.667H21.344L24.011,34H8.333A1.333,1.333,0,0,0,7,35.333Z"
            transform="translate(1.667 10.667)"
            fill="#45afb8"
          />
          <path
            id="Path_33"
            data-name="Path 33"
            d="M13.178,40.96l-1.885,1.885,4,4a1.333,1.333,0,0,0,1.885,0L27.845,36.178,25.96,34.293l-9.724,9.724Z"
            transform="translate(3.098 10.764)"
            fill="#45afb8"
          />
          <path
            id="Path_34"
            data-name="Path 34"
            d="M26,24H43.333v2.667H26Z"
            transform="translate(8 7.333)"
            fill="#45afb8"
          />
          <path
            id="Path_35"
            data-name="Path 35"
            d="M26,19H43.333v2.667H26Z"
            transform="translate(8 5.667)"
            fill="#45afb8"
          />
          <path
            id="Path_36"
            data-name="Path 36"
            d="M26,36h8v2.667H26Z"
            transform="translate(8 11.333)"
            fill="#45afb8"
          />
          <path
            id="Path_37"
            data-name="Path 37"
            d="M26,41h8v2.667H26Z"
            transform="translate(8 13)"
            fill="#45afb8"
          />
          <path
            id="Path_38"
            data-name="Path 38"
            d="M4.667,79.333V4.667H55.333V33.141q1.349-.472,2.667-1.031V3.333A1.333,1.333,0,0,0,56.667,2H3.333A1.333,1.333,0,0,0,2,3.333V80.667A1.333,1.333,0,0,0,3.333,82H56.667a1.316,1.316,0,0,0,.411-.083,32.4,32.4,0,0,1-3.6-2.584Z"
            fill="#45afb8"
          />
          <path
            id="Path_39"
            data-name="Path 39"
            d="M70.145,30.341a46.555,46.555,0,0,1-16.852-5.185,1.333,1.333,0,0,0-1.253,0,46.554,46.554,0,0,1-16.852,5.185A1.333,1.333,0,0,0,34,31.667V47.46A29.091,29.091,0,0,0,52.153,74.231a1.333,1.333,0,0,0,1.027,0A29.091,29.091,0,0,0,71.333,47.46V31.667a1.333,1.333,0,0,0-1.188-1.325ZM68.667,47.46a26.411,26.411,0,0,1-16,24.089,26.411,26.411,0,0,1-16-24.089V32.847a49.176,49.176,0,0,0,16-5.008,49.165,49.165,0,0,0,16,5.008Z"
            transform="translate(10.667 7.667)"
            fill="#45afb8"
          />
          <path
            id="Path_40"
            data-name="Path 40"
            d="M40.178,43.293l-1.885,1.885,8,8a1.333,1.333,0,0,0,1.885,0l16-16-1.885-1.885L47.236,50.35Z"
            transform="translate(12.098 11.098)"
            fill="#45afb8"
          />
          <path
            id="Path_41"
            data-name="Path 41"
            d="M25,55h9.333v2.667H25Z"
            transform="translate(7.667 17.667)"
            fill="#45afb8"
          />
          <path
            id="Path_42"
            data-name="Path 42"
            d="M16,51H37.333v2.667H16Z"
            transform="translate(4.667 16.333)"
            fill="#45afb8"
          />
        </g>
      </g>
    </svg>
  </Container>
);

const { string } = PropTypes;
InsuranceIcon.propTypes = {
  color: string,
  width: string,
  height: string,
};
InsuranceIcon.defaultProps = {
  width: '2.6rem',
  height: '2.6rem',
  color: '#01BCC5',
};

export default InsuranceIcon;
