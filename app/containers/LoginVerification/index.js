/**
 *
 * Login
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import { createStructuredSelector } from 'reselect';

import { compose } from 'redux';
import { Grid, Typography } from '@material-ui/core';
import Link from '@material-ui/core/Link';
import MyLink from 'components/MyLink';
import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';

import { verifyAuthentication } from 'containers/App/actions';

import {
  makeAccountVerification,
  makeSelectVerifyAuth,
} from 'containers/App/selectors';

export function CodeVerification({
  dispatch,
  doVerifyAuth,
  account,
  auth,
  enqueueSnackbar,
}) {
  const [code, setCode] = useState('');

  useEffect(() => {
    if (!account.data) {
      dispatch(push('/login'));
    }
  }, [account]);
  useEffect(() => {
    if (auth.error && auth.error.status === 401) {
      enqueueSnackbar('Invalid Authentication Code', {
        variant: 'error',
      });
    }
  }, [auth]);

  useEffect(() => {
    if (auth.data && auth.data.id) {
      dispatch(push('/dashboard'));
    }
  }, [auth]);
  const handleVerify = () => {
    dispatch(
      doVerifyAuth({
        patientId: account.data.member.id,
        verificationCode: code,
      }),
    );
  };
  const onCodeChange = e => {
    const { value } = e.target;
    setCode(value);
  };

  return (
    <Grid container spacing={2} direction="column">
      <Grid item xs={12}>
        <Typography variant="h4" color="primary">
          Code Verification
          <Typography variant="body1" color="textPrimary">
            For your security, please enter the <strong>6-digit</strong>{' '}
            verfication code sent to{' '}
            <strong>{account.data.member.email}</strong>
          </Typography>
        </Typography>
      </Grid>
      <Grid item xs={12} md={3} style={{ width: '100%' }}>
        <TextField
          field={{
            id: 'verificationCode',
            caption: 'Enter Verification Code',
            required: true,
            pristine: true,
            error: true,
            fullWidth: true,
          }}
          variant="outlined"
          onChange={onCodeChange}
        />
      </Grid>
      <Grid item xs={12} md={3}>
        <Typography variant="body1">
          Did not receive the code?{' '}
          <Link
            to="/register"
            component={MyLink}
            variant="body1"
            color="secondary"
          >
            <strong>Resend Code</strong>
          </Link>
        </Typography>
      </Grid>
      <Grid item xs={12} md={3}>
        <GradientButton variant="contained" size="large" onClick={handleVerify}>
          Proceed
        </GradientButton>
      </Grid>
    </Grid>
  );
}

const { func, object } = PropTypes;
CodeVerification.propTypes = {
  account: object.isRequired,
  auth: object.isRequired,
  dispatch: func.isRequired,
  enqueueSnackbar: func.isRequired,
  doVerifyAuth: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  account: makeAccountVerification(),
  auth: makeSelectVerifyAuth(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doVerifyAuth: verifyAuthentication,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(CodeVerification);
