import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { isIOS, isSafari, isMobileSafari } from 'react-device-detect';
import Dialog from '@material-ui/core/Dialog';
import DialogContentText from '@material-ui/core/DialogContentText';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Grid, Typography, Button } from '@material-ui/core';

import warningicon from 'images/warning-icon.png';

const useStyles = makeStyles({
  top: {
    backgroundImage: 'linear-gradient(to right, #00BBC3 0%,#84C2A0 100%)',
    color: '#46728E',
    justifyContent: 'center',
    display: 'flex',
    padding: '30px',
  },
  title: {
    color: '#2C3E4E',
  },
  subTitle: {
    color: '#00000029',
  },
  content: {
    color: '#2C3E4E',
    textAlign: 'center',
    fontSize: '14px',
  },
  dialog: {
    borderRadius: '8%',
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    backgroundImage: 'linear-gradient(to right, #00BBC3 0%,#84C2A0 100%)',
    color: 'white',
    borderRadius: '20px',
    paddingTop: '3%',
    paddingBottom: '3%',
    paddingLeft: '20%',
    paddingRight: '20%',
  },
});

const BrowserHandler = ({ children }) => {
  const [isOpen, setOpen] = useState(true);

  const classes = useStyles();
  const handleClose = () => {
    setOpen(!isOpen);
  };

  const isNotValidBrowser = isIOS && (!isSafari || !isMobileSafari);

  return (
    <>
      {isNotValidBrowser ? (
        <Dialog
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          maxWidth="xs"
          open={isOpen}
          className={classes.dialog}
          classes={{
            paper: classes.dialog,
          }}
        >
          <Box className={classes.top}>
            <img src={warningicon} alt="warning icon" />
          </Box>
          <Grid container>
            <Grid item>
              <Box paddingY={5} paddingX={5}>
                <Box marginBottom={5}>
                  <Typography
                    variant="h4"
                    align="center"
                    className={classes.title}
                  >
                    <strong>Warning!</strong>
                  </Typography>
                  <Typography align="center" className={classes.subTitle}>
                    Unsupported Browser
                  </Typography>
                </Box>
                <DialogContentText
                  id="alert-dialog-description"
                  className={classes.content}
                >
                  Your browser is not supported to view this website. For IOS
                  devices, please use Safari browser. For Android devices,
                  please use Google Chrome or Firefox browser.
                </DialogContentText>
                <Box className={classes.buttonContainer} mt={4}>
                  <Button className={classes.button} onClick={handleClose}>
                    Close
                  </Button>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Dialog>
      ) : (
        children
      )}
    </>
  );
};

BrowserHandler.propTypes = {
  children: PropTypes.node,
};

export default BrowserHandler;
