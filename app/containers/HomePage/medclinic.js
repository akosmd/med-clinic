import React, { useEffect } from 'react';

import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { compose } from 'redux';

import AkosCard from 'components/AkosCard';

import ProviderIcon from 'components/icons/provider';
import PricingiCon from 'components/icons/pricingPlan';
import DependentIcon from 'components/icons/dependent';

import { Grid } from '@material-ui/core';

import {
  initializeAdvinowSso,
  getPatientDependents,
  showDependentsModal,
  setUserIsFromMedClinic,
  insertAdvinow,
} from 'containers/App/actions';
import {
  makeSelectVerifyAuth,
  makePatientDependents,
  makePatientImpersonation,
  makeShowDependentsModal,
  makeSelectAdvinowSso,
} from 'containers/App/selectors';

import { MEMBER_TYPES } from 'utils/config';

function MedClinicButtons({
  dispatch,
  account,
  advinowSso,
  dependents,
  doGetPatientDependents,
  doSetUserIsFromMedClinic,
  doInsertAdvinow,
  doAdvinowSso,
}) {
  useEffect(() => {
    if (account.error) {
      dispatch(push('/medclinic/login'));
    } else if (account.data) {
      const {
        id: memberId,
        membershipTypeId,
        advinow_id: advinowId,
      } = account.data.patient;
      dispatch(doGetPatientDependents(memberId));

      if (MEMBER_TYPES.VDPC === membershipTypeId && advinowId === null) {
        dispatch(doSetUserIsFromMedClinic(true));
        dispatch(doInsertAdvinow({ member_id: memberId }));
      }

      if (advinowId !== null) {
        dispatch(doAdvinowSso({ member_id: memberId }));
      }
    }
  }, [account]);

  useEffect(() => {
    if (dependents.data && dependents.data.length > 0) {
      const vdpcDependents = dependents.data.filter(
        dep => dep.advinowId === null,
      );
      if (vdpcDependents && vdpcDependents.length > 0) {
        vdpcDependents.map(dep => {
          const { id } = dep;
          dispatch(doInsertAdvinow({ member_id: id }));
          return dep;
        });
      }
    }
  }, [dependents]);

  const onTalkToDoctorClick = () => {
    dispatch(push('/medclinic/sso'));
  };

  return (
    <Grid container spacing={2} alignItems="center" justify="center">
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Talk to a Doctor"
          icon={ProviderIcon}
          loading={advinowSso.loading}
          onClick={() => onTalkToDoctorClick()}
        />
      </Grid>

      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="My Account"
          icon={PricingiCon}
          onClick={() => dispatch(push('/my-account'))}
        />
      </Grid>

      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Add/Edit Dependents"
          icon={DependentIcon}
          onClick={() => dispatch(push('my-account'))}
        />
      </Grid>
    </Grid>
  );
}

const { func, object } = PropTypes;
MedClinicButtons.propTypes = {
  dispatch: func.isRequired,
  doGetPatientDependents: func.isRequired,
  doSetUserIsFromMedClinic: func.isRequired,
  doInsertAdvinow: func.isRequired,
  doAdvinowSso: func.isRequired,
  account: object.isRequired,
  advinowSso: object.isRequired,
  dependents: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  account: makeSelectVerifyAuth(),
  advinowSso: makeSelectAdvinowSso(),
  dependents: makePatientDependents(),
  impersonatedPatient: makePatientImpersonation(),
  impersonationShowModal: makeShowDependentsModal(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doAdvinowSso: initializeAdvinowSso,
    doGetPatientDependents: getPatientDependents,
    doShowDependentsModal: showDependentsModal,
    doSetUserIsFromMedClinic: setUserIsFromMedClinic,
    doInsertAdvinow: insertAdvinow,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(MedClinicButtons);
