import React from 'react';
import PropTypes from 'prop-types';

import { Button } from '@material-ui/core';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';

export default function SessionModal({ open, onClose }) {
  return (
    <Dialog
      open={open}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        Detected User Inactivity
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Your session will expire in 5 minutes.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          Renew Session
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const { func, bool } = PropTypes;

SessionModal.propTypes = {
  open: bool.isRequired,
  onClose: func.isRequired,
};
