import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { compose } from 'redux';
import moment from 'moment';

import AkosCard from 'components/AkosCard';

import CovidIcon from 'components/icons/covid';
import AddDependantIcon from 'components/icons/dependent';
import PharmacyIcon from 'components/icons/pharmacy';

import { Grid } from '@material-ui/core';

import {
  setInxiteUrl,
  initializeInxiteSso,
  getPatientDependents,
  showDependentsModal,
  getBraintreeSubscription,
} from 'containers/App/actions';
import { generatePatientCallId } from 'containers/App/legacyActions';

import {
  makeSelectVerifyAuth,
  makeSelectInxiteSso,
  makeSelectInxiteUrl,
  makePatientImpersonation,
  makeShowDependentsModal,
  makeSelectActualPatient,
  makeSelectBraintreeSubscriptionStatus,
  makeSelectVideoConfStatus,
} from 'containers/App/selectors';

import { MEDICATION_LOOK_UP_URL } from 'utils/config';

import { makeSelectPatientCallId } from 'containers/App/legacySelectors';

import VideoConfConfirmation from './videoConfConfirmationModal';
import MediaNotice from './mediaNotice';

function CovidStandAloneHome({
  dispatch,
  callId,
  doGeneratePatientCallId,
  doGetPatientDependents,
  account,
  patient,
  videoConfStatus,
}) {
  const [showMediaNotice, setShowMediaNotice] = useState(false);
  useEffect(() => {
    if (patient.error) {
      dispatch(push('/login'));
    } else if (
      patient.data &&
      (videoConfStatus.data || videoConfStatus.error)
    ) {
      const { patient: detail } = patient.data;
      const { uuid } = detail;

      const params = {
        patientId: detail.patientLegacyId,
        patientEmail: detail.email,
        patientDob: detail.birthDateAt,
        uuid,
        staffid: 500,
        call_started: moment(Date.now()).format('YYYY/MM/DD'),
        room_name: 'covid19',
        call_type: 'connect',
        device_type: window.navigator.userAgent,
      };
      if (!callId.data && !callId.loading)
        dispatch(doGeneratePatientCallId(params));
    }
  }, [videoConfStatus]);

  useEffect(() => {
    if (account.data) {
      dispatch(doGetPatientDependents(account.data.patient.id));
    }
  }, [account]);

  const handleShowCovidPage = () => {
    setShowMediaNotice(true);
  };
  const handleShowMediaNotice = open => {
    setShowMediaNotice(open);
    dispatch(push('/covid19'));
  };

  if (showMediaNotice)
    return (
      <MediaNotice
        handleCancel={() => handleShowMediaNotice(false)}
        handleConfirm={() => handleShowMediaNotice(false)}
      />
    );

  return (
    <Grid container spacing={2} alignItems="center" justify="center">
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="COVID-19 Assessment & Talk to Provider"
          icon={CovidIcon}
          loading={false}
          onClick={handleShowCovidPage}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Add/Edit Dependents"
          icon={AddDependantIcon}
          onClick={() => dispatch(push('/my-account'))}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Medication Lookup"
          icon={PharmacyIcon}
          href={MEDICATION_LOOK_UP_URL}
        />
      </Grid>
      <VideoConfConfirmation />
    </Grid>
  );
}

const { func, object } = PropTypes;
CovidStandAloneHome.propTypes = {
  dispatch: func.isRequired,
  doGetPatientDependents: func.isRequired,
  doGeneratePatientCallId: func.isRequired,
  account: object.isRequired,
  patient: object.isRequired,
  callId: object.isRequired,
  videoConfStatus: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  patient: makeSelectActualPatient(),
  account: makeSelectVerifyAuth(),
  inxiteSso: makeSelectInxiteSso(),
  inxiteUrl: makeSelectInxiteUrl(),
  impersonatedPatient: makePatientImpersonation(),
  impersonationShowModal: makeShowDependentsModal(),
  callId: makeSelectPatientCallId(),
  subscriptionStatus: makeSelectBraintreeSubscriptionStatus(),
  videoConfStatus: makeSelectVideoConfStatus(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSetUrl: setInxiteUrl,
    doInitInxiteSso: initializeInxiteSso,
    doGetPatientDependents: getPatientDependents,
    doShowDependentsModal: showDependentsModal,
    doGeneratePatientCallId: generatePatientCallId,
    doGetSubscriptionStatus: getBraintreeSubscription,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(CovidStandAloneHome);
