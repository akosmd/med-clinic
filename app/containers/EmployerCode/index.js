/**
 *
 * Login
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Grid, Typography } from '@material-ui/core';

import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';
export function Login() {
  return (
    <Grid container spacing={2} direction="column">
      <Grid item xs={12}>
        <Typography variant="h4" color="primary">
          Employer Code
          <Typography variant="body1" color="textPrimary">
            Enter the <strong>6-digit</strong> code provided by your employer
          </Typography>
        </Typography>
      </Grid>
      <Grid item xs={12} md={3}>
        <TextField
          field={{
            id: 'employerCode',
            caption: 'Employer Code',
            required: true,
            pristine: true,
            error: true,
            fullWidth: true,
          }}
          variant="outlined"
        />
      </Grid>

      <Grid item xs={12} md={3}>
        <GradientButton variant="contained" size="large">
          Proceed
        </GradientButton>
      </Grid>
    </Grid>
  );
}

Login.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps,
);

export default compose(withConnect)(Login);
