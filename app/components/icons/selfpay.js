import React from 'react';
import PropTypes from 'prop-types';
import { Container } from './style';

const SelfPay = props => (
  <Container {...props}>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="100"
      height="100"
      viewBox="0 0 100 100"
    >
      <g id="Group_19" data-name="Group 19" transform="translate(-809 -574)">
        <g id="surface1" transform="translate(819 584)">
          <path
            id="Path_10"
            data-name="Path 10"
            d="M66.667,14.125V6.667A6.675,6.675,0,0,0,60,0H6.667A6.675,6.675,0,0,0,0,6.667V36a6.654,6.654,0,0,0,5.979,6.6l5.012,13.01a6.668,6.668,0,0,0,8.618,3.824l31.3-12.057A19.275,19.275,0,0,0,52.567,64.49l.766,1.225V78.667A1.333,1.333,0,0,0,54.667,80h24A1.333,1.333,0,0,0,80,78.667V34.655a17.449,17.449,0,0,0-5.108-12.29Zm0,7.974,4.047,10.5a4,4,0,0,1-2.3,5.169l-2.313.893A6.668,6.668,0,0,0,66.667,36ZM2.667,36V6.667a4,4,0,0,1,4-4H60a4,4,0,0,1,4,4v8.475h-.012l.012.03V36a4.055,4.055,0,0,1-.091.691L51.7,24.476a7.113,7.113,0,0,0-10.267,9.837L46.636,40H6.667A4,4,0,0,1,2.667,36Zm17.081,6.667-9.481,3.65-1.4-3.65Zm-1.1,14.277a4,4,0,0,1-5.171-2.294L11.229,48.8l15.916-6.133v-.008H49.08l1.733,1.889Zm58.684,20.39H56V66.667H77.333Zm0-13.333H55.407l-.578-.924A16.627,16.627,0,0,1,54.491,46a1.333,1.333,0,0,0-.175-1.563L43.39,32.517a4.447,4.447,0,0,1,6.421-6.15L71.057,47.609l1.885-1.885-4.933-4.933,1.368-.533A6.667,6.667,0,0,0,73.2,31.644L68.687,19.925l4.319,4.328a14.766,14.766,0,0,1,4.327,10.4Zm0,0"
            fill="#45afb8"
          />
          <path
            id="Path_11"
            data-name="Path 11"
            d="M72.367,75.833a3.467,3.467,0,0,0,3.467-3.467v-6.4A3.467,3.467,0,0,0,72.367,62.5h-6.4A3.467,3.467,0,0,0,62.5,65.967v6.4a3.467,3.467,0,0,0,3.467,3.467Zm-7.2-3.467V70.5h2.667V67.833H65.167V65.967a.8.8,0,0,1,.8-.8h6.4a.8.8,0,0,1,.8.8v1.867H70.5V70.5h2.667v1.867a.8.8,0,0,1-.8.8h-6.4A.8.8,0,0,1,65.167,72.367Zm0,0"
            transform="translate(-57.167 -57.167)"
            fill="#45afb8"
          />
          <path
            id="Path_12"
            data-name="Path 12"
            d="M78.125,265.625h5.333v2.667H78.125Zm0,0"
            transform="translate(-71.458 -242.958)"
            fill="#45afb8"
          />
          <path
            id="Path_13"
            data-name="Path 13"
            d="M78.125,359.375h5.333v2.667H78.125Zm0,0"
            transform="translate(-71.458 -328.708)"
            fill="#45afb8"
          />
          <path
            id="Path_14"
            data-name="Path 14"
            d="M359.375,359.375h5.333v2.667h-5.333Zm0,0"
            transform="translate(-328.708 -328.708)"
            fill="#45afb8"
          />
          <path
            id="Path_15"
            data-name="Path 15"
            d="M171.875,265.625h5.333v2.667h-5.333Zm0,0"
            transform="translate(-157.208 -242.958)"
            fill="#45afb8"
          />
          <path
            id="Path_16"
            data-name="Path 16"
            d="M265.625,265.625h5.333v2.667h-5.333Zm0,0"
            transform="translate(-242.958 -242.958)"
            fill="#45afb8"
          />
          <path
            id="Path_17"
            data-name="Path 17"
            d="M359.375,265.625h5.333v2.667h-5.333Zm0,0"
            transform="translate(-328.708 -242.958)"
            fill="#45afb8"
          />
          <path
            id="Path_18"
            data-name="Path 18"
            d="M656.25,78.125h2.667v4H656.25Zm0,0"
            transform="translate(-600.25 -71.458)"
            fill="#45afb8"
          />
          <path
            id="Path_19"
            data-name="Path 19"
            d="M593.75,78.125h2.667v4H593.75Zm0,0"
            transform="translate(-543.083 -71.458)"
            fill="#45afb8"
          />
          <path
            id="Path_20"
            data-name="Path 20"
            d="M531.25,78.125h2.667v4H531.25Zm0,0"
            transform="translate(-485.917 -71.458)"
            fill="#45afb8"
          />
          <path
            id="Path_21"
            data-name="Path 21"
            d="M468.75,78.125h2.667v4H468.75Zm0,0"
            transform="translate(-428.75 -71.458)"
            fill="#45afb8"
          />
          <path
            id="Path_22"
            data-name="Path 22"
            d="M687.5,812.5h2.667v2.667H687.5Zm0,0"
            transform="translate(-628.833 -743.167)"
            fill="#45afb8"
          />
        </g>
      </g>
    </svg>
  </Container>
);

const { string } = PropTypes;
SelfPay.propTypes = {
  color: string,
  width: string,
  height: string,
};
SelfPay.defaultProps = {
  width: '2.6rem',
  height: '2.6rem',
  color: '#01BCC5',
};

export default SelfPay;
