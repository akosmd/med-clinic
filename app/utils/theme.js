import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#41c9d0',
      main: '#12bcc5',
      dark: '#25406B',
      contrastText: '#fff',
    },
    secondary: {
      light: '#506688',
      main: '#25406B',
      dark: '#192c4a',
    },
    text: {
      primary: '#666',
    },
    gradient: {
      main: 'linear-gradient(to right, #55bcbe 0%,#85c3a6 100%)',
      hover: 'linear-gradient(to right, #49b7b9 0%,#85c3a6 100%) !importants',
    },
    background: {
      default: '#f2f2f2',
      bubble: '#f83f37',
    },
    boxShadow: {
      main:
        '0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12)',
    },
  },

  typography: {
    useNextVariants: true,
    fontFamily: 'Lato, sans serif',
  },
});

export const ucareTheme = createMuiTheme({
  palette: {
    primary: {
      light: '#41c9d0',
      main: '#3c93bd',
      dark: '#25406B',
      contrastText: '#fff',
    },
    secondary: {
      light: '#506688',
      main: '#88BC44',
      dark: '#192c4a',
    },
    text: {
      primary: '#666',
    },
    background: {
      default: '#f2f2f2',
      bubble: '#f83f37',
    },

    gradient: {
      main: '#3c93bd',
      hover: '#3c93bd',
    },
    boxShadow: {
      main:
        '0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12)',
    },
  },

  typography: {
    useNextVariants: true,
    fontFamily: 'Lato, sans serif',
  },
});

export default theme;
