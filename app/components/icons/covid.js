import React from 'react';
import PropTypes from 'prop-types';
import { Container } from './style';

const BenefitsIcon = props => (
  <Container {...props}>
    <svg id="Layer_2" version="1.1" viewBox="0 0 512 512">
      <g>
        <path
          d="M151.723,144.859c19.726-18.513,44.396-31.822,71.817-37.732"
          // fill="#01BCC5"
          // // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <path
          d="M109.77,213.115c4.665-16.01,11.893-30.928,21.21-44.28"
          // // fill="none"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <path
          d="M108.116,292.155c-2.873-11.675-4.396-23.88-4.396-36.441c0-2.451,0.058-4.888,0.173-7.311"
          // // fill="none"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <path
          d="M151.338,366.207c-14.605-13.802-26.475-30.468-34.711-49.1"
          // // fill="none"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <path
          d="   M224.143,404.43c-16.977-3.587-32.909-10.008-47.278-18.744"
          // // fill="none"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <path
          d="   M305.272,399.454c-15.532,5.353-32.203,8.26-49.553,8.26"
          // // fill="none"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <path
          d="   M371.145,354.617c-8.908,10.386-19.193,19.556-30.573,27.227"
          // // fill="none"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <path
          d="   M404.631,286.347c-3.397,16.602-9.501,32.218-17.831,46.367"
          // // fill="none"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <path
          d="   M404.863,226.233c1.875,9.537,2.857,19.394,2.857,29.481"
          // // fill="none"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <path
          d="   M362.182,147.226c7.026,6.896,13.385,14.468,18.972,22.613"
          // // fill="none"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <path
          d="   M281.995,105.977c21.283,3.709,41.046,11.845,58.305,23.424"
          // // fill="none"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <g>
          <circle
            cx="255.72"
            cy="39.714"
            // // fill="none"
            r="18"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
          />
          <line
            // // fill="none"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
            x1="255.72"
            x2="255.72"
            y1="57.714"
            y2="122.714"
          />
        </g>
        <g>
          <circle
            cx="101.72"
            cy="96.714"
            // // fill="none"
            r="18"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
          />
          <line
            // // fill="none"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
            x1="114.447"
            x2="160.409"
            y1="109.442"
            y2="155.404"
          />
        </g>
        <g>
          <circle
            cx="82.249"
            cy="203.58"
            // fill="none"
            r="18"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
          />
          <line
            // fill="none"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
            x1="98.792"
            x2="133.72"
            y1="210.675"
            y2="225.714"
          />
        </g>
        <g>
          <circle
            cx="395.725"
            cy="183.692"
            // fill="none"
            r="18"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
          />
          <line
            // fill="none"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
            x1="380.615"
            x2="350.72"
            y1="193.474"
            y2="212.714"
          />
        </g>
        <g>
          <circle
            cx="353.143"
            cy="404.911"
            // fill="none"
            r="18"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
          />
          <line
            // fill="none"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
            x1="342.3"
            x2="325.72"
            y1="390.544"
            y2="368.714"
          />
        </g>
        <g>
          <circle
            cx="409.72"
            cy="96.714"
            // fill="none"
            r="18"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
          />
          <line
            // fill="none"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
            x1="396.992"
            x2="351.03"
            y1="109.442"
            y2="155.404"
          />
        </g>
        <g>
          <circle
            cx="255.72"
            cy="474.714"
            // fill="none"
            r="18"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
          />
          <line
            // fill="none"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
            x1="255.72"
            x2="255.72"
            y1="456.714"
            y2="391.714"
          />
        </g>
        <g>
          <circle
            cx="46.67"
            cy="303.707"
            // fill="none"
            r="18"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
          />
          <line
            // fill="none"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
            x1="64.43"
            x2="128.565"
            y1="300.78"
            y2="290.209"
          />
        </g>
        <g>
          <circle
            cx="473.22"
            cy="257.214"
            // fill="none"
            r="18"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
          />
          <line
            // fill="none"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
            x1="455.22"
            x2="390.22"
            y1="257.214"
            y2="257.214"
          />
        </g>
        <g>
          <circle
            cx="431.576"
            cy="366.063"
            // fill="none"
            r="18"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
          />
          <line
            // fill="none"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
            x1="416.712"
            x2="363.037"
            y1="355.911"
            y2="319.251"
          />
        </g>
        <g>
          <circle
            cx="101.72"
            cy="417.714"
            // fill="none"
            r="18"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
          />
          <line
            // fill="none"
            // stroke={props.color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeMiterlimit="10"
            strokeWidth="15"
            x1="114.447"
            x2="160.409"
            y1="404.986"
            y2="359.024"
          />
        </g>
        <circle
          cx="214.72"
          cy="173.714"
          // fill="none"
          r="18"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <circle
          cx="296.22"
          cy="176.214"
          // fill="none"
          r="12.5"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <circle
          cx="178.22"
          cy="245.214"
          // fill="none"
          r="12.5"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <circle
          cx="190.22"
          cy="323.214"
          // fill="none"
          r="12.5"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <circle
          cx="348.22"
          cy="287.214"
          // fill="none"
          r="12.5"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <circle
          cx="273.72"
          cy="332.714"
          // fill="none"
          r="18"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
        <circle
          cx="272.72"
          cy="248.714"
          // fill="none"
          r="23"
          // stroke={props.color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeMiterlimit="10"
          strokeWidth="15"
        />
      </g>
    </svg>
  </Container>
);

const { string } = PropTypes;
BenefitsIcon.propTypes = {
  color: string,
  width: string,
  height: string,
};
BenefitsIcon.defaultProps = {
  width: '2.5rem',
  height: '2.5rem',
  color: '#01BCC5',
};

export default BenefitsIcon;
