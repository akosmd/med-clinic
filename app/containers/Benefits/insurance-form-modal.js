import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import DateFnsUtils from '@date-io/date-fns';
import { Button, CircularProgress } from '@material-ui/core';

import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

import TextField from 'components/TextField';
import DatePickerField from 'components/DatePickerField';

import {
  handleChange,
  handleDateChange,
  highlightFormErrors,
  setValuesFrom,
} from 'utils/formHelper';

import { insuranceInfoModel } from './model';

function InsuranceFormModal({
  onClose,
  open,
  onSubmit,
  enqueueSnackbar,
  data,
  loading,
}) {
  const [insuranceInfo, setInsuranceInfo] = useState({ ...insuranceInfoModel });

  useEffect(() => {
    if (data) {
      const transformedData = {
        ...data,
        renewalDate: moment(data.renewalDate).format('MM/DD/YYYY'),
        expiryDate: moment(data.expiryDate).format('MM/DD/YYYY'),
      };
      const fieldsWithValues = setValuesFrom(transformedData, insuranceInfo);
      setInsuranceInfo({ ...fieldsWithValues });
    } else {
      setInsuranceInfo({ ...insuranceInfoModel });
    }
  }, [open]);

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: insuranceInfo,
      event,
      saveStepFunc: setInsuranceInfo,
    });
  };

  const onDateChange = field => value => {
    handleDateChange({
      field: field.id,
      state: insuranceInfo,
      value,
      saveStepFunc: setInsuranceInfo,
    });
  };

  const handleSubmit = () => {
    if (
      data &&
      !insuranceInfo.completed &&
      insuranceInfo.status === undefined
    ) {
      onClose();
      return;
    }
    if (!insuranceInfo.completed) {
      highlightFormErrors(insuranceInfo, setInsuranceInfo);
      enqueueSnackbar('Please fill out all fields marked with asterisk *', {
        variant: 'error',
      });
      return;
    }
    onSubmit(insuranceInfo);
  };

  const method = data ? 'Update' : 'Add';

  return (
    <Dialog
      onClose={onClose}
      aria-labelledby="Family-dialog"
      open={open}
      disableBackdropClick
      disableEscapeKeyDown
    >
      <DialogTitle id="Family-dialog">{method} Insurance</DialogTitle>

      <DialogContent dividers>
        <TextField
          field={insuranceInfo.carrierName}
          variant="outlined"
          onChange={onInputChange(insuranceInfo.carrierName)}
        />
        <TextField
          field={insuranceInfo.policyNumber}
          variant="outlined"
          onChange={onInputChange(insuranceInfo.policyNumber)}
        />
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <DatePickerField
            format="MM/dd/yyyy"
            openTo="year"
            minDate={moment().add(-18, 'year')}
            maxDate={moment()}
            clearable
            fullWidth
            field={insuranceInfo.renewalDate}
            onChange={onDateChange(insuranceInfo.renewalDate)}
            disableFuture
          />
        </MuiPickersUtilsProvider>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <DatePickerField
            format="MM/dd/yyyy"
            openTo="year"
            minDate={moment()}
            maxDate={moment().add(18, 'year')}
            clearable
            fullWidth
            field={insuranceInfo.expiryDate}
            onChange={onDateChange(insuranceInfo.expiryDate)}
          />
        </MuiPickersUtilsProvider>
      </DialogContent>
      <DialogActions>
        {loading ? (
          <CircularProgress color="secondary" />
        ) : (
          <>
            <Button onClick={onClose}>Cancel</Button>
            <Button onClick={handleSubmit} color="primary">
              {method}
            </Button>
          </>
        )}
      </DialogActions>
    </Dialog>
  );
}

const { func, bool, object } = PropTypes;
InsuranceFormModal.propTypes = {
  onClose: func.isRequired,
  open: bool.isRequired,
  onSubmit: func.isRequired,
  enqueueSnackbar: func.isRequired,
  data: object,
  loading: bool,
};

export default InsuranceFormModal;
