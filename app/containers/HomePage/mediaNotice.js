import React from 'react';
import PropTypes from 'prop-types';

import { Button } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';

function MediaNotice({ handleConfirm, handleCancel }) {
  return (
    <Dialog
      open
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        Confirm Video and Microphone
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Please verify and confirm that your Camera and Microphone are working
          before proceeding. It is required during the Video Conference.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCancel}>Cancel and close</Button>
        <Button color="primary" onClick={handleConfirm}>
          Yes, Video and Microphone are working
        </Button>
      </DialogActions>
    </Dialog>
  );
}

MediaNotice.propTypes = {
  handleConfirm: PropTypes.func.isRequired,
  handleCancel: PropTypes.func.isRequired,
};
export default MediaNotice;
