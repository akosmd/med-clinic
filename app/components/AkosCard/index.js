/**
 *
 * Card
 *
 */

import React from 'react';
import PropTypes, { bool } from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Card, CardContent, CircularProgress } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  content: {
    margin: '1.5rem',
  },
  card: {
    // minHeight: '13rem',
    boxShadow:
      '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 0px 0px 0px rgba(0,0,0,0.14), 0px 1px 1px -1px rgba(0,0,0,0.12)',
  },
  cardSelected: {
    // margin: '1rem',
    zoom: 1.1,
    height: '100%',
    color: '#fff',
    backgroundColor: '#12bcc5',
    '& svg': {
      fill: '#fff',
      color: '#fff',
    },
    boxShadow:
      '0 8px 28px -12px rgba(0, 0, 0, 0.56), 0 4px 15px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)',
    transition: theme.transitions.create('all', {
      easing: theme.transitions.easing.easeInOut,
    }),
  },
  title: {
    marginTop: '1.5rem',
  },
  actionArea: {
    transition: 'all .5s ease',

    '&:hover': {
      color: '#fff',
      backgroundColor: '#12bcc5',
    },
    '&:hover svg': {
      fill: '#fff',
      color: '#fff',
      stroke: '#fff',
    },
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'center',
  },
  disabledArea: {
    backgroundColor: '#d0d0d0',
    display: 'flex',
    justifyContent: 'center',
  },
  link: {
    textDecoration: 'none',
  },
}));
function AkosCard({
  title,
  icon: Icon,
  href,
  onClick,
  loading,
  disabled,
  selected,
  content,
}) {
  const classes = useStyles();

  const renderCard = () => (
    <Card
      elevation={1}
      className={selected ? classes.cardSelected : classes.card}
      onClick={onClick}
    >
      <div
        className={disabled ? classes.disabledArea : classes.actionArea}
        onClick={onClick}
        role="button"
        aria-hidden
      >
        <CardContent className={classes.content}>
          {!loading && (
            <Typography component="div" align="center">
              {Icon && <Icon />}
            </Typography>
          )}
          {loading && (
            <Typography component="div" align="center">
              <CircularProgress size="2rem" />
            </Typography>
          )}

          <Typography align="center" variant="h6" className={classes.title}>
            {loading ? 'Please wait...' : title || ''}
          </Typography>
          {content && content}
        </CardContent>
      </div>
    </Card>
  );

  const renderLink = () => (
    <a
      href={href}
      className={classes.link}
      target="_blank"
      rel="noopener noreferrer"
    >
      {renderCard()}
    </a>
  );
  if (href && href !== '') return renderLink();

  return renderCard();
}
const { func, string, oneOfType, node } = PropTypes;
AkosCard.propTypes = {
  onClick: func,
  title: string,
  loading: bool,
  selected: bool,
  href: string,
  disabled: bool,
  icon: oneOfType([node, func]),
  content: node,
};

AkosCard.defaultProps = {
  loading: false,
};
export default AkosCard;
