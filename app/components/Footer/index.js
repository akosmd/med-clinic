import React from 'react';
import { string, number } from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';

import { MEMBER_TYPES } from 'utils/config';

const useStyles = makeStyles(theme => ({
  footer: {
    zIndex: theme.zIndex.drawer + 1,
    position: 'fixed',
    padding: '1rem',
    display: 'flex',
    left: 0,
    bottom: 0,
    width: '100%',
    backgroundColor: '#25406B',
    color: 'white',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
}));
function Footer({ width, path, memberType }) {
  const classes = useStyles();
  const footerDetailsMapping = {
    akos: {
      name: 'Akos',
      email: 'info@akosmd.com',
      phone: '1.844.900.AKOS(2567)',
    },
    covid: {
      name: 'Agile Urgent Care',
      email: 'info@agileurgentcare.com',
      phone: '1.201.381.4800',
    },
  };

  const isPathAUC = ['covid-nj'].includes(path.split('/')[1]);
  const isMemberTypeCovid = MEMBER_TYPES.Covid === memberType;

  const { name, email, phone } =
    isPathAUC || isMemberTypeCovid
      ? footerDetailsMapping.covid
      : footerDetailsMapping.akos;
  return (
    <div className={classes.footer}>
      <div>
        Copyright &copy; {new Date(Date.now()).getFullYear()} {name} | All
        Rights Reserved
      </div>
      {width !== 'xs' && <div>Email: {email}</div>}
      {width !== 'xs' && <div>Phone: {phone}</div>}
      {<div>v {process.env.variables.npm_package_version}</div>}
    </div>
  );
}

Footer.propTypes = {
  width: string,
  path: string,
  memberType: number,
};

export default Footer;
