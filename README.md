## Local Development

- Clone this project
- checkout `usa-develop` branch
- run `npm install`
- run `npm start`
- Browse to `http://localhost:3000`

## Staging Deployment

- Clone this project
- checkout `usa-develop` branch
- run `npm install`
- run `npm run build:staging`
- copy contents of `build` folder to `/var/www/html/staging/member/.`

## Staging Production

- Clone this project
- checkout `usa-develop` branch
- run `npm install`
- run `npm run build`
- copy contents of `build` folder to `/var/www/html/prod/member/.`
