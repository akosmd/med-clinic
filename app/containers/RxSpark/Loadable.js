/**
 *
 * Asynchronously loads the component for GoodRx
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
