import React from 'react';
import PropTypes from 'prop-types';

import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Tooltip from '@material-ui/core/Tooltip';

export default function EnhancedTableHead(props) {
  const createSortHandler = property => event => {
    props.onRequestSort(event, property);
  };

  const { order, orderBy, header = [] } = props;

  return (
    <TableHead>
      <TableRow>
        {header.map(
          row => (
            <TableCell
              key={row.id}
              align={row.numeric ? 'right' : 'left'}
              padding={row.disablePadding ? 'none' : 'default'}
              sortDirection={orderBy === row.id ? order : false}
              style={{ minWidth: row.width || '200' }}
            >
              <Tooltip
                title="Sort"
                placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                enterDelay={300}
              >
                <TableSortLabel
                  active={orderBy === row.id}
                  direction={order}
                  onClick={createSortHandler(row.id)}
                >
                  <b>{row.label}</b>
                </TableSortLabel>
              </Tooltip>
            </TableCell>
          ),
          this,
        )}
      </TableRow>
    </TableHead>
  );
}

const { func, array, string } = PropTypes;

EnhancedTableHead.propTypes = {
  onRequestSort: func.isRequired,

  header: array,
  order: string.isRequired,
  orderBy: string.isRequired,
};
