import React from 'react';
import PropTypes from 'prop-types';
import { Container } from './style';

const NoInsuranceIcon = props => (
  <Container {...props}>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="100"
      height="100"
      viewBox="0 0 100 100"
    >
      <g id="Group_17" data-name="Group 17" transform="translate(-1115 -573)">
        <g id="Outline" transform="translate(1123 581)">
          <path
            id="Path_29"
            data-name="Path 29"
            d="M43,15.333v-8A1.333,1.333,0,0,0,41.667,6H12.333A1.333,1.333,0,0,0,11,7.333v8a1.333,1.333,0,0,0,1.333,1.333H41.667A1.333,1.333,0,0,0,43,15.333ZM40.333,14H13.667V8.667H40.333Z"
            transform="translate(3 1.333)"
            fill="#45afb8"
          />
          <path
            id="Path_34"
            data-name="Path 34"
            d="M26,24H43.333v2.667H26Z"
            transform="translate(8 7.333)"
            fill="#45afb8"
          />
          <path
            id="Path_35"
            data-name="Path 35"
            d="M26,19H43.333v2.667H26Z"
            transform="translate(8 5.667)"
            fill="#45afb8"
          />
          <path
            id="Path_36"
            data-name="Path 36"
            d="M26,36h8v2.667H26Z"
            transform="translate(8 11.333)"
            fill="#45afb8"
          />
          <path
            id="Path_37"
            data-name="Path 37"
            d="M26,41h8v2.667H26Z"
            transform="translate(8 13)"
            fill="#45afb8"
          />
          <path
            id="Path_38"
            data-name="Path 38"
            d="M4.667,79.333V4.667H55.333V33.141q1.349-.472,2.667-1.031V3.333A1.333,1.333,0,0,0,56.667,2H3.333A1.333,1.333,0,0,0,2,3.333V80.667A1.333,1.333,0,0,0,3.333,82H56.667a1.316,1.316,0,0,0,.411-.083,32.4,32.4,0,0,1-3.6-2.584Z"
            fill="#45afb8"
          />
          <path
            id="Path_39"
            data-name="Path 39"
            d="M70.145,30.341a46.555,46.555,0,0,1-16.852-5.185,1.333,1.333,0,0,0-1.253,0,46.554,46.554,0,0,1-16.852,5.185A1.333,1.333,0,0,0,34,31.667V47.46A29.091,29.091,0,0,0,52.153,74.231a1.333,1.333,0,0,0,1.027,0A29.091,29.091,0,0,0,71.333,47.46V31.667a1.333,1.333,0,0,0-1.188-1.325ZM68.667,47.46a26.411,26.411,0,0,1-16,24.089,26.411,26.411,0,0,1-16-24.089V32.847a49.176,49.176,0,0,0,16-5.008,49.165,49.165,0,0,0,16,5.008Z"
            transform="translate(10.667 7.667)"
            fill="#45afb8"
          />
          <path
            id="Path_41"
            data-name="Path 41"
            d="M25,55h9.333v2.667H25Z"
            transform="translate(7.667 17.667)"
            fill="#45afb8"
          />
          <path
            id="Path_42"
            data-name="Path 42"
            d="M16,51H37.333v2.667H16Z"
            transform="translate(4.667 16.333)"
            fill="#45afb8"
          />
          <g id="icon-add" transform="translate(63.439 38.13) rotate(45)">
            <path
              id="path"
              d="M0,16.549,16.549,0"
              transform="translate(11.702 0) rotate(45)"
              fill="none"
              stroke="#45afb8"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
            />
            <path
              id="path-2"
              data-name="path"
              d="M0,16.549,16.549,0"
              transform="translate(23.405 11.704) rotate(135)"
              fill="none"
              stroke="#45afb8"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
            />
          </g>
          <g
            id="icon-add-2"
            data-name="icon-add"
            transform="translate(18.439 23.927) rotate(45)"
          >
            <path
              id="path-3"
              data-name="path"
              d="M0,7.753,7.753,0"
              transform="translate(5.482 0) rotate(45)"
              fill="none"
              stroke="#45afb8"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
            />
            <path
              id="path-4"
              data-name="path"
              d="M0,7.753,7.753,0"
              transform="translate(10.965 5.483) rotate(135)"
              fill="none"
              stroke="#45afb8"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
            />
          </g>
          <g
            id="icon-add-3"
            data-name="icon-add"
            transform="translate(18.439 45.927) rotate(45)"
          >
            <path
              id="path-5"
              data-name="path"
              d="M0,7.753,7.753,0"
              transform="translate(5.482 0) rotate(45)"
              fill="none"
              stroke="#45afb8"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
            />
            <path
              id="path-6"
              data-name="path"
              d="M0,7.753,7.753,0"
              transform="translate(10.965 5.483) rotate(135)"
              fill="none"
              stroke="#45afb8"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
            />
          </g>
          <g
            id="Rectangle_17"
            data-name="Rectangle 17"
            transform="translate(9 22)"
            fill="none"
            stroke="#45afb8"
            strokeWidth="2.5"
          >
            <rect width="19" height="19" rx="2" stroke="none" />
            <rect
              x="1.25"
              y="1.25"
              width="16.5"
              height="16.5"
              rx="0.75"
              fill="none"
            />
          </g>
          <g
            id="Rectangle_18"
            data-name="Rectangle 18"
            transform="translate(9 44)"
            fill="none"
            stroke="#45afb8"
            strokeWidth="2.5"
          >
            <rect width="19" height="19" rx="2" stroke="none" />
            <rect
              x="1.25"
              y="1.25"
              width="16.5"
              height="16.5"
              rx="0.75"
              fill="none"
            />
          </g>
        </g>
      </g>
    </svg>
  </Container>
);

const { string } = PropTypes;
NoInsuranceIcon.propTypes = {
  color: string,
  width: string,
  height: string,
};
NoInsuranceIcon.defaultProps = {
  width: '2.6rem',
  height: '2.6rem',
  color: '#01BCC5',
};

export default NoInsuranceIcon;
