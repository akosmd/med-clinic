import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';

import SignatureCanvas from 'react-signature-canvas';

import moment from 'moment';
import { createStructuredSelector } from 'reselect';
import { Grid, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';
import DatePickerField from 'components/DatePickerField';
import SelectField from 'components/Select';
import StatesSelect from 'components/StatesSelect';
import ConsentForm from 'components/Consent';

import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

// eslint-disable-next-line no-unused-vars
import SignaturePad from 'react-signature-pad-wrapper';

import useMediaQuery from '@material-ui/core/useMediaQuery';
import Link from '@material-ui/core/Link';
import MyLink from 'components/MyLink';

import {
  handleChange,
  highlightFormErrors,
  handleEmailChange,
  handleDateChange,
  extractFormValues,
  handlePhoneChange,
  handleEqualityChange,
  formatPhoneNumber,
  invalidEmail,
  isNumber,
} from 'utils/formHelper';
import countries from 'utils/countries.json';
import ethnicities from 'utils/ethnicities.json';
import races from 'utils/races.json';

import {
  register as registerRequest,
  resetRegisterData,
  logoutUser,
  checkPatientExist as findPatient,
} from 'containers/App/actions';

import {
  makeSelectRegister,
  makeSelectPatientExist,
  makeSelectUnauthorizedPath,
} from 'containers/App/selectors';
import model from './model';

import NoticePage from './confirmRegistration';
import NotificationMessage from './notificationMessage';

import ExistsDialog from './existsDialog';

const useStyles = makeStyles({
  formContainer: {
    width: '100%',
  },
  loginLink: {
    marginLeft: '1rem',
  },
  signingPad: {
    border: '1px solid #b7b7b7',
    padding: '1rem',
    borderRadius: '4px',
    width: '100%',
    height: '200px',
  },
  signingLabel: {
    padding: '1rem',
  },
  sigControls: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  signingPadFocus: {
    border: '1px solid #F14137',
    padding: '1rem',
    borderRadius: '4px',
    width: '100%',
    height: '200px',
  },
});

export function CovidRegister({
  doRegister,
  dispatch,
  enqueueSnackbar,
  registrationData,
  foundPatients,
  doResetRegister,
  doLogoutUser,
  doFindPatient,
  unauthorizedPath,
  history,
  membershipTypeId,
}) {
  const classes = useStyles();
  const [register, setRegister] = useState({ ...model });
  const [readConsent, setReadConsent] = useState(false);
  const [sigPad, setSigPad] = useState(null);
  const [showNotice, setshowNotice] = useState(false);
  const [showConfirm, setshowConfirm] = useState(false);
  const [sendTo, setSendTo] = useState('');
  const [showExists, setShowExists] = useState(false);
  const [showMemberGroupCodeField, setShowMemberGroupCodeField] = useState(
    false,
  );

  const [signatureClassName, setSignatureClassName] = useState(
    classes.signingPad,
  );
  const isMobile = useMediaQuery(theme => theme.breakpoints.down('sm'));
  const fullNameRef = useRef(null);

  // when user tries to register, make sure to cleanup existing store
  useEffect(() => {
    dispatch(doLogoutUser());
  }, []);

  useEffect(() => {
    if (registrationData.data) {
      setRegister({ ...model });
      setshowNotice(false);
      setshowConfirm(true);
      // savePatientToHg();
      dispatch(doResetRegister());
    }
  }, [registrationData]);

  useEffect(() => {
    const {
      location: { search },
    } = history;
    if (search !== '') {
      evaluateQs(search);
    }
  }, [history]);

  useEffect(() => {
    if (foundPatients.data && foundPatients.data.exists) {
      setShowExists(true);
      switch (foundPatients.data.searchType) {
        case 'phone':
          handlePhoneChange({
            field: 'phone',
            state: register,
            duplicate: true,
            errorMessage: 'Mobile Number already exists',
            saveStepFunc: setRegister,
          });
          break;
        case 'email':
          handleEmailChange({
            field: 'email',
            state: register,
            duplicate: true,
            errorMessage: 'Email already exists',
            saveStepFunc: setRegister,
          });
          break;
        case 'date':
          handleDateChange({
            field: 'dateOfBirth',
            state: {
              ...register,
              firstName: {
                ...register.firstName,
                error: true,
                errorMessage: 'User already exists',
              },
              lastName: {
                ...register.lastName,
                error: true,
                errorMessage: 'User already exists',
              },
            },
            saveStepFunc: setRegister,
            duplicate: true,
            errorMessage: 'User already exists',
          });
          break;
        case 'employerCode':
          handleChange({
            field: 'employerCode',
            state: {
              ...register,
              firstName: {
                ...register.firstName,
                error: true,
                errorMessage: 'User already exists',
              },
              lastName: {
                ...register.lastName,
                error: true,
                errorMessage: 'User already exists',
              },
              dateOfBirth: {
                ...register.dateOfBirth,
                error: false,
                duplicate: false,
              },
            },
            duplicate: true,
            errorMessage: 'User already exists',
            saveStepFunc: setRegister,
          });
          break;
        case 'nameDOB':
          handleChange({
            field: 'lastName',
            state: {
              ...register,
              firstName: {
                ...register.firstName,
                error: true,
                errorMessage: 'User already exists',
              },
              dateOfBirth: {
                ...register.dateOfBirth,
                error: true,
                errorMessage: 'User already exists',
              },
              employerCode: {
                ...register.employerCode,
                error: false,
                duplicate: false,
              },
            },
            duplicate: true,
            errorMessage: 'User already exists',
            saveStepFunc: setRegister,
          });
          break;
        case 'nameEmployer':
          handleChange({
            field: 'lastName',
            state: {
              ...register,
              firstName: {
                ...register.firstName,
                error: true,
                errorMessage: 'User already exists',
              },
              employerCode: {
                ...register.employerCode,
                error: true,
                errorMessage: 'User already exists',
              },
            },
            duplicate: true,
            errorMessage: 'User already exists',
            saveStepFunc: setRegister,
          });
          break;
        default: {
          break;
        }
      }
    }
  }, [foundPatients]);

  const evaluateQs = search => {
    const params = search.split('&');
    const code = params[0].replace('?memberGroupCode=', '');
    setShowMemberGroupCodeField(true);
    setRegister({
      ...register,
      memberGroupCode: {
        ...register.memberGroupCode,
        value: code,
        error: false,
        pristine: false,
        readOnly: true,
      },
    });
  };

  const onInputChange = field => event => {
    if (register.fullName !== '') {
      setSignatureClassName(classes.signingPad);
    }
    handleChange({
      field: field.id,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
  };
  const handleCheckPatientExist = payload => {
    const debounced = _.debounce(() => dispatch(doFindPatient(payload)), 2000);
    debounced();
  };
  const onEmailChange = field => event => {
    handleEmailChange({
      field: field.id,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
    if (!invalidEmail(event.target.value)) {
      handleCheckPatientExist({
        email: event.target.value,
        searchType: 'email',
      });
    }
  };

  const onDateChange = field => value => {
    const years = moment(Date.now()).diff(moment(value), 'years');
    if (years >= 18) {
      handleDateChange({
        field: field.id,
        state: {
          ...register,
          firstName: {
            ...register.firstName,
            error: register.firstName.value === '',
          },
          lastName: {
            ...register.lastName,
            error: register.lastName.value === '',
          },
          employerCode: {
            ...register.employerCode,
            error: false,
          },
        },
        value,
        saveStepFunc: setRegister,
      });
      if (moment(new Date(value)).isValid()) {
        handleCheckPatientExist({
          firstName: register.firstName.value,
          lastName: register.lastName.value,
          birthDateAt: moment(value).format('YYYY-MM-DD'),
          employerCode: register.employerCode.value,
          searchType: 'basicInfo',
        });
      }
    } else {
      handleDateChange({
        field: field.id,
        state: register,
        value: null,
        error: true,
        errorMessage: 'You should be at least 18 years old to register*',
        saveStepFunc: setRegister,
      });
    }
  };

  const onPasswordChange = () => event => {
    handleEqualityChange({
      field1: register.password,
      field2: register.confirmPassword,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
  };

  const onCountryChange = event => {
    const { value: country } = event.target;
    const isUs = country === 'US';
    register.state = {
      ...register.state,
      value: '',
      error: isUs,
      pristine: true,
      required: isUs,
    };
    register.zipCode = {
      ...register.zipCode,
      value: '',
      error: isUs,
      pristine: true,
      required: isUs,
    };

    handleChange({
      field: register.country.id,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
  };

  const handleSubmit = (notifyVia, to) => {
    const formValues = extractFormValues(register);
    const dateOfBirth = moment(formValues.dateOfBirth).format('YYYY-MM-DD');
    const params = {
      ...formValues,
      dateOfBirth,
      MembershipTypeId: membershipTypeId,
      sendTo: notifyVia,
    };
    setSendTo(to);
    setshowConfirm(true);
    dispatch(doRegister(params));
  };

  const handleShowNotif = () => {
    const hasSigned =
      register.signature.base64 || register.fullName.value !== '';
    if (!register.completed || !hasSigned) {
      if (!hasSigned) {
        enqueueSnackbar(
          'Please fill out signature or input your Full name to proceed',
          {
            variant: 'error',
          },
        );
        fullNameRef.current.focus();
        setSignatureClassName(classes.signingPadFocus);
      }
      if (!register.completed) {
        enqueueSnackbar('Please fill out all fields marked with asterisk *', {
          variant: 'error',
        });
        highlightFormErrors(register, setRegister);
      }
    } else {
      setShowExists(false);
      setshowNotice(true);
    }
  };

  const onSigned = () => {
    const signature = {
      ...model.signature,
      dateStamp: moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
      base64: sigPad.getTrimmedCanvas().toDataURL('image/png'),
      filename: `signature-${Date.now()}.png`,
      filetype: 'image/png',
    };
    setRegister({
      ...register,
      signature,
      fullName: { ...register.fullName, error: false },
    });
    setSignatureClassName(classes.signingPad);
  };

  const clear = () => {
    const hasFullName = register.fullName.value !== '';
    setRegister({
      ...register,
      signature: model.signature,
      fullName: { ...register.fullName, error: !hasFullName },
    });
    sigPad.clear();
  };

  const handleBack = () => {
    setshowNotice(!showNotice);
  };

  const handleEmployerCode = field => event => {
    handleChange({
      field: field.id,
      state: {
        ...register,
        firstName: {
          ...register.firstName,
          error: register.firstName.value === '',
        },
        lastName: {
          ...register.lastName,
          error: register.lastName.value === '',
        },
      },
      event,
      saveStepFunc: setRegister,
    });
    handleCheckPatientExist({
      firstName: register.firstName.value,
      lastName: register.lastName.value,
      employerCode: event.target.value,
      searchType: 'basicInfo',
      birthDateAt: moment(register.dateOfBirth.value).format('YYYY-MM-DD'),
    });
  };
  const handleSearch = field => event => {
    const years = moment(Date.now()).diff(
      moment(register.dateOfBirth.value),
      'years',
    );
    handleChange({
      field: field.id,
      state: {
        ...register,
        firstName: {
          ...register.firstName,
          error: register.firstName.value === '',
          errorMessage: undefined,
        },
        lastName: {
          ...register.lastName,
          error: register.lastName.value === '',
          errorMessage: undefined,
        },
        employerCode: {
          ...register.employerCode,
          error: false,
        },
        dateOfBirth: {
          ...register.dateOfBirth,
          value: register.dateOfBirth.value,
          error: years < 18 || !isNumber(years),
          errorMessage: 'You should be at least 18 years old to register *',
        },
      },
      event,
      saveStepFunc: setRegister,
    });
    handleCheckPatientExist({
      firstName:
        field.id === 'firstName'
          ? event.target.value
          : register.firstName.value,
      lastName:
        field.id === 'lastName' ? event.target.value : register.lastName.value,
      birthDateAt: moment(register.dateOfBirth.value).format('YYYY-MM-DD'),
      employerCode: register.employerCode.value,
      searchType: 'basicInfo',
    });
  };

  const handlePhoneSearch = field => event => {
    handlePhoneChange({
      field: field.id,
      state: register,
      event,
      duplicate: false,
      saveStepFunc: setRegister,
    });
    if (!formatPhoneNumber(event.target.value).error) {
      handleCheckPatientExist({
        phone: event.target.value,
        searchType: 'phone',
      });
    }
  };

  if (showConfirm)
    return (
      <NotificationMessage recipient={sendTo} loginUrl={unauthorizedPath} />
    );

  if (showNotice) {
    return (
      <NoticePage
        onSendEmail={handleSubmit}
        onSendPhone={handleSubmit}
        onBack={handleBack}
        details={register}
      />
    );
  }

  return (
    <div>
      <form autoComplete="off">
        <Grid container spacing={2} direction="column">
          <Grid item xs={12}>
            <Typography variant="h4" color="primary">
              Registration
              <Typography variant="body1" color="textPrimary">
                Please provide the information below to create an account
                profile. If you already have an account, please{' '}
                <Link
                  to="login"
                  component={MyLink}
                  variant="body1"
                  color="secondary"
                >
                  <strong>Sign In here</strong>
                </Link>
                .
              </Typography>
            </Typography>
          </Grid>
          <Grid item xs={12} md={6} className={classes.formContainer}>
            <Grid container spacing={3} justify="center" alignItems="center">
              <Grid item xs={12} md={4}>
                <TextField
                  field={register.firstName}
                  variant="outlined"
                  onChange={onInputChange(register.firstName)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={4}>
                <TextField
                  field={register.middleName}
                  variant="outlined"
                  onChange={onInputChange(register.middleName)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={4}>
                <TextField
                  field={register.lastName}
                  variant="outlined"
                  onChange={handleSearch(register.lastName)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.phone}
                  variant="outlined"
                  onChange={handlePhoneSearch(register.phone)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.email}
                  variant="outlined"
                  type="email"
                  onChange={onEmailChange(register.email)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                {showMemberGroupCodeField ? (
                  <TextField
                    field={register.memberGroupCode}
                    variant="outlined"
                    type="text"
                    onChange={onInputChange(register.memberGroupCode)}
                    keyEvents={{
                      handleKeys: ['enter'],
                      onKeyEvent: handleShowNotif,
                    }}
                  />
                ) : (
                  <TextField
                    field={register.employerCode}
                    variant="outlined"
                    type="text"
                    onChange={handleEmployerCode(register.employerCode)}
                    keyEvents={{
                      handleKeys: ['enter'],
                      onKeyEvent: handleShowNotif,
                    }}
                  />
                )}
              </Grid>
              {!isMobile && <Grid item md={6} />}
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.password}
                  type="password"
                  variant="outlined"
                  onChange={onPasswordChange(register.password)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.confirmPassword}
                  variant="outlined"
                  type="password"
                  onChange={onPasswordChange(register.confirmPassword)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <DatePickerField
                  format="MM/dd/yyyy"
                  openTo="year"
                  minDate={moment().add(-90, 'year')}
                  initialFocusedDate="1980-01-01"
                  maxDate={moment().add(-18, 'year')}
                  clearable
                  fullWidth
                  field={register.dateOfBirth}
                  onChange={onDateChange(register.dateOfBirth)}
                  disableFuture
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                  style={{ width: '100%' }}
                />
              </Grid>
              {!isMobile && <Grid item md={6} />}
              <Grid item xs={12} md={6}>
                <SelectField
                  field={register.gender}
                  options={[
                    { name: '', value: '' },
                    { name: 'Male', value: '12000' },
                    { name: 'Female', value: '12001' },
                  ]}
                  variant="outlined"
                  onChange={onInputChange(register.gender)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.address1}
                  variant="outlined"
                  onChange={onInputChange(register.address1)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.address2}
                  variant="outlined"
                  onChange={onInputChange(register.address2)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.city}
                  variant="outlined"
                  onChange={onInputChange(register.city)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <SelectField
                  field={register.country}
                  options={countries}
                  variant="outlined"
                  onChange={onCountryChange}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                {register.country.value === 'US' ? (
                  <StatesSelect
                    field={register.state}
                    variant="outlined"
                    onChange={onInputChange(register.state)}
                    keyEvents={{
                      handleKeys: ['enter'],
                      onKeyEvent: handleShowNotif,
                    }}
                  />
                ) : (
                  <TextField
                    field={register.state}
                    variant="outlined"
                    onChange={onInputChange(register.state)}
                    keyEvents={{
                      handleKeys: ['enter'],
                      onKeyEvent: handleShowNotif,
                    }}
                  />
                )}
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.zipCode}
                  variant="outlined"
                  onChange={onInputChange(register.zipCode)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              {!isMobile && <Grid item md={6} />}
              <Grid item xs={12} md={6}>
                <SelectField
                  field={register.ethnicity}
                  options={ethnicities}
                  variant="outlined"
                  onChange={onInputChange(register.ethnicity)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <SelectField
                  field={register.race}
                  options={races}
                  variant="outlined"
                  onChange={onInputChange(register.race)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <ConsentForm
              profile={register}
              readConsent={readConsent}
              handleChange={handleChange}
              setReadConsent={setReadConsent}
              setProfile={setRegister}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="signature" className={classes.signingLabel}>
                Signature
              </InputLabel>
              <SignatureCanvas
                penColor="black"
                id="signature"
                onEnd={onSigned}
                ref={ref => {
                  setSigPad(ref);
                }}
                canvasProps={{
                  className: signatureClassName,
                  redrawonresize: 'true',
                }}
                clearOnResize={false}
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} md={6} className={classes.sigControls}>
            <Button
              variant="contained"
              color="primary"
              onClick={clear}
              disabled={!register.signature.base64}
            >
              Clear Signature
            </Button>
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              field={register.fullName}
              variant="outlined"
              onChange={onInputChange(register.fullName)}
              keyEvents={{
                handleKeys: ['enter'],
                onKeyEvent: handleShowNotif,
              }}
              inputRef={fullNameRef}
            />
          </Grid>

          <Grid item xs={12} md={3}>
            <GradientButton
              variant="contained"
              size="large"
              onClick={handleShowNotif}
              disabled={
                !readConsent ||
                !register.consent.value ||
                registrationData.loading ||
                foundPatients.loading
              }
            >
              Save & continue
            </GradientButton>
            <Link
              to="/login"
              component={MyLink}
              variant="body1"
              color="secondary"
              className={classes.loginLink}
            >
              <strong>Back to Sign In</strong>
            </Link>
          </Grid>
        </Grid>
      </form>
      <ExistsDialog
        handleCancel={() => {
          setShowExists(false);
        }}
        open={showExists}
      />
    </div>
  );
}

const { func, object, string, number } = PropTypes;
CovidRegister.propTypes = {
  enqueueSnackbar: func.isRequired,
  dispatch: func.isRequired,
  doResetRegister: func.isRequired,
  doRegister: func.isRequired,
  doLogoutUser: func.isRequired,
  doFindPatient: func.isRequired,
  registrationData: object.isRequired,
  history: object.isRequired,
  foundPatients: object.isRequired,
  unauthorizedPath: string,
  membershipTypeId: number,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doResetRegister: resetRegisterData,
    doRegister: registerRequest,
    doLogoutUser: logoutUser,
    doFindPatient: findPatient,
  };
}

const mapStateToProps = createStructuredSelector({
  registrationData: makeSelectRegister(),
  foundPatients: makeSelectPatientExist(),
  unauthorizedPath: makeSelectUnauthorizedPath(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(CovidRegister);
