import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Echo from 'laravel-echo';
import { useSnackbar } from 'notistack';

import Fab from '@material-ui/core/Fab';
import Link from '@material-ui/core/Link';
import ChatIcon from 'components/icons/chat';

import { setChatMessagesSize } from 'containers/App/actions';

import {
  makeSelectActualPatient,
  makeSelectMemberJoinRoom,
  makeSelectChatMessages,
} from 'containers/App/selectors';
import { Typography, makeStyles } from '@material-ui/core';
import getOptions, { USER_TYPES } from './config';

const useStyles = makeStyles(theme => ({
  bubble: {
    position: 'absolute',
    top: '-5px',
    background: theme.palette.background.bubble,
    borderRadius: '50%',
    height: 25,
    width: 25,
    textAlign: 'center',
    right: 0,
  },
}));

const Bubble = ({ unread }) => {
  const classes = useStyles();
  return (
    unread !== 0 && (
      <div className={classes.bubble}>
        <Typography variant="body2">{unread > 99 ? 99 : unread}</Typography>
      </div>
    )
  );
};

const ChatButton = ({
  component,
  isMobile,
  className,
  memberRoom,
  dispatch,
  chatMessages,
  patient: {
    data: { patient: patientData, token },
  },
  doSetChatMessageSize,
}) => {
  const [unread, setUnread] = useState(chatMessages.unread);
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    if (memberRoom.data && patientData) {
      joinRoom();
    }
  });

  const joinRoom = () => {
    const echo = new Echo(getOptions(token, USER_TYPES.member));
    const roomEcho = new Echo(getOptions(token, USER_TYPES.member));
    roomEcho.join('MemberWaitingRoom');

    echo.join(patientData.userChannelName);

    echo.private(patientData.userChannelName).listen('UserChatEvent', () => {
      setUnread(prev => {
        dispatch(doSetChatMessageSize(prev + 1));
        return prev + 1;
      });
      enqueueSnackbar('You have a new message', {
        variant: 'success',
      });
    });
  };

  return (
    <Link component={component} to="/chatroom">
      <Fab
        variant="extended"
        aria-label="Chat"
        color="primary"
        className={className}
      >
        {<Bubble unread={unread || 0} />}
        <ChatIcon noMargin={isMobile} />
        {!isMobile ? 'Chat with our care navigator' : null}
      </Fab>
    </Link>
  );
};

const mapStateToProps = createStructuredSelector({
  patient: makeSelectActualPatient(),
  memberRoom: makeSelectMemberJoinRoom(),
  chatMessages: makeSelectChatMessages(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSetChatMessageSize: setChatMessagesSize,
  };
}

const { object, func, bool, number, string } = PropTypes;

ChatButton.propTypes = {
  patient: object.isRequired,
  memberRoom: object.isRequired,
  chatMessages: object.isRequired,
  dispatch: func.isRequired,
  doSetChatMessageSize: func.isRequired,
  component: object.isRequired,
  isMobile: bool.isRequired,
  className: string.isRequired,
};

Bubble.propTypes = {
  unread: number.isRequired,
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(ChatButton);
