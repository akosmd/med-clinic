import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import { Typography } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Grid from '@material-ui/core/Grid';

const DialogBox = ({ title, status, handleClose, children }) => (
  <Dialog fullScreen open={status} onClose={handleClose}>
    <AppBar>
      <Toolbar>
        <IconButton edge="start" color="inherit" onClick={handleClose}>
          <CloseIcon />
        </IconButton>
        <Typography variant="h6">{title}</Typography>
      </Toolbar>
    </AppBar>
    <Grid container justify="center">
      <Grid item xs={12} md={8}>
        {children}
      </Grid>
    </Grid>
  </Dialog>
);

const { string, bool, func, node } = PropTypes;
DialogBox.propTypes = {
  title: string,
  status: bool,
  handleClose: func,
  children: node,
};

export default DialogBox;
