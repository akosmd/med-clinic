import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';

import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import GradientButton from 'components/GradientButton';

import {
  getBrainTreePlans,
  getBraintreeToken,
  braintreeCheckout,
} from 'containers/App/actions';
import {
  makeSelectBraintreePlans,
  makeSelectBraintreeToken,
  makeSelectBraintreeCheckout,
} from 'containers/App/selectors';

import Braintree from 'components/Braintree';
import Pricing from '../Pricing';

const useStyles = makeStyles({
  cost: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  formLabel: {
    marginRight: 0,
  },
  container: {
    margin: '.5rem',
    padding: '1rem',
  },
  header: {
    marginTop: '2rem',
  },
});

const Plan = ({
  dispatch,
  doGetPlans,
  doGetBraintreeToken,
  doBraintreeCheckout,
  braintreeCheckoutStatus,
  plans,
  braintreeToken,
}) => {
  const classes = useStyles();
  const [memberPlans, setMemberPlans] = useState([]);
  const [showPayment, setShowPayment] = useState(false);
  const [showBraintree, setShowBraintree] = useState(false);
  const [selectedPlan, setSelectedPlan] = useState(0);

  useEffect(() => {
    if (!plans.loading) {
      dispatch(doGetPlans());
      dispatch(doGetBraintreeToken());
    }
  }, []);

  useEffect(() => {
    if (plans.data) {
      setMemberPlans(
        plans.data
          .filter(plan => plan.name.includes('member_'))
          .sort((a, b) => a.cost - b.cost),
      );
    }
  }, [plans]);

  const handleSelectPlan = plan => {
    setSelectedPlan(plan);
    setShowPayment(true);
  };

  const handleCheckout = payload => {
    const params = {
      plan_id: selectedPlan.id,
      payment_method_nonce: payload.nonce,
      credit_card: payload.details.bin,
    };
    dispatch(doBraintreeCheckout(params));
  };
  if (showBraintree)
    return (
      <Braintree
        braintree={braintreeToken.data}
        amount={Number(selectedPlan.cost)}
        doBraintreeCheckout={handleCheckout}
      />
    );

  return (
    <Grid
      container
      alignItems="center"
      justify="center"
      className={classes.header}
    >
      <Grid item xs={12} md={6} lg={4}>
        <Pricing
          title="VIRTUAL PRIMARY CARE"
          selected
          pricing={[
            { id: 1, caption: 'Connected Care Platform', originalAmount: '' },
            {
              id: 2,
              caption: 'Virtual Primary Care',
              originalAmount: 'unlimited',
            },
            { id: 3, caption: 'Virtual Consult', originalAmount: 'unlimited' },
            {
              id: 4,
              caption: 'Akos Med Clinic Consult',
              originalAmount: 'unlimited',
            },
          ]}
          plans={memberPlans}
          onPlanSelected={handleSelectPlan}
        />

        <Grid item xs={12} md={4} className={classes.header}>
          <GradientButton
            variant="contained"
            size="large"
            disabled={!showPayment || braintreeCheckoutStatus.loading}
            onClick={() => setShowBraintree(true)}
          >
            Proceed to Payment
          </GradientButton>
        </Grid>
      </Grid>
    </Grid>
  );
};

const { func, array, object } = PropTypes;

Plan.propTypes = {
  dispatch: func.isRequired,
  plans: array.isRequired,
  braintreeToken: object.isRequired,
  braintreeCheckoutStatus: object.isRequired,
  doGetPlans: func.isRequired,
  doGetBraintreeToken: func.isRequired,
  doBraintreeCheckout: func.isRequired,
};
function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doGetPlans: getBrainTreePlans,
    doGetBraintreeToken: getBraintreeToken,
    doBraintreeCheckout: braintreeCheckout,
  };
}

const mapStateToProps = createStructuredSelector({
  plans: makeSelectBraintreePlans(),
  braintreeToken: makeSelectBraintreeToken(),
  braintreeCheckoutStatus: makeSelectBraintreeCheckout(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Plan);
