import React, { lazy, Suspense } from 'react';
import PropTypes from 'prop-types';

import CircularProgress from '@material-ui/core/CircularProgress';
import FlexView from 'components/FlexView';

const ViewerComponent = lazy(() => import('./viewer'));

const Loader = ({ loaderHeight }) => (
  <FlexView
    direction="column"
    justify="center"
    alignitems="center"
    height={loaderHeight}
  >
    <CircularProgress />
  </FlexView>
);
const Viewer = ({ loaderHeight, ...props }) => (
  <Suspense fallback={<Loader />}>
    <ViewerComponent loaderHeight={loaderHeight} {...props} />
  </Suspense>
);

const { oneOfType, string, number } = PropTypes;

Loader.propTypes = {
  loaderHeight: oneOfType([string, number]),
};

Viewer.propTypes = {
  loaderHeight: oneOfType([string, number]),
};

export default Viewer;
