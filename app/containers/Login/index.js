/**
 *
 * Login
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';

import { useCookies } from 'react-cookie';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import KeyboardEventHandler from 'react-keyboard-event-handler';

import { Grid, Typography, CircularProgress, Button } from '@material-ui/core';
import Link from '@material-ui/core/Link';
import MyLink from 'components/MyLink';
import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';

import WarningIcon from '@material-ui/icons/Warning';

import {
  signIn,
  resetAuthFields,
  setNotificationType,
  resetSignin,
  setIsUnverifiedAcct,
  setDashboard,
} from 'containers/App/actions';
import {
  makeSelectSignin,
  makeSelectIsUnverifiedAccount,
} from 'containers/App/selectors';

import {
  initialField,
  initialState,
  handleChange,
  highlightFormErrors,
  extractFormValues,
  formatPhone,
} from 'utils/formHelper';

import { INXITE_SSO_URL } from 'utils/config';
import { FORGOT_PASS_URL } from 'utils/constants';

const employeeValidator = {
  ...initialField,
  id: 'employeeValidator',
  caption: 'Phone Number or Email',
  errorMessage: 'Phone Number or Email is required',
};
const password = {
  ...initialField,
  id: 'password',
  caption: 'Password',
};
export function Login({
  dispatch,
  signin,
  doPatientSignin,
  doResetAuthFields,
  enqueueSnackbar,
  doSetNotificationType,
  doResetSignin,
  history,
  isUnverifiedAccount,
  doSetIsUnverifiedAcct,
  doSetDashboardPath,
}) {
  // eslint-disable-next-line no-unused-vars
  const [cookies, setCookie] = useCookies(['name']);
  const [login, setLogin] = useState({
    employeeValidator,
    password,
    ...initialState,
  });
  const [browserError, setBrowserError] = useState(false);

  useEffect(() => {
    // eslint-disable-next-line no-useless-escape
    const isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
    const isIOS = /(iPhone|iPod|iPad)/i.test(navigator.platform);

    if (isIOS && !isSafari) {
      setBrowserError(true);
    }

    if (
      isSafari &&
      !document.cookie.match(/^(.*;)?\s*fixed\s*=\s*[^;]+(.*)?$/)
    ) {
      setCookie('fixed', 'fixed', {
        path: '/',
        expires: new Date('Tue, 19 Jan 2038 03:14:07 UTC'),
      });
      window.location.replace(`${INXITE_SSO_URL}safari/iframe`);
    }

    window.addEventListener('beforeunload', resetIsUnverifiedAcct);

    const url = history.location.pathname.split('/');

    dispatch(doSetDashboardPath(url[1]));
  }, []);

  useEffect(() => {
    if (signin.error && signin.error.status === 401) {
      enqueueSnackbar(
        signin.error.message || 'Invalid Phone Number or Email or Password',
        {
          variant: 'error',
        },
      );
      dispatch(doResetSignin());
    } else if (signin.data) {
      dispatch(push('/2fa'));
      dispatch(doResetAuthFields());
    }
  }, [signin]);

  const resetIsUnverifiedAcct = () => {
    dispatch(doSetIsUnverifiedAcct(false));
    window.removeEventListener('beforeunload', resetIsUnverifiedAcct);
  };

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: login,
      event,
      saveStepFunc: setLogin,
    });
  };

  const handleVerify = () => {
    resetIsUnverifiedAcct();
    dispatch(push('/account-verification'));
  };

  const handleSubmit = () => {
    if (!login.completed) {
      enqueueSnackbar('Please input your Email and Password', {
        variant: 'error',
      });
      highlightFormErrors(login, setLogin);
    }
    if (login.completed) {
      const params = extractFormValues(login);
      const isEmail = params.employeeValidator.includes('@');
      dispatch(doSetNotificationType(isEmail));
      dispatch(
        doPatientSignin({
          ...params,
          employeeValidator: parseInt(params.employeeValidator, 10)
            ? formatPhone(params.employeeValidator)
            : params.employeeValidator,
        }),
      );
    }
  };

  const isPathAUC = ['covid-nj'].includes(
    history.location.pathname.split('/')[1],
  );

  const renderAlert = () => (
    <Dialog
      open={browserError}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Unsupported Browser</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          <WarningIcon color="error" /> <strong>Warning! </strong>Unfortunately
          the browser you are using is currently not supported. We only support
          latest Safari browser at this time, please open the link using Safari.
        </DialogContentText>
      </DialogContent>
    </Dialog>
  );
  const DialogLoginErrorAlert = () => (
    <Dialog
      open
      onClose={resetIsUnverifiedAcct}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
      disableBackdropClick
      disableEscapeKeyDown
    >
      <DialogTitle>Unverified Account</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-slide-description">
          Your account is not yet verified. Please click the verify account
          button below to verify your account.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={resetIsUnverifiedAcct}>Cancel</Button>
        <Button color="primary" onClick={handleVerify}>
          Verify Account
        </Button>
      </DialogActions>
    </Dialog>
  );

  if (browserError) return renderAlert();

  return (
    <div>
      {isUnverifiedAccount && <DialogLoginErrorAlert />}
      <Grid container spacing={2} direction="column">
        <Grid item xs={12}>
          <Typography variant="h4" color="primary">
            Sign In
            <Typography variant="body1" color="textPrimary">
              Sign in with your registered phone number or email address and
              password
            </Typography>
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={12} md={8}>
              <KeyboardEventHandler
                handleKeys={['enter']}
                onKeyEvent={handleSubmit}
              >
                <TextField
                  field={login.employeeValidator}
                  variant="outlined"
                  onChange={onInputChange(login.employeeValidator)}
                />
              </KeyboardEventHandler>
            </Grid>

            <Grid item xs={12} sm={12} md={8}>
              <KeyboardEventHandler
                handleKeys={['enter']}
                onKeyEvent={handleSubmit}
              >
                <TextField
                  field={login.password}
                  type="password"
                  variant="outlined"
                  onChange={onInputChange(login.password)}
                />
              </KeyboardEventHandler>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12} md={6}>
          <Typography variant="body1">
            First time user?{' '}
            <Link
              to="register"
              component={MyLink}
              variant="body1"
              color="secondary"
            >
              <strong>Register here</strong>
            </Link>
          </Typography>
        </Grid>
        {!isPathAUC && (
          <Grid item xs={12} md={6}>
            <Typography variant="body1">
              Need to verify your account?{' '}
              <Link
                to="/account-verification"
                component={MyLink}
                variant="body1"
                color="secondary"
              >
                <strong>Verify here</strong>
              </Link>
            </Typography>
          </Grid>
        )}
        <Grid item xs={12} md={6}>
          <Link href={FORGOT_PASS_URL} color="secondary">
            Forgot Password
          </Link>
        </Grid>
        <Grid item xs={12} md={3}>
          <GradientButton
            variant="contained"
            size="large"
            onClick={handleSubmit}
          >
            {signin.loading && <CircularProgress color="secondary" />}
            {!signin.loading && 'Proceed'}
          </GradientButton>
        </Grid>
      </Grid>
      <iframe
        id="inxiteFrame"
        title="Inxite"
        src={`${INXITE_SSO_URL}logout`}
        style={{ display: 'none' }}
      />
    </div>
  );
}

const { func, object, bool } = PropTypes;

Login.propTypes = {
  dispatch: func.isRequired,
  doPatientSignin: func.isRequired,
  doResetAuthFields: func.isRequired,
  signin: object,
  history: object,
  enqueueSnackbar: func.isRequired,
  doSetNotificationType: func.isRequired,
  doResetSignin: func.isRequired,
  isUnverifiedAccount: bool.isRequired,
  doSetIsUnverifiedAcct: func.isRequired,
  doSetDashboardPath: func.isRequired,
};
const mapStateToProps = createStructuredSelector({
  signin: makeSelectSignin(),
  isUnverifiedAccount: makeSelectIsUnverifiedAccount(),
});
function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doPatientSignin: signIn,
    doResetAuthFields: resetAuthFields,
    doSetNotificationType: setNotificationType,
    doResetSignin: resetSignin,
    doSetIsUnverifiedAcct: setIsUnverifiedAcct,
    doSetDashboardPath: setDashboard,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Login);
