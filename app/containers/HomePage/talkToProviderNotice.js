import React from 'react';
import PropTypes from 'prop-types';

import { Button } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';

function TalkToProviderNotice({ handleConfirm, handleCancel }) {
  return (
    <Dialog
      open
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        Is this related to COVID-19?
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          If your concern is related to COVID-19, please click{' '}
          <strong>"Yes, Take me to COVID-19 Assessment"</strong>, otherwise
          click <em>"No"</em>.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCancel}>No</Button>
        <Button color="primary" onClick={handleConfirm}>
          Yes, Take me to COVID-19 Assessment
        </Button>
      </DialogActions>
    </Dialog>
  );
}

TalkToProviderNotice.propTypes = {
  handleConfirm: PropTypes.func.isRequired,
  handleCancel: PropTypes.func.isRequired,
};
export default TalkToProviderNotice;
