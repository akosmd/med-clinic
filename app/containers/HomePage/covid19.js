import React, { useState, Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { push } from 'connected-react-router';

import { connect } from 'react-redux';
import { compose } from 'redux';

import { Grid, Typography } from '@material-ui/core';

import AkosCard from 'components/AkosCard';
import CashPayment from 'containers/Payment/cash';
import InsurancePayment from 'containers/Payment/insurance';
import NoInsurancePayment from 'containers/Payment/no-insurance';

import InsuranceIcon from 'components/icons/insurance';
import NoInsuranceIcon from 'components/icons/noinsurance';

import moment from 'moment';

import {
  getBrainTreeToken,
  generatePatientCallId,
  resetPaymentCheckout,
  savePaymentToPs,
} from 'containers/App/legacyActions';
import { saveIdDocument } from 'containers/App/actions';

import MedicalHistory from 'components/MedicalHistory';

import {
  makeSelectSignin,
  makePatientImpersonation,
  makeSelectIdImage,
  makeSelectInsuranceImage,
  makeSelectVideoConfStatus,
} from 'containers/App/selectors';
import {
  makeSelectPaymentCheckout,
  makeSelectPaymentAmount,
  makeSelectWaitingRoomStatus,
  makeSelectPatientCallId,
} from 'containers/App/legacySelectors';

import CovidChat from 'containers/CovidChat';

function CovidHomepage({
  patient,
  paymentCheckout,
  amount,
  roomName,
  doGeneratePatientCallId,
  impersonatedPatient,
  callId,
  enqueueSnackbar,
  doGetBrainTreeToken,
  doResetPaymentCheckout,
  doSavePaymentToPs,
  dispatch,
  width,
  videoConfStatus,
  history,
}) {
  const [paymentType, setPaymentType] = useState(false);
  const [actualPatient, setActualPatient] = useState(patient);
  const [paymentString, setPaymentString] = useState('');
  const [showHistory, setShowHistory] = useState(false);
  const [showChat, setShowChat] = useState(false);

  const scrollRef = React.useRef(null);
  const fillerRef = React.useRef(null);
  useEffect(() => {
    dispatch(doGetBrainTreeToken());
  }, []);

  useEffect(() => {
    if (paymentCheckout.data && paymentCheckout.data.status_code === 200) {
      enqueueSnackbar('Payment successful.', {
        variant: 'success',
      });

      const { call_id: patientCallId } = callId.data.result;
      const paymentData = new FormData();
      paymentData.set('callId', patientCallId);

      dispatch(doSavePaymentToPs(paymentData));

      setTimeout(handleSetShowHistory, 1000);
    } else if (paymentCheckout.error) {
      enqueueSnackbar(
        (paymentCheckout.error.status_code === 500 &&
          'Something went wrong. Please try again later.') ||
          `Payment was not successful. Reason: "${
            paymentCheckout.error.error
          }"`,
        {
          variant: 'error',
        },
      );
      dispatch(doResetPaymentCheckout());
    }
  }, [paymentCheckout]);

  useEffect(() => {
    if (videoConfStatus.data || videoConfStatus.error) {
      const tmpPatient = impersonatedPatient.data
        ? impersonatedPatient.data.patient
        : patient.data.patient;
      const params = {
        patientId: tmpPatient.patientLegacyId,
        patientEmail: tmpPatient.email,
        patientDob: tmpPatient.birthDateAt,
        uuid: tmpPatient.uuid,
        staffid: 500,
        call_started: moment(Date.now()).format('YYYY/MM/DD'),
        room_name: roomName,
        call_type: 'connect',
        device_type: window.navigator.userAgent,
      };
      if (!callId.loading && !callId.data)
        dispatch(doGeneratePatientCallId(params));

      setActualPatient(tmpPatient);
    }
  }, [videoConfStatus]);

  const handleShowVideoConf = () => {
    dispatch(doResetPaymentCheckout());
    setShowHistory(false);
    setShowChat(true);
  };

  const handleSetShowHistory = () => {
    dispatch(doResetPaymentCheckout());
    setShowHistory(!showHistory);
  };

  const handleCancel = () => {
    setShowHistory(false);
    setShowChat(false);
  };

  const paymentTypes = {
    cash: (
      <Fragment>
        <CashPayment
          patient={actualPatient || undefined}
          amount={amount}
          onConferenceClick={handleSetShowHistory}
          enqueueSnackbar={enqueueSnackbar}
          buttonCaption="Take Assessment Now"
        />
      </Fragment>
    ),
    insurance: (
      <Fragment>
        <InsurancePayment
          patient={actualPatient || undefined}
          amount={amount}
          onConferenceClick={handleSetShowHistory}
          enqueueSnackbar={enqueueSnackbar}
          buttonCaption="Take Assessment Now"
          width={width}
          otherOption
        />
      </Fragment>
    ),
    noInsurance: (
      <Fragment>
        <NoInsurancePayment
          patient={actualPatient || undefined}
          amount={amount}
          onConferenceClick={handleSetShowHistory}
          enqueueSnackbar={enqueueSnackbar}
          buttonCaption="Take Assessment Now"
          width={width}
          history={history}
        />
      </Fragment>
    ),
  };

  const handleAutoScroll = () => {
    if (!paymentType) fillerRef.current.scrollIntoView({ behavior: 'smooth' });
    if (scrollRef.current !== null) {
      scrollRef.current.scrollIntoView({ behavior: 'smooth' });
    }
  };

  const handleSetPaymentType = payment => {
    handleAutoScroll();
    setPaymentType(paymentTypes[payment]);
    setPaymentString(payment);
  };

  const handleSetShowVideoConf = () => {
    setShowHistory(false);
    setShowChat(false);
    // setShowVideoConf(true);
    dispatch(push(`/video-conference/${roomName}`));
  };

  if (showHistory)
    return (
      <MedicalHistory
        onSymptomSelected={handleShowVideoConf}
        roomName={roomName}
        onCancel={handleCancel}
      />
    );

  if (showChat)
    return (
      <CovidChat
        roomName={roomName}
        onConference={handleSetShowVideoConf}
        showHeader={false}
      />
    );

  return (
    <>
      <Grid
        container
        spacing={2}
        alignItems="center"
        justify="center"
        id="payment-container"
      >
        <Grid item xs={12}>
          <Typography variant="h5">Payment Options</Typography>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
          <AkosCard
            title="I have insurance"
            icon={() => <InsuranceIcon height="3.5rem" width="3.5rem" />}
            selected={paymentString === 'insurance'}
            onClick={() => handleSetPaymentType('insurance')}
          />
        </Grid>

        <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
          <AkosCard
            title="I don't have insurance"
            icon={() => <NoInsuranceIcon height="3.5rem" width="3.5rem" />}
            selected={paymentString === 'noInsurance'}
            onClick={() => handleSetPaymentType('noInsurance')}
          />
        </Grid>
      </Grid>
      <Grid
        container
        spacing={2}
        alignItems="center"
        justify="center"
        ref={scrollRef}
      >
        <Grid item xs={12} lg={8} xl={6}>
          {paymentType !== null && paymentType}
        </Grid>
      </Grid>
      <div style={{ paddingTop: 500 }} ref={fillerRef} />
    </>
  );
}

const { object, func, number, string } = PropTypes;
CovidHomepage.propTypes = {
  patient: object.isRequired,
  dispatch: func.isRequired,
  amount: number,
  callId: object.isRequired,
  impersonatedPatient: object.isRequired,
  paymentCheckout: object.isRequired,
  roomName: string,
  enqueueSnackbar: func.isRequired,
  doGetBrainTreeToken: func.isRequired,
  doResetPaymentCheckout: func.isRequired,
  doSavePaymentToPs: func.isRequired,
  doGeneratePatientCallId: func.isRequired,
  width: string.isRequired,
  videoConfStatus: object.isRequired,
  history: object.isRequired,
};

CovidHomepage.defaultProps = {
  amount: 49,
  roomName: 'covid19',
};

const mapStateToProps = createStructuredSelector({
  patient: makeSelectSignin(),
  impersonatedPatient: makePatientImpersonation(),
  idImage: makeSelectIdImage(),
  insurangeImage: makeSelectInsuranceImage(),
  paymentCheckout: makeSelectPaymentCheckout(),
  grandTotal: makeSelectPaymentAmount(),
  waitingRoomStatus: makeSelectWaitingRoomStatus(),
  callId: makeSelectPatientCallId(),
  videoConfStatus: makeSelectVideoConfStatus(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doGetBrainTreeToken: getBrainTreeToken,
    doGeneratePatientCallId: generatePatientCallId,
    doResetPaymentCheckout: resetPaymentCheckout,
    doSaveIdDocument: saveIdDocument,
    doSavePaymentToPs: savePaymentToPs,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(CovidHomepage);
