import React, { useState, useEffect, Fragment } from 'react';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { makeSelectAdvinowSso } from 'containers/App/selectors';

import { ADVINOW_SSO_URL } from 'utils/config';
import { Typography, CircularProgress } from '@material-ui/core';

function AdvinowSso({ advinowSso }) {
  const [isLoaded, setLoaded] = useState(false);
  const [hasToken, setHasToken] = useState(false);

  useEffect(() => {
    if (advinowSso.data && advinowSso.data.token) setHasToken(true);
  }, [advinowSso]);

  const onLoaded = () => {
    if (!isLoaded) {
      setLoaded(true);
    }
  };

  return (
    <Fragment>
      {!isLoaded && (
        <Typography align="center" component="div">
          <CircularProgress />
          <Typography>Please wait...</Typography>
        </Typography>
      )}
      {hasToken && (
        <div>
          <iframe
            src={`${ADVINOW_SSO_URL}${advinowSso.data.token}&kiosk=AkosWeb1`}
            title="Akos Member Portal"
            style={{ width: '100%', height: '100vh', border: 0 }}
            onLoad={onLoaded}
          />
        </div>
      )}
    </Fragment>
  );
}

AdvinowSso.propTypes = {
  advinowSso: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  advinowSso: makeSelectAdvinowSso(),
});

const withConnect = connect(
  mapStateToProps,
  null,
);

export default compose(withConnect)(AdvinowSso);
