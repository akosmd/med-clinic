import React from 'react';
import PropTypes from 'prop-types';

import { Paper, Typography, CircularProgress } from '@material-ui/core';

import FlexView from 'components/FlexView';
import Button from 'components/GradientButton';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  camera: {
    display: 'flex',
    [theme.breakpoints.down('md')]: {
      display: `none`,
    },
  },
}));

const Viewer = ({
  loaderHeight,
  loading,
  src,
  handleRetake,
  handleDelete,
  ...rest
}) => {
  const classes = useStyles();
  return (
    <Paper elevation={0}>
      {loading ? (
        <FlexView
          height={loaderHeight}
          padding={0}
          justify="center"
          direction="column"
        >
          <Typography component="div" align="center">
            <CircularProgress />
          </Typography>
        </FlexView>
      ) : (
        <FlexView padding={0} justify="center" direction="column">
          <img alt="" src={src} style={{ objectFit: 'contain' }} {...rest} />
          <FlexView justify="space-evenly" padding={0}>
            <Button onClick={handleRetake} className={classes.camera}>
              Retake Photo
            </Button>
            <Button onClick={handleDelete}>Delete Photo</Button>
          </FlexView>
        </FlexView>
      )}
    </Paper>
  );
};

const { number, string, func, bool } = PropTypes;

Viewer.propTypes = {
  loaderHeight: number,
  loading: bool,
  src: string,
  handleRetake: func,
  handleDelete: func,
};

export default Viewer;
