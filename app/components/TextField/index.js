/**
 *
 * TextField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Input from '@material-ui/core/Input';
import TextInput from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import KeyboardEventHandler from 'react-keyboard-event-handler';
function TextField({
  field,
  type,
  margin,
  className,
  onChange,
  inputComponent,
  variant,
  keyEvents,
  loading,
  inputRef,
}) {
  if (!field) return null;
  const {
    error,
    caption,
    errorMessage,
    id,
    pristine,
    shrink,
    readOnly,
    showError,
    ...rest
  } = field;
  let message = caption;
  let memberIdError = false;
  let memberIdLabelError = caption;

  if (error && !pristine)
    message =
      !errorMessage && field.required ? `${caption} is required` : errorMessage;
  if (field.id === 'memberId' && !/^[a-z0-9]+$/i.test(field.value)) {
    memberIdLabelError = 'Member Id should only contain numbers and letters.';
    memberIdError = true;
  }
  const dummy = Date.now();
  if (variant === 'outlined')
    return !keyEvents ? (
      <TextInput
        id={id}
        name={`${dummy}-${id}`}
        error={(error && !pristine) || (!error && memberIdError)}
        label={field.error && !pristine ? message : memberIdLabelError}
        helperText={loading ? 'Please wait while loading...' : undefined}
        {...rest}
        onChange={onChange}
        margin={margin}
        type={type}
        fullWidth={field.fullWidth}
        inputProps={{ readOnly, autoComplete: 'off' }}
        autoComplete="off"
        variant="outlined"
        inputRef={inputRef}
      />
    ) : (
      <KeyboardEventHandler
        handleKeys={keyEvents.handleKeys}
        onKeyEvent={keyEvents.onKeyEvent}
        style={{ width: '100%' }}
      >
        <TextInput
          id={id}
          name={`${dummy}-${id}`}
          error={error && !pristine}
          helperText={loading ? 'Please wait while loading...' : undefined}
          label={field.error && !pristine ? message : caption}
          {...rest}
          onChange={onChange}
          margin={margin}
          type={type}
          InputLabelProps={{
            shrink,
          }}
          fullWidth={field.fullWidth}
          autoComplete="off"
          inputProps={{ readOnly, autoComplete: 'off' }}
          variant="outlined"
          inputRef={inputRef}
        />
      </KeyboardEventHandler>
    );
  return (
    <FormControl
      margin={margin}
      required={field.required}
      fullWidth={field.fullWidth}
      className={className}
    >
      <InputLabel htmlFor={field.id} error={error && !pristine}>
        {field.error && !pristine ? message : field.caption}
      </InputLabel>

      <Input
        {...rest}
        error={error && !pristine}
        type={type}
        placeholder={field.placeholder || ''}
        autoFocus={field.autoFocus}
        fullWidth={field.fullWidth}
        variant={variant}
        onChange={onChange}
        inputComponent={inputComponent}
      />
    </FormControl>
  );
}

const { string, bool, shape, func, object } = PropTypes;
TextField.propTypes = {
  field: shape({
    id: string.isRequired,
    value: string,
    caption: string.isRequired,
    errorMessage: string,
    placeholder: string,
    error: bool,
    required: bool,
    fullWidth: bool,
    autoFocus: bool,
  }),
  onChange: func,
  type: string,
  margin: string,
  className: string,
  inputComponent: func,
  variant: string,
  keyEvents: object,
  loading: bool,
  inputRef: object,
};

TextField.defaultProps = {
  type: 'text',
  margin: 'normal',
  onChange: undefined,
  className: undefined,
  inputComponent: undefined,
};

export default TextField;
