import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Button } from '@material-ui/core';

function NotificationMessage({ recipient, loginUrl }) {
  return (
    <Typography component="div" align="center">
      <Typography variant="h6" align="center">
        Congratulations!
      </Typography>
      <Typography>
        Your account has been created successfully, you may login using the
        username {recipient}. Click the button below to Login.
      </Typography>
      <br />
      <Button href={loginUrl}>Login Now</Button>
    </Typography>
  );
}

NotificationMessage.propTypes = {
  recipient: PropTypes.string,
  loginUrl: PropTypes.string,
};
NotificationMessage.defaultProps = {
  loginUrl: '/',
};

export default NotificationMessage;
