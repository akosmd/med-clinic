/* eslint-disable react/no-unescaped-entities */
import React, { useState } from 'react';
import PropTypes from 'prop-types';

import DropIn from 'braintree-web-drop-in-react';

import { Grid, Typography } from '@material-ui/core';
import PadLockIcon from '@material-ui/icons/Lock';

import GradientButton from 'components/GradientButton';

function BraintreeWidget({
  braintree,
  amount,
  doBraintreeCheckout,
  enqueueSnackbar,
}) {
  const [braintreeInstance, setInstance] = useState(undefined);
  const [canPay, setCanPay] = useState(false);

  const token = braintree.data ? braintree.data.token : undefined;

  const handleBuy = () => {
    braintreeInstance.requestPaymentMethod((err, payload) => {
      if (err) {
        // Handle errors in requesting payment method
        enqueueSnackbar(
          'Credit card authorization failed. Please correct and try again.',
          {
            variant: 'error',
          },
        );
      }

      if (!err) {
        // const { nonce } = payload;
        // eslint-disable-next-line camelcase
        // const { call_id } = callId.data.result;
        // const params = {
        //   amount,
        //   payment_method_nonce: nonce,
        //   patient_id: patient.patientLegacyId,
        //   call_id,
        //   payment_mode: paymentMethod,
        // };

        doBraintreeCheckout(payload);
        // enqueueSnackbar(
        //   'Payment successful. Please wait while we check you in to our virtual waiting room...',
        //   {
        //     variant: 'success',
        //   },
        // );
      }
    });
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Typography variant="h6">
          Please use the payment options below to complete the transaction.
        </Typography>
        <Typography>
          <PadLockIcon /> Payment details are encrypted and securely provided by
          our payment processor. We don't store any sensitive data on our
          servers.
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <DropIn
          options={{
            authorization: token,
            paypal: {
              flow: 'checkout',
              amount,
              currency: 'USD',
            },
            applePay: {
              displayName: 'checkout',
              paymentRequest: {
                total: {
                  label: 'checkout',
                  amount,
                },
              },
            },
            googlePay: {
              googlePayVersion: 2,
              merchantInfo: {
                merchantId: '17369998254377824045',
              },
              transactionInfo: {
                totalPriceStatus: 'FINAL',
                totalPrice: amount,
                currencyCode: 'USD',
              },
              allowedPaymentMethods: [
                {
                  type: 'CARD',
                  parameters: {},
                },
              ],
            },
            paypalCredit: {
              flow: 'checkout',
              amount,
              currency: 'USD',
            },
          }}
          onInstance={instance => {
            setInstance(instance);
          }}
          onNoPaymentMethodRequestable={() => setCanPay(false)}
          onPaymentMethodRequestable={() => setCanPay(true)}
        />
      </Grid>
      <Grid item xs={12} md={6}>
        <GradientButton
          variant="contained"
          size="large"
          disabled={!canPay}
          onClick={handleBuy}
        >
          Pay Now $ {amount ? amount.toFixed(2) : 0.0}
        </GradientButton>
      </Grid>
    </Grid>
  );
}

const { object, number, func } = PropTypes;
BraintreeWidget.propTypes = {
  braintree: object.isRequired,
  doBraintreeCheckout: func.isRequired,
  amount: number.isRequired,
  enqueueSnackbar: func.isRequired,
};

export default BraintreeWidget;
