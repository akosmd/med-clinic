import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { compose } from 'redux';

import AkosCard from 'components/AkosCard';
// import AppointmentIcon from 'components/icons/appointment';

import ProviderIcon from 'components/icons/provider';
import FAQICon from 'components/icons/faq';
import PricingiCon from 'components/icons/pricingPlan';
import CovidIcon from 'components/icons/covid';
import PharmacyIcon from 'components/icons/pharmacy';
import DependentIcon from 'components/icons/dependent';

import { Grid, Typography, Button } from '@material-ui/core';

import {
  setInxiteUrl,
  getPatientDependents,
  showDependentsModal,
  getUserDefaultRoomSuccess,
} from 'containers/App/actions';
import {
  makeSelectVerifyAuth,
  makeSelectInxiteSso,
  makeSelectInxiteUrl,
  makePatientDependents,
  makePatientImpersonation,
  makeShowDependentsModal,
} from 'containers/App/selectors';

import { MEDICATION_LOOK_UP_URL } from 'utils/config';

import getRoom from './defaultRooms';
import MediaNotice from './mediaNotice';
import TalkToProviderNotice from './talkToProviderNotice';

function IndividualButtons({
  dispatch,
  setDefaultRoom,
  account,
  doGetPatientDependents,
}) {
  const [showPricng, setShowPricing] = useState(false);
  const [showMediaPopup, setShowMediaPopup] = useState(false);
  const [showTalkToProvider, setShowTalkToProvider] = useState(false);

  useEffect(() => {
    if (account.error) {
      dispatch(push('/login'));
    } else if (account.data) {
      dispatch(doGetPatientDependents(account.data.patient.id));
    }
  }, [account]);

  const setShowPricingClick = () => {
    setShowPricing(!showPricng);
  };

  const handleCovid19Click = () => {
    dispatch(setDefaultRoom(getRoom(10000)));
    setShowMediaPopup(true);
  };
  const handleTalkToProviderClick = () => {
    // dispatch(setDefaultRoom(getRoom(10000)));
    setShowTalkToProvider(true);
  };

  const handleConfirmAssessment = () => {
    dispatch(push('/covid19'));
  };

  const renderPricing = () => (
    <Typography component="div" align="center">
      <Typography align="center" variant="h5">
        Pay Per Use
      </Typography>
      <Typography variant="h1">$ 49</Typography>
      <Typography varaint="body1">per consult</Typography>
      <Button variant="contained" color="primary" onClick={setShowPricingClick}>
        Close
      </Button>
    </Typography>
  );

  if (showMediaPopup)
    return (
      <MediaNotice
        handleCancel={() => setShowMediaPopup(false)}
        handleConfirm={handleConfirmAssessment}
      />
    );

  if (showTalkToProvider) {
    return (
      <TalkToProviderNotice
        handleCancel={() => dispatch(push('/medical-history'))}
        handleConfirm={() => dispatch(push('/covid19'))}
      />
    );
  }
  return (
    <Grid container spacing={2} alignItems="stretch">
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="COVID-19 Assessment"
          icon={CovidIcon}
          onClick={handleCovid19Click}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Talk to a Provider"
          icon={ProviderIcon}
          onClick={handleTalkToProviderClick}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Medication Lookup"
          icon={PharmacyIcon}
          href={MEDICATION_LOOK_UP_URL}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="Add/Edit Dependents"
          icon={DependentIcon}
          onClick={() => dispatch(push('/my-account'))}
        />
      </Grid>

      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        <AkosCard
          title="FAQs"
          icon={FAQICon}
          onClick={() => dispatch(push('/faq'))}
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={4} xl={3}>
        {!showPricng ? (
          <AkosCard
            title="Pricing & Plans"
            icon={PricingiCon}
            onClick={setShowPricingClick}
          />
        ) : (
          <AkosCard content={renderPricing()} />
        )}
      </Grid>
    </Grid>
  );
}

const { func, object } = PropTypes;
IndividualButtons.propTypes = {
  dispatch: func.isRequired,
  doGetPatientDependents: func.isRequired,
  setDefaultRoom: func.isRequired,
  account: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  account: makeSelectVerifyAuth(),
  inxiteSso: makeSelectInxiteSso(),
  inxiteUrl: makeSelectInxiteUrl(),
  dependents: makePatientDependents(),
  impersonatedPatient: makePatientImpersonation(),
  impersonationShowModal: makeShowDependentsModal(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doSetUrl: setInxiteUrl,
    doGetPatientDependents: getPatientDependents,
    doShowDependentsModal: showDependentsModal,
    setDefaultRoom: getUserDefaultRoomSuccess,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(IndividualButtons);
