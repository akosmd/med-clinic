import React, { lazy, Suspense } from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import PropTypes from 'prop-types';
import FlexView from 'components/FlexView';
import Typography from '@material-ui/core/Typography';

const CameraComponent = lazy(() => import('./camera'));

const Loader = ({ containerHeight }) => (
  <FlexView direction="column" justify="center" height={containerHeight}>
    <LinearProgress />
    <Typography align="center" variant="body1">
      {'Initializing camera...'}
    </Typography>
  </FlexView>
);
const WebCam = ({ containerHeight, ...props }) => (
  <Suspense fallback={<Loader containerHeight={containerHeight} />}>
    <CameraComponent containerHeight={containerHeight} {...props} />
  </Suspense>
);

const { oneOfType, string, number } = PropTypes;

Loader.propTypes = {
  containerHeight: oneOfType([string, number]),
};

WebCam.propTypes = {
  containerHeight: oneOfType([string, number]),
};

export default WebCam;
