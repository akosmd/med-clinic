import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import SignatureCanvas from 'react-signature-canvas';

import moment from 'moment';
import { createStructuredSelector } from 'reselect';
import { Grid, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import _ from 'lodash';

import TextField from 'components/TextField';
import GradientButton from 'components/GradientButton';
import DatePickerField from 'components/DatePickerField';
import SelectField from 'components/Select';
import StatesSelect from 'components/StatesSelect';
import ConsentForm from 'components/Consent';

import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

// eslint-disable-next-line no-unused-vars
import SignaturePad from 'react-signature-pad-wrapper';

import Link from '@material-ui/core/Link';
import MyLink from 'components/MyLink';
import countries from 'utils/countries.json';
import ethnicities from 'utils/ethnicities.json';
import races from 'utils/races.json';

import { MEMBER_TYPES } from 'utils/config';

import {
  handleChange,
  highlightFormErrors,
  handleEmailChange,
  handleDateChange,
  extractFormValues,
  handleEqualityChange,
  handlePhoneChange,
  formatPhoneNumber,
  invalidEmail,
} from 'utils/formHelper';

import {
  register as registerRequest,
  resetRegisterData,
  findPatient,
  logoutUser,
} from 'containers/App/actions';
import {
  makeSelectRegister,
  makeSelectFindPatient,
} from 'containers/App/selectors';

import useMediaQuery from '@material-ui/core/useMediaQuery';

import model from '../model';
import NoticePage from '../confirmRegistration';
import NotificationMessage from '../notificationMessage';

import { patientExists } from '../patientEvaluator';
import ExistsDialog from '../existsDialog';
const useStyles = makeStyles({
  formContainer: {
    width: '100%',
  },
  loginLink: {
    marginLeft: '1rem',
  },
  signingPad: {
    border: '1px solid #b7b7b7',
    padding: '1rem',
    borderRadius: '4px',
    width: '100%',
    height: '200px',
  },
  signingLabel: {
    padding: '1rem',
  },
  sigControls: {
    display: 'flex',
    justifyContent: 'space-between',
  },
});

export function VdpcLite({
  doRegister,
  dispatch,
  enqueueSnackbar,
  registrationData,
  doResetRegister,
  doLogoutUser,
  history,
  foundPatients,
  doFindPatient,
}) {
  const [register, setRegister] = useState({ ...model });
  const [readConsent, setReadConsent] = useState(false);
  const [showNotice, setshowNotice] = useState(false);
  const [showConfirm, setshowConfirm] = useState(false);
  const [sendTo, setSendTo] = useState('');
  const [sigPad, setSigPad] = useState(null);
  const [showExists, setShowExists] = useState(false);
  const [showMemberGroupCodeField, setShowMemberGroupCodeField] = useState(
    false,
  );
  const isMobile = useMediaQuery(theme => theme.breakpoints.down('sm'));
  // when user tries to register, make sure to cleanup existing store
  useEffect(() => {
    dispatch(doLogoutUser());
  }, []);
  useEffect(() => {
    const {
      location: { search },
    } = history;
    if (search !== '') {
      evaluateQs(search);
    }
  }, [history]);

  useEffect(() => {
    if (foundPatients.data) {
      const formValues = extractFormValues(register);
      const exists = patientExists({
        patients: foundPatients.data,
        patientToFind: formValues,
      });
      if (exists) setShowExists(true);
    }
  }, [foundPatients]);

  useEffect(() => {
    if (registrationData.data) {
      setRegister({ ...model });
      setshowNotice(false);
      setshowConfirm(true);
      dispatch(doResetRegister());
    }
  }, [registrationData]);

  const evaluateQs = search => {
    const params = search.split('&');
    const code = params[0].replace('?memberGroupCode=', '');
    setShowMemberGroupCodeField(true);
    setRegister({
      ...register,
      memberGroupCode: {
        ...register.memberGroupCode,
        value: code,
        error: false,
        pristine: false,
        readOnly: true,
      },
    });
  };

  const classes = useStyles();

  const onInputChange = field => event => {
    handleChange({
      field: field.id,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
  };

  const onEmailChange = field => event => {
    handleEmailChange({
      field: field.id,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
    const params = {
      limit: 100,
      find: event.target.value,
      look_up_columns: ['email'],
    };
    const debounced = _.debounce(() => dispatch(doFindPatient(params)), 2000);
    if (!invalidEmail(event.target.value)) debounced();
  };

  const onDateChange = field => value => {
    const years = moment(Date.now()).diff(moment(value), 'years');
    if (years >= 18) {
      handleDateChange({
        field: field.id,
        state: register,
        value,
        saveStepFunc: setRegister,
      });
      const params = {
        limit: 100,
        find: moment(value).format('YYYY-MM-DD'),
        look_up_columns: ['birth_date_at'],
      };
      const debounced = _.debounce(() => dispatch(doFindPatient(params)), 2000);
      if (moment.isDate(value)) debounced();
    } else {
      handleDateChange({
        field: field.id,
        state: register,
        value: null,
        error: true,

        saveStepFunc: setRegister,
      });
    }
  };

  const onPasswordChange = () => event => {
    handleEqualityChange({
      field1: register.password,
      field2: register.confirmPassword,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
  };

  const handleSearch = field => event => {
    handleChange({
      field: field.id,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
    const params = {
      limit: 100,
      find: event.target.value,
      look_up_columns: ['email', 'last_name', 'first_name', 'phone'],
    };
    const debounced = _.debounce(() => dispatch(doFindPatient(params)), 2000);
    if (!field.error && event.target.value) debounced();
  };

  const handlePhoneSearch = field => event => {
    handlePhoneChange({
      field: field.id,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
    const params = {
      limit: 100,
      find: formatPhoneNumber(event.target.value).value,
      look_up_columns: ['phone'],
    };
    const debounced = _.debounce(() => dispatch(doFindPatient(params)), 2000);

    if (!formatPhoneNumber(event.target.value).error) debounced();
  };

  const handleSubmit = (notifyVia, to) => {
    const formValues = extractFormValues(register);
    const dateOfBirth = moment(formValues.dateOfBirth).format('YYYY-MM-DD');
    const { employerCode } = formValues;
    const params = {
      ...formValues,
      dateOfBirth,
      employerCode: employerCode === '' ? 'AKOS' : employerCode,
      MembershipTypeId: MEMBER_TYPES.VDPC_Lite,
      sendTo: notifyVia,
    };
    setSendTo(to);
    setshowConfirm(true);
    dispatch(doRegister(params));
  };

  const handleShowNotif = () => {
    const hasSigned =
      register.signature.base64 || register.fullName.value !== '';
    if (!register.completed || !hasSigned) {
      highlightFormErrors(register, setRegister);
      enqueueSnackbar('Please fill out all fields marked with asterisk *', {
        variant: 'error',
      });
    } else {
      setshowNotice(true);
    }
  };

  const onSigned = () => {
    const signature = {
      ...model.signature,
      dateStamp: moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
      base64: sigPad.getTrimmedCanvas().toDataURL('image/png'),
      filename: `signature-${Date.now()}.png`,
      filetype: 'image/png',
    };
    setRegister({
      ...register,
      signature,
      fullName: { ...register.fullName, error: false },
    });
  };
  const clear = () => {
    const hasFullName = register.fullName.value !== '';
    setRegister({
      ...register,
      signature: model.signature,
      fullName: { ...register.fullName, error: !hasFullName },
    });
    sigPad.clear();
  };

  const handleBack = () => {
    setshowNotice(!showNotice);
  };

  const onCountryChange = event => {
    register.state = {
      ...register.state,
      value: '',
      error: true,
      pristine: true,
    };

    handleChange({
      field: register.country.id,
      state: register,
      event,
      saveStepFunc: setRegister,
    });
  };

  if (showConfirm)
    return <NotificationMessage recipient={sendTo} loginUrl="/login" />;

  if (showNotice) {
    return (
      <NoticePage
        onSendEmail={handleSubmit}
        onSendPhone={handleSubmit}
        onBack={handleBack}
        details={register}
      />
    );
  }

  return (
    <div>
      <form autoComplete="off">
        <Grid container spacing={2} direction="column">
          <Grid item xs={12}>
            <Typography variant="h4" color="primary">
              Registration
              <Typography variant="body1" color="textPrimary">
                Please provide the information below to create an account
                profile. If you already have an account, please{' '}
                <Link
                  to="login"
                  component={MyLink}
                  variant="body1"
                  color="secondary"
                >
                  <strong>Sign In here</strong>
                </Link>
                .
              </Typography>
            </Typography>
          </Grid>
          <Grid item xs={12} md={6} className={classes.formContainer}>
            <Grid container spacing={3} justify="center" alignItems="center">
              <Grid item xs={12} md={4}>
                <TextField
                  field={register.firstName}
                  variant="outlined"
                  onChange={onInputChange(register.firstName)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={4}>
                <TextField
                  field={register.middleName}
                  variant="outlined"
                  onChange={onInputChange(register.middleName)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={4}>
                <TextField
                  field={register.lastName}
                  variant="outlined"
                  onChange={handleSearch(register.lastName)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.phone}
                  variant="outlined"
                  onChange={handlePhoneSearch(register.phone)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.email}
                  variant="outlined"
                  type="email"
                  onChange={onEmailChange(register.email)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                {showMemberGroupCodeField ? (
                  <TextField
                    field={register.memberGroupCode}
                    variant="outlined"
                    type="text"
                    onChange={onInputChange(register.memberGroupCode)}
                    keyEvents={{
                      handleKeys: ['enter'],
                      onKeyEvent: handleShowNotif,
                    }}
                  />
                ) : (
                  <TextField
                    field={register.employerCode}
                    variant="outlined"
                    type="text"
                    onChange={onInputChange(register.employerCode)}
                    keyEvents={{
                      handleKeys: ['enter'],
                      onKeyEvent: handleShowNotif,
                    }}
                  />
                )}
              </Grid>
              {!isMobile && <Grid item md={6} />}
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.password}
                  type="password"
                  variant="outlined"
                  onChange={onPasswordChange(register.password)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.confirmPassword}
                  variant="outlined"
                  type="password"
                  onChange={onPasswordChange(register.confirmPassword)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <DatePickerField
                  format="MM/dd/yyyy"
                  openTo="year"
                  minDate={moment().add(-90, 'year')}
                  initialFocusedDate="1980-01-01"
                  maxDate={moment().add(-18, 'year')}
                  clearable
                  fullWidth
                  field={register.dateOfBirth}
                  onChange={onDateChange(register.dateOfBirth)}
                  disableFuture
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                  style={{ width: '100%' }}
                />
              </Grid>
              {!isMobile && <Grid item md={6} />}
              <Grid item xs={12} md={6}>
                <SelectField
                  field={register.gender}
                  options={[
                    { name: '', value: '' },
                    { name: 'Male', value: '12000' },
                    { name: 'Female', value: '12001' },
                  ]}
                  variant="outlined"
                  onChange={onInputChange(register.gender)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.address1}
                  variant="outlined"
                  onChange={onInputChange(register.address1)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.address2}
                  variant="outlined"
                  onChange={onInputChange(register.address2)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.city}
                  variant="outlined"
                  onChange={onInputChange(register.city)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <SelectField
                  field={register.country}
                  options={countries}
                  variant="outlined"
                  onChange={onCountryChange}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                {register.country.value === 'US' ? (
                  <StatesSelect
                    field={register.state}
                    variant="outlined"
                    onChange={onInputChange(register.state)}
                    keyEvents={{
                      handleKeys: ['enter'],
                      onKeyEvent: handleShowNotif,
                    }}
                  />
                ) : (
                  <TextField
                    field={register.state}
                    variant="outlined"
                    onChange={onInputChange(register.state)}
                    keyEvents={{
                      handleKeys: ['enter'],
                      onKeyEvent: handleShowNotif,
                    }}
                  />
                )}
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  field={register.zipCode}
                  variant="outlined"
                  onChange={onInputChange(register.zipCode)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              {!isMobile && <Grid item md={6} />}
              <Grid item xs={12} md={6}>
                <SelectField
                  field={register.ethnicity}
                  options={ethnicities}
                  variant="outlined"
                  onChange={onInputChange(register.ethnicity)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <SelectField
                  field={register.race}
                  options={races}
                  variant="outlined"
                  onChange={onInputChange(register.race)}
                  keyEvents={{
                    handleKeys: ['enter'],
                    onKeyEvent: handleShowNotif,
                  }}
                />
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <ConsentForm
              profile={register}
              readConsent={readConsent}
              handleChange={handleChange}
              setReadConsent={setReadConsent}
              setProfile={setRegister}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="signature" className={classes.signingLabel}>
                Signature
              </InputLabel>
              <SignatureCanvas
                penColor="black"
                id="signature"
                onEnd={onSigned}
                ref={ref => {
                  setSigPad(ref);
                }}
                canvasProps={{
                  className: classes.signingPad,
                  redrawOnResize: true,
                }}
                clearOnResize={false}
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} md={6} className={classes.sigControls}>
            <Button
              variant="contained"
              color="primary"
              onClick={clear}
              disabled={!register.signature.base64}
            >
              Clear Signature
            </Button>
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              field={register.fullName}
              variant="outlined"
              onChange={onInputChange(register.fullName)}
            />
          </Grid>

          <Grid item xs={12} md={3}>
            <GradientButton
              variant="contained"
              size="large"
              onClick={handleShowNotif}
              disabled={
                !readConsent ||
                !register.consent.value ||
                registrationData.loading
              }
            >
              Next
            </GradientButton>
            <Link
              to="/login"
              component={MyLink}
              variant="body1"
              color="secondary"
              className={classes.loginLink}
            >
              <strong>Back to Sign In</strong>
            </Link>
          </Grid>
        </Grid>
      </form>

      <ExistsDialog
        handleCancel={() => {
          setShowExists(false);
        }}
        open={showExists}
      />
    </div>
  );
}

const { func, object } = PropTypes;
VdpcLite.propTypes = {
  enqueueSnackbar: func.isRequired,
  dispatch: func.isRequired,
  doResetRegister: func.isRequired,
  doRegister: func.isRequired,
  doFindPatient: func.isRequired,
  doLogoutUser: func.isRequired,
  history: object.isRequired,
  registrationData: object.isRequired,
  foundPatients: object.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doResetRegister: resetRegisterData,
    doRegister: registerRequest,
    doLogoutUser: logoutUser,
    doFindPatient: findPatient,
  };
}

const mapStateToProps = createStructuredSelector({
  registrationData: makeSelectRegister(),
  foundPatients: makeSelectFindPatient(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(VdpcLite);
