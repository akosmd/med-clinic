/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import DropIn from 'braintree-web-drop-in-react';

import { Grid, Typography } from '@material-ui/core';
import PadLockIcon from '@material-ui/icons/Lock';

import GradientButton from 'components/GradientButton';
import { checkoutBraintree } from 'containers/App/legacyActions';
import { saveIdDocument } from 'containers/App/actions';
import {
  makeSelectBrainTreeToken,
  makeSelectPatientCallId,
  makeSelectPaymentAmount,
  makeSelectPaymentCheckout,
} from 'containers/App/legacySelectors';

import { PAYMENT_MODES } from 'utils/config';

function BraintreeWidget({
  paymentMethod,
  patient,
  callId,
  braintree,
  amount,
  grandTotal,
  doBraintreeCheckout,
  dispatch,
  enqueueSnackbar,
  paymentCheckout,
  doSavePayment,
}) {
  const [braintreeInstance, setInstance] = useState(undefined);
  const [canPay, setCanPay] = useState(false);
  const [showMe, setShowMe] = useState(true);
  const [token, setToken] = useState();

  useEffect(() => {
    setShowMe(grandTotal > 0);
  }, [grandTotal]);

  useEffect(() => {
    if (braintree.data) {
      setToken(braintree.data.result);
    }
  }, [braintree]);
  // const token = braintree.data ? braintree.data.result : undefined;

  useEffect(() => {
    if (paymentCheckout.error) {
      braintreeInstance.clearSelectedPaymentMethod();
    }
  }, [paymentCheckout]);

  const handleBuy = () => {
    // eslint-disable-next-line camelcase
    const { call_id } = callId.data.result;
    setCanPay(false);
    dispatch(
      doSavePayment({
        planMemberId: patient.id,
        paymentModeId: PAYMENT_MODES.SELF_PAYMENT,
        amount,
        callId: call_id,
        attachments: [],
      }),
    );

    braintreeInstance.requestPaymentMethod((err, payload) => {
      if (err) {
        // Handle errors in requesting payment method
        setCanPay(true);
        enqueueSnackbar(
          'Credit card authorization failed. Please correct and try again.',
          {
            variant: 'error',
          },
        );
      }

      if (!err) {
        const { nonce } = payload;
        const params = {
          amount,
          payment_method_nonce: nonce,
          patient_id: patient.patientLegacyId,
          call_id,
          payment_mode: paymentMethod,
        };
        dispatch(doBraintreeCheckout(params));
      }
    });
  };
  if (!showMe) return null;
  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Typography variant="h6">
          Please use the payment options below to complete the transaction.
        </Typography>
        <Typography>
          <PadLockIcon /> Payment details are encrypted and securely provided by
          our payment processor. We don't store any sensitive data on our
          servers.
        </Typography>
      </Grid>
      {token && (
        <Grid item xs={12}>
          <DropIn
            options={{
              authorization: token,
              card: {
                cardholderName: true,
              },
              paypal: {
                flow: 'checkout',
                amount,
                currency: 'USD',
              },
              applePay: {
                displayName: 'checkout',
                paymentRequest: {
                  total: {
                    label: 'checkout',
                    amount,
                  },
                },
              },
              googlePay: {
                googlePayVersion: 2,
                // merchantInfo: {
                merchantId: '17369998254377824045',
                // },
                transactionInfo: {
                  totalPriceStatus: 'FINAL',
                  totalPrice: `${amount}`,
                  currencyCode: 'USD',
                },
                allowedPaymentMethods: [
                  {
                    type: 'CARD',
                    parameters: {},
                  },
                ],
              },
              paypalCredit: {
                flow: 'checkout',
                amount,
                currency: 'USD',
              },
            }}
            onInstance={instance => {
              setInstance(instance);
            }}
            onNoPaymentMethodRequestable={() => setCanPay(false)}
            onPaymentMethodRequestable={() => setCanPay(true)}
            // onPaymentOptionSelected={() => onCanPay(true)}
          />
        </Grid>
      )}
      <Grid item xs={12} md={6}>
        <GradientButton
          variant="contained"
          size="large"
          disabled={!canPay}
          onClick={handleBuy}
        >
          Pay Now $ {amount ? amount.toFixed(2) : 0.0}
        </GradientButton>
      </Grid>
    </Grid>
  );
}

const { object, number, func, string } = PropTypes;
BraintreeWidget.propTypes = {
  braintree: object.isRequired,
  dispatch: func.isRequired,
  doBraintreeCheckout: func.isRequired,
  amount: number.isRequired,
  grandTotal: number,
  patient: object.isRequired,
  callId: object.isRequired,
  enqueueSnackbar: func.isRequired,
  paymentMethod: string,
  paymentCheckout: object.isRequired,
  doSavePayment: func.isRequired,
};

BraintreeWidget.defaultProps = {
  paymentMethod: 'selfPay',
};
const mapStateToProps = createStructuredSelector({
  braintree: makeSelectBrainTreeToken(),
  callId: makeSelectPatientCallId(),
  grandTotal: makeSelectPaymentAmount(),
  paymentCheckout: makeSelectPaymentCheckout(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    doBraintreeCheckout: checkoutBraintree,
    doSavePayment: saveIdDocument,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(BraintreeWidget);
