import React from 'react';
import { Table, TableBody, TableRow, TableCell } from '@material-ui/core';
import RemoveIcon from '@material-ui/icons/Close';
import PropTypes from 'prop-types';

const list = ({ data, removeItem, id, noListText }) => (
  <Table>
    <TableBody>
      {data ? (
        data.map((item, index) => (
          <TableRow hover tabIndex={-1} key={`${id}-row-${index + 1}`}>
            <TableCell>{index + 1}</TableCell>
            <TableCell
              style={{ width: '100%' }}
              key={`${id}-cell-${index + 1}`}
              align="left"
            >
              {item.Name || item}
            </TableCell>
            <TableCell onClick={() => removeItem(item)}>
              {<RemoveIcon />}
            </TableCell>
          </TableRow>
        ))
      ) : (
        <TableRow>
          <TableCell>{noListText || 'No Item Available'}</TableCell>
        </TableRow>
      )}
    </TableBody>
  </Table>
);

const { oneOfType, bool, func, array, string } = PropTypes;

list.propTypes = {
  data: oneOfType([array, bool]),
  removeItem: func.isRequired,
  id: string.isRequired,
  noListText: string.isRequired,
};

export default list;
